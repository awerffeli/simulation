name := """simulation"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, SbtWeb)

scalaVersion := "2.11.1"

resolvers +=
  "maven2-repository.dev.java.net" at "http://download.java.net/maven/2"

resolvers +=
  "osgeo" at "http://download.osgeo.org/webdav/geotools/"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  javaWs,
  "mysql" % "mysql-connector-java" % "5.1.21",
  "org.webjars" %% "webjars-play" % "2.3.0",
  "org.webjars" % "jquery" % "2.1.1",
  "org.webjars" % "jquery-ui" % "1.11.1",
  "org.webjars" % "bootstrap" % "3.2.0",
  "org.webjars" % "angularjs" % "1.3.0-rc.3",
  "javax.ws.rs" % "javax.ws.rs-api" % "2.0.1",
  "org.glassfish.jersey.core" % "jersey-client" % "2.12",
  "org.geotools" % "gt-shapefile" % "12-RC1",
  "org.geotools" % "gt-swing" % "12-RC1",
  "com.github.play2war" % "play2-war-core-common_2.10" % "1.2",
  "org.json" % "json" % "20140107"
)


