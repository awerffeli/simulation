# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table default_map (
  id                        integer auto_increment not null,
  max_latitude              integer,
  min_latitude              integer,
  max_longitude             integer,
  min_longitude             integer,
  name                      varchar(255),
  is_removed                tinyint(1) default 0,
  version                   integer not null,
  constraint pk_default_map primary key (id))
;

create table deterministic_driver (
  traffic_id                integer,
  start_node_id             integer,
  target_destination_node_id integer,
  is_leaving_simulation     tinyint(1) default 0,
  car_size                  integer,
  constraint ck_deterministic_driver_car_size check (car_size in (0,1,2)))
;

create table driver_info (
  id                        integer auto_increment not null,
  car_id                    integer,
  simulation_result_id      integer,
  car_size                  varchar(255),
  strategy                  varchar(255),
  max_walking_distance      integer,
  max_willing_to_pay_for_parking double,
  destination_node_id       integer,
  parkspace_id              integer,
  start                     bigint,
  end                       bigint,
  optimal_parkspace_id      integer,
  driven_distance           double,
  walked_distance           double,
  version                   integer not null,
  constraint pk_driver_info primary key (id))
;

create table link (
  id                        integer auto_increment not null,
  map_id                    integer,
  start_node_id             integer,
  destination_node_id       integer,
  speed                     integer,
  length                    double,
  direction                 integer,
  constraint pk_link primary key (id))
;

create table matching_result (
  id                        integer auto_increment not null,
  simulation_id             integer,
  search_count              integer,
  offer_count               integer,
  match_count               integer,
  cancel_match_count        integer,
  successful_match_count    integer,
  cancel_search_count       integer,
  cancel_offer_count        integer,
  constraint pk_matching_result primary key (id))
;

create table node (
  dtype                     varchar(10) not null,
  id                        integer auto_increment not null,
  map_id                    integer,
  longitude                 double,
  latitude                  double,
  max_allowed_park_time     integer,
  price                     integer,
  total_parkspaces          integer,
  type                      integer,
  constraint ck_node_type check (type in (0,1)),
  constraint pk_node primary key (id))
;

create table simulation (
  id                        integer auto_increment not null,
  traffic_id                integer,
  map_id                    integer,
  run_date                  datetime,
  name                      varchar(255),
  comment                   varchar(255),
  runs_count                integer,
  has_finished              tinyint(1) default 0,
  simulation_duration       integer,
  simulation_speed          integer,
  start_time                bigint,
  end_time                  bigint,
  show_visualization        tinyint(1) default 0,
  is_removed                tinyint(1) default 0,
  constraint pk_simulation primary key (id))
;

create table simulation_result (
  id                        integer auto_increment not null,
  simulation_id             integer,
  total_driven_distance     double,
  total_walked_distance     double,
  max_driven_distance       double,
  traffic_name              varchar(255),
  new_vehicles_rate         integer,
  parked_vehicles_count     integer,
  driving_vehicles_count    integer,
  constant_traffic          tinyint(1) default 0,
  version                   integer not null,
  constraint pk_simulation_result primary key (id))
;

create table simulation_step (
  id                        integer auto_increment not null,
  step_nr                   integer,
  driver_info_id            integer,
  link_id                   integer,
  constraint pk_simulation_step primary key (id))
;

create table traffic (
  id                        integer auto_increment not null,
  name                      varchar(255),
  comment                   varchar(255),
  constant_traffic          tinyint(1) default 0,
  new_vehicles_rate         integer,
  driving_vehicles_count    integer,
  parked_vehicles_count     integer,
  vehicle_compact_percent   float,
  vehicle_normal_percent    float,
  vehicle_big_percent       float,
  strategy_luck_percentage  float,
  strategy_one_two_block_percentage float,
  strategy_inside_outside_percentage float,
  strategy_match_percentage float,
  range_pay_parking_min     integer,
  range_pay_parking_max     integer,
  range_walk_min            integer,
  range_walk_max            integer,
  range_time_parking_min    integer,
  range_time_parking_max    integer,
  range_time_parked_parking_min integer,
  range_time_parked_parking_max integer,
  max_parkspace_search_time integer,
  is_template               tinyint(1) default 0,
  original_traffic_id       integer,
  is_removed                tinyint(1) default 0,
  version                   integer not null,
  constraint pk_traffic primary key (id))
;

alter table deterministic_driver add constraint fk_deterministic_driver_traffic_1 foreign key (traffic_id) references traffic (id) on delete restrict on update restrict;
create index ix_deterministic_driver_traffic_1 on deterministic_driver (traffic_id);
alter table deterministic_driver add constraint fk_deterministic_driver_startNode_2 foreign key (start_node_id) references node (id) on delete restrict on update restrict;
create index ix_deterministic_driver_startNode_2 on deterministic_driver (start_node_id);
alter table deterministic_driver add constraint fk_deterministic_driver_targetDestinationNode_3 foreign key (target_destination_node_id) references node (id) on delete restrict on update restrict;
create index ix_deterministic_driver_targetDestinationNode_3 on deterministic_driver (target_destination_node_id);
alter table driver_info add constraint fk_driver_info_simulationResult_4 foreign key (simulation_result_id) references simulation_result (id) on delete restrict on update restrict;
create index ix_driver_info_simulationResult_4 on driver_info (simulation_result_id);
alter table driver_info add constraint fk_driver_info_destinationNode_5 foreign key (destination_node_id) references node (id) on delete restrict on update restrict;
create index ix_driver_info_destinationNode_5 on driver_info (destination_node_id);
alter table driver_info add constraint fk_driver_info_parkspace_6 foreign key (parkspace_id) references node (id) on delete restrict on update restrict;
create index ix_driver_info_parkspace_6 on driver_info (parkspace_id);
alter table driver_info add constraint fk_driver_info_optimalParkspace_7 foreign key (optimal_parkspace_id) references node (id) on delete restrict on update restrict;
create index ix_driver_info_optimalParkspace_7 on driver_info (optimal_parkspace_id);
alter table link add constraint fk_link_map_8 foreign key (map_id) references default_map (id) on delete restrict on update restrict;
create index ix_link_map_8 on link (map_id);
alter table link add constraint fk_link_startNode_9 foreign key (start_node_id) references node (id) on delete restrict on update restrict;
create index ix_link_startNode_9 on link (start_node_id);
alter table link add constraint fk_link_destinationNode_10 foreign key (destination_node_id) references node (id) on delete restrict on update restrict;
create index ix_link_destinationNode_10 on link (destination_node_id);
alter table matching_result add constraint fk_matching_result_simulation_11 foreign key (simulation_id) references simulation (id) on delete restrict on update restrict;
create index ix_matching_result_simulation_11 on matching_result (simulation_id);
alter table node add constraint fk_node_map_12 foreign key (map_id) references default_map (id) on delete restrict on update restrict;
create index ix_node_map_12 on node (map_id);
alter table simulation add constraint fk_simulation_traffic_13 foreign key (traffic_id) references traffic (id) on delete restrict on update restrict;
create index ix_simulation_traffic_13 on simulation (traffic_id);
alter table simulation add constraint fk_simulation_map_14 foreign key (map_id) references default_map (id) on delete restrict on update restrict;
create index ix_simulation_map_14 on simulation (map_id);
alter table simulation_result add constraint fk_simulation_result_simulation_15 foreign key (simulation_id) references simulation (id) on delete restrict on update restrict;
create index ix_simulation_result_simulation_15 on simulation_result (simulation_id);
alter table simulation_step add constraint fk_simulation_step_driverInfo_16 foreign key (driver_info_id) references driver_info (id) on delete restrict on update restrict;
create index ix_simulation_step_driverInfo_16 on simulation_step (driver_info_id);
alter table simulation_step add constraint fk_simulation_step_link_17 foreign key (link_id) references link (id) on delete restrict on update restrict;
create index ix_simulation_step_link_17 on simulation_step (link_id);
alter table traffic add constraint fk_traffic_originalTraffic_18 foreign key (original_traffic_id) references traffic (id) on delete restrict on update restrict;
create index ix_traffic_originalTraffic_18 on traffic (original_traffic_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table default_map;

drop table deterministic_driver;

drop table driver_info;

drop table link;

drop table matching_result;

drop table node;

drop table simulation;

drop table simulation_result;

drop table simulation_step;

drop table traffic;

SET FOREIGN_KEY_CHECKS=1;

