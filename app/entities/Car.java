package entities;

import models.*;
import util.CarSize;
import util.CarType;
import util.Logger;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Created by werf on 30.09.14.
 */

public class Car {
    private int id;
    private Node currentNode = null;

    private CarType type;
    private CarSize carSize;

    private double latitude;
    private double longitude;

    private double drivenDistance = 0;

    private final SimulationManagerInterface simulationManager;
    private ParkspaceManagerInterface parkspaceManager;


    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);


    public Car(int carId, SimulationManagerInterface simulationManager, ParkspaceManagerInterface parkspaceManager, CarSize carSize, Node startNode) {
        this.id = carId;
        this.parkspaceManager = parkspaceManager;
        this.simulationManager = simulationManager;

        this.carSize = carSize;
        this.currentNode = startNode;
        Position position = startNode.getPosition();
        this.latitude = position.getLatitude();
        this.longitude = position.getLongitude();
    }


    public int getId() {
        return id;
    }

    public CarSize getCarSize() {
        return carSize;
    }

    public CarType getType() {
        return type;
    }

    public void setType(CarType type) {
        this.type = type;
    }

    public Position getPosition() {
        return new Position(this.longitude, this.latitude);
    }

    private void setPosition(Position newPosition) {
        Position oldPosition = new Position(this.longitude, this.latitude);
        this.latitude = newPosition.getLatitude();
        this.longitude = newPosition.getLongitude();
        if(!oldPosition.equals(newPosition)) {
            this.pcs.firePropertyChange("position", oldPosition, newPosition);
        }
    }

    public Node getCurrentNode() {
        return currentNode;
    }

    public void setCurrentNode(Node currentNode) {
        this.currentNode = currentNode;
        this.setPosition(currentNode.getPosition());
    }


    public String toString() {
        if (this.getPosition() != null) {
            return "ID: " + this.getId() + " lat:" + this.getPosition().getLatitude() + " long: " + this.getPosition().getLongitude();
        }
        return "ID: " + this.getId();
    }

    public boolean claimParkspace(Parkspace parkspace) {
        if (this.parkspaceManager != null) {
            boolean success = this.parkspaceManager.acquireParkspace(parkspace);
            if(success) {
                return true;
            }
            Logger.logCar(this, "will not park on parkspace " + this.getCurrentNode().getId() + " reason: parkspace already taken");
        }
        return false;
    }

    public void park(int parkTimeMinutes) {
        int parkTimeSeconds = parkTimeMinutes * 60;
        int parkTimeMillis = new Double(Simulation.convertToSimulationTime(parkTimeSeconds, this.simulationManager.getSimulationSpeed()) * 1000).intValue();
        Logger.logCar(this, "is parking on parkspace #" + this.getCurrentNode().getId() + " for " + parkTimeMillis / 1000 + " simulation seconds, ("+parkTimeSeconds+") real time seconds");
        try {
            Thread.sleep((parkTimeMillis));
        } catch (InterruptedException e) {}
    }

    public void unPark() {
        Node currentNode = this.getCurrentNode();

        if(!currentNode.isParkspace()) {
            System.out.println("car "+this.getId()+" ERROR: wanted to UN-park on a node ("+currentNode.getId()+") that is not a parkspace");
            Thread.dumpStack();
            return;
        }
        Parkspace parkspace = (Parkspace) currentNode;
        this.parkspaceManager.freeParkspace(parkspace);
    }

    void drive(Link link) {
        this.drivenDistance += link.getLength();
        int simulationSpeed = this.simulationManager.getSimulationSpeed();

        //int sleeping = (int)((1F / simulationSpeed) * 200);

        //max speed = 1300 -> sleep time = 1 millisecond
        int granularity = 20;
        int sleeping = new Long((1000L / granularity) / ((long)simulationSpeed / Simulation.DEFAULT_SPEED)).intValue();

        Node startnode = link.getStartNode();
        Node destinationNode = link.getDestinationNode();

        int distance = (int) DefaultMap.getDistance(startnode, destinationNode);

        int speedKmPerH = link.getSpeed();

        double speedMPerS = speedKmPerH / 3.6;

        int stepQuantity = (int) ((distance / speedMPerS) * granularity);

        double currentLat = startnode.getPosition().getLatitude();
        double currentLong = startnode.getPosition().getLongitude();

        double destinationLat = destinationNode.getPosition().getLatitude();
        double destinationLong = destinationNode.getPosition().getLongitude();

        boolean arrived = false;
        boolean arrivedLat = false;
        boolean arrivedLong = false;

        double stepLat;
        double stepLong;
        Position position;

        if (currentLat < destinationLat) {
            double diffLat = destinationLat - currentLat;
            stepLat = (diffLat / stepQuantity);
        } else {
            double diffLat = currentLat - destinationLat;
            stepLat = 0 - ((diffLat / stepQuantity));
        }

        if (currentLong < destinationLong) {
            double diffLong = destinationLong - currentLong;
            stepLong = (diffLong / stepQuantity);
        } else {
            double diffLong = currentLong - destinationLong;
            stepLong = 0 - ((diffLong / stepQuantity));
        }

        do {
            if (stepLat > 0) {
                if (currentLat < destinationLat) {
                    currentLat = currentLat + stepLat;
                } else {
                    arrivedLat = true;
                }
            } else {
                if (destinationLat < currentLat) {
                    currentLat = currentLat + stepLat;
                } else {
                    arrivedLat = true;
                }
            }

            if (stepLong > 0) {
                if (currentLong < destinationLong) {
                    currentLong = currentLong + stepLong;
                } else {
                    arrivedLong = true;
                }

            } else {
                if (destinationLong < currentLong) {
                    currentLong = currentLong + stepLong;
                } else {
                    arrivedLong = true;
                }
            }

            position = new Position(currentLong, currentLat);
            this.setPosition(position);

            try {
                Thread.sleep(sleeping);
            } catch (InterruptedException e) {
                // e.printStackTrace();
                Logger.logCar(this, "stopped thread");
            }

            if (arrivedLat && arrivedLong) {
                arrived = true;
            }
        } while (arrived == false);

        //now car is on target node
        this.setCurrentNode(destinationNode);
    }

    public int getDrivenDistanceRounded() {
        return (int)Math.round(drivenDistance);
    }


    public double getDrivenDistance() {
        return drivenDistance;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

}
