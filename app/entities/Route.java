package entities;

import models.Link;

import java.util.LinkedList;

/**
 * Created by werf on 01.10.14.
 */
public class Route {
    private final LinkedList<Link> links;
    private int duration;             //seconds

    public Route(LinkedList<Link> links) {
        this.links = links;
        calculateDuration();
    }

    private void calculateDuration() {
        double tempDuration = 0;
        for(Link link : this.links) {
            double length = link.getLength();
            int speedKMH = link.getSpeed();
            double speedMS = speedKMH / 3.6;

            tempDuration += length / speedMS;
        }

       this.duration = new Double(tempDuration).intValue();
    }

    public int getDuration() {
        return duration;
    }

    public LinkedList<Link> getLinks() {
        return links;
    }
}
