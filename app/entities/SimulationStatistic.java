package entities;

import models.Link;
import models.Simulation;
import models.results.DriverInfo;
import models.results.SimulationResult;
import models.results.SimulationStep;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by werf on 04.11.14.
 */
public class SimulationStatistic implements SimulationStatisticInterface {

    private ConcurrentHashMap<Integer, Driver> drivers = new ConcurrentHashMap<>();

    private ConcurrentHashMap<Integer, ArrayList<Link>> steps = new ConcurrentHashMap<>();

    private SimulationManagerInterface simulationManager;
    private SimulationResult simulationResult;


    public SimulationStatistic(Simulation simulation, SimulationManagerInterface simulationManager) {
        this.simulationResult = new SimulationResult(simulation);
        this.simulationResult.save();
        this.simulationManager = simulationManager;
    }


    @Override
    public void saveSimulationStep(Driver driver, Link link) {
        if(this.simulationManager.hasFinished()) {
            return;
        }
        int driverId = driver.getId();
        //do not save steps to drive home in statistic
        //note: driver that are null are drivers that are parked at the beginning of simulation run, because they will not be saved to statistics
        if(driver == null || driver.hasFoundParking()) {
            return;
        }

        ArrayList<Link> carSteps;
        if(!this.steps.containsKey(driverId)) {
            carSteps = new ArrayList<>();
            this.steps.put(driverId, carSteps);
        }

        carSteps = this.steps.get(driverId);
        carSteps.add(link);
    }


    @Override
    public void addDriver(Driver driver) {
        if(this.simulationManager.hasFinished()) {
            return;
        }
        this.drivers.put(driver.getId(), driver);
    }

    /**
     * process results, save simulation steps,...
     */
    public void save() {

        for(Driver driver : this.drivers.values()) {
            DriverInfo driverInfo = new DriverInfo(driver, this.simulationResult);
            this.simulationResult.addDriverInfo(driverInfo);

            /*
            if (this.steps.containsKey(driver.getId())) {
                ArrayList<Link> carSteps = this.steps.get(driver.getId());
                for (Link link : carSteps) {
                    if (link != null) {
                        SimulationStep simulationStep = new SimulationStep(driverInfo, link);
                        driverInfo.addSimulationStep(simulationStep);
                    }
                }
            }
            */
            List<Link> driverLinks = driver.getDrivenLinks();
            synchronized (driverLinks) {
                for (Link link : driver.getDrivenLinks()) {
                    SimulationStep simulationStep = new SimulationStep(driverInfo, link);
                    driverInfo.addSimulationStep(simulationStep);
                }
            }
        }


        this.simulationResult.save();

    }
}
