package entities;

import models.Parkspace;

/**
 * Created by werf on 14.10.14.
 */
public interface ParkspaceManagerInterface {

    public boolean acquireParkspace(Parkspace parkspace);

    public void freeParkspace(Parkspace parkspace);

    public boolean isAvaliable(Parkspace parkspace);
}