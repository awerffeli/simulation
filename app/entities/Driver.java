package entities;

import models.*;
import util.*;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by werf on 13.10.14.
 */

public class Driver implements Runnable, SimulationEventDispatcher {

    private final Car car;

    private DriverPreference preference;

    private DriverStatus status;

    protected final MapInterface map;

    private List<DriverListenerInterface> listeners;

    private ParkspaceManagerInterface parkspaceManager;

    private boolean driveDefaultStrategy = true;

    private volatile boolean foundParking = false;

    private List<Link> drivenLinks = Collections.synchronizedList(new ArrayList<Link>()); ;

    private long startSearchTime;
    private long endSearchTime;
    private Parkspace parkspace;

    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private final SimulationManagerInterface simulationManager;

    private final SimulationStatisticInterface simulationStatistic;

    public Driver(Car car, DriverPreference preference, SimulationManagerInterface simulationManager,
                  ParkspaceManagerInterface parkspaceManager, SimulationStatisticInterface simulationStatistic, DriverStatus status) {
        this.car = car;
        this.preference = preference;

        this.listeners = new ArrayList<>();
        this.map = simulationManager.getMap();

        this.status = status;
        this.simulationManager = simulationManager;
        this.parkspaceManager = parkspaceManager;
        this.simulationStatistic = simulationStatistic;
    }

    public int getId() {
        return this.car.getId();
    }

    public Node getCurrentNode() {
        return car.getCurrentNode();
    }

    public Position getPosition() {
        return car.getPosition();
    }


    public void setDriveDefaultStrategy(boolean driveDefaultStrategy) {
        this.driveDefaultStrategy = driveDefaultStrategy;
    }

    public boolean isDriveDefaultStrategy() {
        return driveDefaultStrategy;
    }

    public boolean hasFoundParking() {
        return foundParking;
    }

    public void setFoundParking() {
        this.foundParking = true;
    }

    protected void start() {
        if(this.status == DriverStatus.PARKED) {
            if(this.park()) {
                this.unPark();
            }
            this.driveHome();
        }
        else {
            this.setStartSearchTime();
            this.preference.getStrategy().drive(this);
        }
    }

    public void setStartSearchTime() {
        this.startSearchTime = System.currentTimeMillis();
    }

    public Parkspace getParkspace() {
        return parkspace;
    }

    public long getEndSearchTime() {
        return endSearchTime;
    }

    public long getStartSearchTime() {
        return startSearchTime;
    }

    /**
     * returns how many seconds driver has been looking for a parkspace
     * @return
     */
    public double getParkspaceSearchTime() {
        long start = this.startSearchTime;
        long now = System.currentTimeMillis();
        long diff = now - start;

        return (Simulation.convertFromSimulationTime(diff, this.simulationManager.getSimulationSpeed()) / 1000);
    }

    public boolean driveFastestRouteToTarget(Node targetDestinationNode) {
        Route route = this.getSimulationManager().getMap().getFastestRoute(this.getCurrentNode(), targetDestinationNode);
        return this.driveRoute(route);
    }

    protected SimulationManagerInterface getSimulationManager() {
        return simulationManager;
    }

    public void driveHome() {
        if(this.hasFinished()) {
            return;
        }
        this.setDriveHome();
        MapInterface map = this.simulationManager.getMap();

        Node destinationNode = map.getRandomEdgeNode();
        Route route = map.getFastestRoute(this.car.getCurrentNode(), destinationNode);

        this.driveRoute(route);
        this.simulationManager.notifyDriverEnd();

        this.removeCar(this.car);
        this.end();
    }

    /**
     * returns true if found parking, else false
     * @param route
     * @return
     */
    public boolean driveRoute(Route route) {
        if (route == null) {
            Logger.logDriver(this, "can not get to set target destination reason: no route to " + this.car.getCurrentNode().getId()+" route == null");
            return false;
        }
        else {
            LinkedList<Link> links = route.getLinks();
            if (links == null) {
                Logger.logDriver(this, "can not get to set target destination reason: no route to " + this.car.getCurrentNode().getId()+" links == null");
                return false;
            }
            else if(links.size() <= 0) {
                Logger.logDriver(this, "can not get to set target destination reason: no route to " + this.car.getCurrentNode().getId()+" links size == 0");
                return false;
            }
            else {
                for(Link link : route.getLinks()) {
                    boolean foundParking = this.driveLink(link);
                    if(foundParking) {
                        return true;
                    }
                    if(this.hasFinished()) {
                        return false;
                    }
                }
            }
        }
        Logger.logCar(this.car, "arrived at " + this.car.getCurrentNode().getId());
        return false;
    }

    /**
     * if flag isLookingForParking is set car will stop if finds any parking
     * @param currentDrivingLink
     * @return
     */
    public boolean driveLink(Link currentDrivingLink) {
        //Logger.logDriver(this, "is driving link "+ currentDrivingLink.getId());
        car.drive(currentDrivingLink);
        //this.simulationStatistic.saveSimulationStep(this, currentDrivingLink);
        this.drivenLinks.add(currentDrivingLink);
        //this.recentlyDrivenNodes.add(currentDrivingLink.getDestinationNode());

        if (this.isLookingForParking()) {                   //if the car will be removed after this drive (car is driving home) then do not check for parkspaces anymore
            return this.checkForParkspace();                //this.simulationStatistic.saveSimulationStep(this, currentDrivingLink);
        }
        return false;
    }

    public List<Link> getDrivenLinks() {
        return this.drivenLinks;
    }

    public List<Node> getRecentlyDrivenNodes() {
        List<Node> nodes = new ArrayList<>();
        for(Link link : this.getRecentlyDrivenLinks()) {
            nodes.add(link.getDestinationNode());
        }
        return nodes;
    }

    public List<Link> getRecentlyDrivenLinks() {
        if(this.drivenLinks.size() == 0) {
            return new ArrayList<>();
        }
        //driver remembers 40 recently driven links
        int start = this.drivenLinks.size() - 41;
        if(start < 0) {
            start = 0;
        }

        return this.drivenLinks.subList(start, this.drivenLinks.size()-1);
    }

    /**
     * returns the node the driver was on before the current node
     * @return
     */
    public Node getPreviousVisitedNode() {
        if(this.drivenLinks.size() == 0) {
            return null;
        }
        return this.drivenLinks.get(this.drivenLinks.size()-1).getStartNode();
    }

    public DriverStatus getStatus() {
        return this.status;
    }


    protected boolean checkForParkspace() {
        Node currentNode = this.car.getCurrentNode();
        String logMsg = "";
        if (currentNode.isParkspace()) {
            double parkingToDestination = (int) DefaultMap.getDistance(currentNode, this.preference.getTargetDestinationNode());
            if (this.preference.getMaxWalkDistance() < parkingToDestination) {
                Logger.logDriver(this, "will not park on parkspace " + currentNode.getId() + " reason: too far (" + parkingToDestination + " Meter) from destination node "
                        + this.preference.getTargetDestinationNode().getId());
                return false;
            }
            Parkspace parkspace = (Parkspace) currentNode;
            int parkTime = this.preference.getParkTime();


            if(parkspace.getMaxAllowedParkTime() < parkTime) {
                logMsg = String.format("will not park on parkspace " + currentNode.getId() +
                        " reason: max time to low, wants to park " + this.preference.getParkTime() + " Minutes, parkspace allows " + parkspace.getMaxAllowedParkTime() + " Minutes");
                Logger.logDriver(this, logMsg);
                return false;
            }

            //always pay for a whole hour
            double pricePerHour = parkspace.getPricePerHour();

            if (pricePerHour > this.preference.getMaxWillingToPayForParking()) {
                logMsg = String.format("will not park on parkspace " + currentNode.getId() +
                        " reason: too expensive, willing to pay: " + this.preference.getMaxWillingToPayForParking() + " CHF cost: " + pricePerHour + " CHF");
                Logger.logDriver(this, logMsg);
                return false;
            }
            return this.parkspaceManager.isAvaliable(parkspace);
        }
        return false;
    }

    public int getParkTime() {
        return this.preference.getParkTime();
    }

    public boolean park() {
        Node currentNode = this.car.getCurrentNode();
        if(!currentNode.isParkspace()) {
            System.out.println("driver "+this.getId()+"ERROR: wanted to park on a node ("+currentNode.getId()+") that is not a parkspace");
            Thread.dumpStack();
            return false;
        }
        Parkspace parkspace = (Parkspace) currentNode;
        if(!this.car.claimParkspace(parkspace)) {
            return false;
        }



        this.setParked();

        int parkTime = this.getParkTime();
        this.setParkspace(parkspace);

        this.foundParking = true;
        car.park(parkTime);
        return true;
    }

    public DriverPreference getPreference() {
        return preference;
    }

    public void unPark() {
        this.car.unPark();
    }

    public double getDrivenDistance() {
        return this.car.getDrivenDistance();
    }

    public int getDrivenDistanceRounded() {
        return this.car.getDrivenDistanceRounded();
    }

    public int hashCode() {
        return this.car.getId();
    }

    /*
    private void setTargetDestinationNode(Node targetDestinationNode) {
        Logger.logCar(this.car, "has target "+targetDestinationNode.getId());
        this.targetDestinationNode = targetDestinationNode;
        UpdateDriverDestinationEvent event = new UpdateDriverDestinationEvent(this);
        this.dispatchEvent(event);
    }
    */

    public boolean isLookingForParking() {
        return this.status == DriverStatus.SEARCH;
    }

    @Override
    public void addCarListener(DriverListenerInterface listener) {
        listeners.add(listener);
    }

    @Override
    public void removeCar(Car car) {
        RemoveDriverEvent event = new RemoveDriverEvent(this);
        this.dispatchEvent(event);
    }

    public void setDriving() {
        DriverStatus oldStatus = this.getStatus();
        this.status = DriverStatus.DRIVING;
        this.pcs.firePropertyChange("status", oldStatus, this.status);
    }

    public void setMatched() {
        DriverStatus oldStatus = this.getStatus();
        this.status = DriverStatus.MATCHED;
        this.pcs.firePropertyChange("status", oldStatus, this.status);
    }

    public void setSearching() {
        DriverStatus oldStatus = this.getStatus();
        this.status = DriverStatus.SEARCH;
        this.pcs.firePropertyChange("status", oldStatus, this.status);
    }

    public void setDriveHome() {
        if(this.endSearchTime == 0) {
            this.endSearchTime = System.currentTimeMillis();
        }
        DriverStatus oldStatus = this.getStatus();
        this.status = DriverStatus.HOME;
        this.pcs.firePropertyChange("status", oldStatus, this.status);
    }

    public void setParkspace(Parkspace parkspace) {
        this.parkspace = parkspace;
    }

    public void setParked() {
        this.endSearchTime = System.currentTimeMillis();

        DriverStatus oldStatus = this.getStatus();
        this.status = DriverStatus.PARKED;
        this.pcs.firePropertyChange("status", oldStatus, this.status);
    }

    public CarSize getCarSize() {
        return this.car.getCarSize();
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    private void dispatchEvent(RemoveDriverEvent event) {
        for (DriverListenerInterface l : listeners) {
            l.stateChanged(event);
        }
    }

    private void dispatchEvent(UpdateDriverDestinationEvent event) {
        for (DriverListenerInterface l : listeners) {
            l.stateChanged(event);
        }
    }

    public boolean hasFinished() {
        return this.simulationManager.hasFinished();
    }

    public void end() {
        if(this.endSearchTime == 0) {
            this.endSearchTime = System.currentTimeMillis();
        }
    }

    @Override
    public void run() {
        this.start();
        //Logger.logDriver(this, "thread ended");
    }

    public boolean equals(Object obj) {
        if(obj instanceof Driver) {
            Driver driver = (Driver) obj;
            return this.getId() == driver.getId();
        }
        return false;
    }

    public String toString() {
        StringBuilder value = new StringBuilder();

        value.append("ID: " + this.car.getId());
        return value.toString();
    }

}
