package entities;

/**
 * Created by werf on 14.10.14.
 */
public interface SimulationManagerInterface {

    public boolean hasFinished();

    public int getSimulationSpeed();

    public MapInterface getMap();

    public void notifyDriverEnd();
}