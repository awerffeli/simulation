package entities;

import models.Link;
import models.Node;
import util.Utility;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by werf on 05.11.14.
 */
public class MapArea{

    private List<Link> links;

    private Node lastVisitedNode;

    public MapArea(List<Link> links) {
        this.links = links;
    }

    public List<Link> getLinks() {
        return links;
    }

    public Link getRandomNeighbourLink(Node node) {
        List<Link> neightbourLinks = new LinkedList<>();
        Link backLink = null;
        for(Link link : this.links) {
            //lastVisitedNode prevents that car just drives back and forth
            if(link.getStartNode().equals(node)) {
                if(link.getDestinationNode().equals(lastVisitedNode)) {
                    backLink = link;
                }
                else {
                    neightbourLinks.add(link);
                }
            }
        }
        int count = neightbourLinks.size();
        if(count > 0) {
            Link link = neightbourLinks.get(Utility.random(0, count - 1));
            this.lastVisitedNode = link.getStartNode();
            return link;
        //if its a dead-end-road car sould be able to return
        } else if(backLink != null) {
            return backLink;
        }
        return null;
    }
}
