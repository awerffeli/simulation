package entities;

import entities.strategies.StrategyInterface;
import models.Node;

/**
 * Created by werf on 01.12.14.
 */
public class DriverPreference {
    private int maxWalkDistance;                     //in Meter
    private double maxWillingToPayForParking;           //in CHF
    private int maxSearchTime;                          //in Minutes
    private int parkTime;                               //in Minutes
    private StrategyInterface strategy;
    private Node targetDestinationNode;

    public DriverPreference(int maxWalkingDistance, double maxWillingToPayForParking, int maxSearchTime, int parkTime, StrategyInterface strategy, Node targetDestinationNode) {
        this.maxWalkDistance = maxWalkingDistance;
        this.maxWillingToPayForParking = maxWillingToPayForParking;
        this.maxSearchTime = maxSearchTime;
        this.parkTime = parkTime;
        this.strategy = strategy;
        this.targetDestinationNode = targetDestinationNode;
    }

    public int getMaxWalkDistance() {
        return maxWalkDistance;
    }

    public double getMaxWillingToPayForParking() {
        return maxWillingToPayForParking;
    }

    /**
     * in seconds
     * @return
     */
    public int getMaxSearchTime() {
        return maxSearchTime;
    }

    public int getParkTime() {
        return parkTime;
    }

    public StrategyInterface getStrategy() {
        return strategy;
    }

    public Node getTargetDestinationNode() {
        return targetDestinationNode;
    }
}
