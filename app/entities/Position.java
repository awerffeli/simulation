package entities;

/**
 * Created by werf on 30.09.14.
 */

public class Position {


    private double longitude;           // = x
    private double latitude;            // = y


    public Position(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String toString() {
        return "latitude: " + this.latitude + " longitude: " +
                this.longitude;
    }

    public boolean equals(Position position) {
        if(this.latitude == position.latitude &&
                this.longitude == position.longitude) {
            return true;
        }
        return false;
    }

}
