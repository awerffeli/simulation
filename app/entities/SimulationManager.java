package entities;

import controllers.SimulationController;
import entities.strategies.*;
import entities.strategies.placebook.Placebook;
import models.Node;
import models.Parkspace;
import models.Simulation;
import models.Traffic;
import models.results.MatchingResult;
import util.*;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by werf on 30.09.14.
 */
public class SimulationManager implements Runnable, SimulationManagerInterface, ParkspaceManagerInterface {
    public Map<Integer, Thread> driverThreads = new ConcurrentHashMap<>();

    private Map<Integer, Driver> drivers = new ConcurrentHashMap<>();
    private Map<Integer, Parkspace> parkspaces = new ConcurrentHashMap<>();

    private Map<Parkspace, Boolean> freeParkspacesForStartUp = new HashMap<>();

    private final Simulation simulation;
    private SimulationEventSource eventSource;
    
    private final StrategyInterface luckStrategy, oneTwoBlockStrategy, insideOutsideStrategy;
    private final Placebook placebook;

    private final SimulationChangeListener simulationChangeListener;
    private NewVehiclesDemon newVehiclesDemon;

    private final SimulationStatisticInterface simulationStatistic;
    private Timer timerNewVehicles;

    private int currentCarId = 1;
    private volatile boolean hasFinished = false;


    public SimulationManager(Simulation simulation, SimulationEventSource eventSource) {
        this.simulation = simulation;
        this.eventSource = eventSource;

        //initialize strategies
        this.luckStrategy = new LuckStrategy(simulation.getMap());

        this.oneTwoBlockStrategy = new OneTwoBlockStrategy(simulation.getMap());

        this.insideOutsideStrategy = new InsideOutsideStrategy(simulation.getMap());

        if(this.simulation.getTraffic().getStrategyMatchPercentage() > 0) {
            this.placebook = new Placebook(simulation.getMap(), this);
            Thread threadPlacebook = new Thread(placebook);
            //set higher priotity to placebook for better matching on fast speeds
            threadPlacebook.setPriority(Thread.NORM_PRIORITY+2);
            threadPlacebook.start();
        } else {
            this.placebook = null;
        }


        this.simulationChangeListener = new SimulationChangeListener(this.eventSource);

        this.simulationStatistic = new SimulationStatistic(this.simulation, this);
    }

    public void endSimulation() {
        this.hasFinished = true;
        this.eventSource.end();
        this.simulation.setFinished();
        this.simulation.save();

        for (Driver driver : this.drivers.values()) {
            driver.end();
        }

        if(this.placebook != null) {
            new MatchingResult(this.simulation, placebook).save();
        }

        if (this.newVehiclesDemon != null) {
            this.newVehiclesDemon.cancel();
        }
        if(this.timerNewVehicles != null) {
            this.timerNewVehicles.cancel();
        }

        this.driverThreads = new ConcurrentHashMap<>();
        this.drivers = new ConcurrentHashMap<>();
        this.parkspaces = new ConcurrentHashMap<>();

        SimulationController.removeSimulationManager(this);

        //endaction takes some time to save all statistics
        this.simulationStatistic.save();
        this.eventSource.resultReady();
    }

    @Override
    public void run() {
        this.runSimulation();
    }

    private void runSimulation() {
        this.simulation.save();
        Timer endSimulationTimer = new Timer(false);
        int simulationDurationMillis = this.getSimulation().getEffectiveSimulationDurationSeconds() * 1000;

        EndSimulationTask endSimulationTask = new EndSimulationTask(this);
        endSimulationTimer.schedule(endSimulationTask, simulationDurationMillis);

        Traffic traffic = this.simulation.getTraffic();

        this.loadParkspaces();

        int drivingVehicles = traffic.getDrivingVehiclesCount();
        int parkedVehicles = traffic.getParkedVehiclesCount();

        //Testing Parameters

        /*
        Node startNode = Ebean.find(Node.class).where().eq("id", 1276).setMaxRows(1).findUnique();
        Node targetDestinationNode = Ebean.find(Node.class).where().eq("id", 1137).setMaxRows(1).findUnique();
        this.createCarDriver(startNode, DriverStatus.DRIVING, this.luckStrategy, targetDestinationNode, CarSize.BIG);

        startNode = Ebean.find(Node.class).where().eq("id", 1216).setMaxRows(1).findUnique();
        this.createCarDriver(startNode, DriverStatus.DRIVING, this.oneTwoBlockStrategy, targetDestinationNode, CarSize.BIG);
        */

        int j = 1;
        while (j <= parkedVehicles) {
            this.createParkedCarDriver();
            j++;
        }

        int i = 1;
        while (i <= drivingVehicles) {
            this.createDrivingCarDriver();
            i++;
        }

        //create new cars every 60 seconds
        this.timerNewVehicles = new Timer(true);
        double seconds = Simulation.convertToSimulationTime(60, this.simulation.getSimulationSpeed());
        int milliSeconds = (int) (seconds * 1000);

        this.newVehiclesDemon = new NewVehiclesDemon(this);

        timerNewVehicles.schedule(newVehiclesDemon, milliSeconds, milliSeconds);
    }

    public void setEventSource(SimulationEventSource eventSource) {
        this.eventSource = eventSource;
    }

    //we need to load parkspace now and not reload them from the DB, because we have some transient fields
    private void loadParkspaces() {
        for (Parkspace parkspace : this.simulation.getMap().getParkspaces()) {
            this.parkspaces.put(parkspace.getId(), parkspace);
            this.freeParkspacesForStartUp.put(parkspace, true);
        }
    }

    /*
    public void sendUpdateDriverDestinationEvent(Driver driver) {
        this.eventSource.sendUpdateDestinationEvent(driver);
    }
    */

    private StrategyInterface getRandomStrategy() {
        int random = Utility.random(1, 100);
        Traffic traffic = this.simulation.getTraffic();

        StrategyInterface strategy;
        if (random <= traffic.getStrategyLuckPercentage()) {
            strategy = this.luckStrategy;

        } else if(random <= (traffic.getStrategyLuckPercentage() + traffic.getStrategyOneTwoBlockPercentage())){
            strategy = this.oneTwoBlockStrategy;
        }
        else if(random <= (traffic.getStrategyInsideOutsidePercentage() + traffic.getStrategyLuckPercentage() + traffic.getStrategyOneTwoBlockPercentage())){
            strategy = this.insideOutsideStrategy;
        }
        else {
            strategy = new MatchStrategy(simulation.getMap());
        }
        return strategy;
    }

    private CarSize getRandomCarSize() {
        int random = Utility.random(1, 100);
        Traffic traffic = this.simulation.getTraffic();
        float normalPercentage = traffic.getVehicleNormalPercent();
        float bigPercentage = traffic.getVehicleBigPercent();

        CarSize carSize;
        if (random <= normalPercentage) {
            return CarSize.NORMAL;
        } else if (random <= (normalPercentage+bigPercentage)) {
            carSize = CarSize.BIG;
        } else {
            carSize = CarSize.COMPACT;
        }
        return carSize;
    }

    private void createParkedCarDriver() {
        Node startNode = this.getRandomParkspace();
        //Node targetDestinationNode = this.simulation.getMap().getRandomEdgeNode();v
        StrategyInterface strategy = this.getRandomStrategy();
        CarSize carSize = this.getRandomCarSize();
        this.createCarDriver(startNode, DriverStatus.PARKED, strategy, startNode, carSize);
    }

    protected void createDrivingCarDriver() {
        Node startNode = this.simulation.getMap().getRandomNode();
        //Node targetDestinationNode = this.simulation.getMap().getRandomDestination();
        Node targetDestinationNode = this.simulation.getMap().getRandomDestination();
        StrategyInterface strategy = this.getRandomStrategy();
        CarSize carSize = this.getRandomCarSize();
        this.createCarDriver(startNode, DriverStatus.DRIVING, strategy, targetDestinationNode, carSize);
    }

    /*
    private Driver createCarDriver(DeterministicDriver temporalDriver) {
        return this.createCarDriver(temporalDriver.getStartNode(), temporalDriver.isLeavingSimulation(),
                temporalDriver.getStrategy(), temporalDriver.getTargetDestinationNode(), temporalDriver.getCarSize());
    }
    */

    private Traffic getTraffic() {
        return this.simulation.getTraffic();
    }

    private void createCarDriver(Node startNode, DriverStatus status, StrategyInterface strategy, Node targetDestinationNode, CarSize carSize) {

        Car car = new Car(currentCarId++, this, this, carSize, startNode);

        //set random price between 2.50-5.00 CHF
        double random = Utility.random(this.getTraffic().getRangePayParkingMin(), this.getTraffic().getRangePayParkingMax());
        double maxWillingToPayForParking = (random / 100);

        //set random distance between 300 - 700 meter
        int maxWalkDistance = Utility.random(this.getTraffic().getRangeWalkMin(), this.getTraffic().getRangeWalkMax());

        int parkTime;
        if(status == DriverStatus.PARKED) {
            parkTime = Utility.random(this.getTraffic().getRangeTimeParkedParkingMin(), this.getTraffic().getRangeTimeParkedParkingMax());
        }
        else {
            parkTime = Utility.random(this.getTraffic().getRangeTimeParkingMin(), this.getTraffic().getRangeTimeParkingMax());
        }

        DriverPreference preference = new DriverPreference(maxWalkDistance, maxWillingToPayForParking, this.getTraffic().getMaxParkspaceSearchTime(), parkTime, strategy, targetDestinationNode);

        Driver driver;
        if(strategy instanceof MatchStrategy) {
            driver = new PlacebookDriver(car, preference, this, this, this.simulationStatistic, status, placebook);
            ((MatchStrategy) strategy).setDriver(driver);
        }
        else {
            driver = new Driver(car, preference, this, this, this.simulationStatistic, status);
        }

        driver.addCarListener(this.simulationChangeListener);
        int currentId = car.getId();
        car.addPropertyChangeListener(this.simulationChangeListener);
        driver.addPropertyChangeListener(this.simulationChangeListener);

        this.eventSource.sendNewDriverEvent(driver);

        if(this.hasFinished()) {
            return;
        }

        Thread threadDriver = new Thread(driver);
        threadDriver.setName("DriverThread #" + driver.getId());
        this.driverThreads.put(currentId, threadDriver);
        this.drivers.put(currentId, driver);

        //we do not save statistics for cars that will just leave the simulation
        if(!status.equals(DriverStatus.PARKED)) {
            this.simulationStatistic.addDriver(driver);
        }

        threadDriver.start();
        Logger.logDriver(driver, "has been added to simulation");
    }

    public Simulation getSimulation() {
        return simulation;
    }

    public SimulationEventSource getEventSource() {
        return eventSource;
    }

    @Override
    public boolean acquireParkspace(Parkspace parkspace) {

        int parkspaceId = parkspace.getId();
        if (this.parkspaces.containsKey(parkspaceId)) {

            //we need to get parkspace from Manager because some fields are transient
            Parkspace parkspace2 = this.parkspaces.get(parkspaceId);
            return parkspace2.parkCar();
        }
        return false;
    }

    public void freeParkspace(Parkspace parkspace) {
        int parkspaceId = parkspace.getId();
        if (this.parkspaces.containsKey(parkspaceId)) {

            //we need to get parkspace from Manager because some fields are transient
            Parkspace parkspace2 = this.parkspaces.get(parkspaceId);
            parkspace2.removeCar();
        }
    }

    @Override
    public boolean isAvaliable(Parkspace parkspace) {
        int parkspaceId = parkspace.getId();
        if (this.parkspaces.containsKey(parkspaceId)) {
            Parkspace parkspace2 = this.parkspaces.get(parkspaceId);
            return parkspace2.isAvailable();
        }
        return false;
    }

    /**
     * checks if parkspace is free, so that all parkspaces are filled at simulation start
     * @return Parkspace
     */
    private Parkspace getRandomParkspace() {

        Iterator it = this.freeParkspacesForStartUp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry)it.next();
            Parkspace parkspace = (Parkspace)pairs.getKey();
            Boolean isFree = (Boolean)pairs.getValue();
            if(isFree) {
                pairs.setValue(false);
                return parkspace;
            }
        }

        //else just return a random parkspace that may be already taken
        return this.getMap().getRandomParkspace();
    }



    @Override
    public boolean hasFinished() {
        return this.hasFinished;
    }

    @Override
    public int getSimulationSpeed() {
        return this.simulation.getSimulationSpeed();
    }

    @Override
    public MapInterface getMap() {
        return this.simulation.getMap();
    }

    @Override
    public void notifyDriverEnd() {
        if(!this.hasFinished && this.simulation.getTraffic().isConstantTraffic()) {
            this.createDrivingCarDriver();
        }
    }
}

class EndSimulationTask extends TimerTask {

    private SimulationManager simulationManager;

    public EndSimulationTask(SimulationManager simulationManager) {
        this.simulationManager = simulationManager;
    }

    public void run() {
        this.simulationManager.endSimulation();
    }
}

class NewVehiclesDemon extends TimerTask {

    private SimulationManager simulationManager;

    public NewVehiclesDemon(SimulationManager simulationManager) {
        this.simulationManager = simulationManager;
    }

    public void run() {
        Simulation simulation = simulationManager.getSimulation();
        Traffic traffic = simulation.getTraffic();

        int newVehicles = traffic.getNewVehiclesRate();

        int i = 1;
        while (i <= newVehicles) {
            simulationManager.createDrivingCarDriver();
            i++;
        }
    }
}