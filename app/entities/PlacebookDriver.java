package entities;

import entities.strategies.MatchStrategy;
import entities.strategies.placebook.Match;
import entities.strategies.placebook.PlacebookChecker;
import entities.strategies.placebook.PlacebookInterface;
import entities.strategies.placebook.PlacebookUser;
import models.Node;
import models.Parkspace;
import models.Simulation;
import util.DriverStatus;
import util.Logger;

/**
 * Created by werf on 14.11.14.
 *
 * SEARCH
 * 1. before driving register placebook
 * 2. create search on placebook server
 * 3. drive fastest way to destination
 * 4. check if theres a matched offer
 * 4.1 if there is no matched offer, drive randomly and look for parking
 * 4.2 if there is a match drive to offered parkspace
 * 4.2.1 70 meter before arriving notify that arrived -> set route status to "searcher_ready"
 * 4.2.2 offerer leaves parkspace
 * 4.2.3 park
 * 4.2.4 set route status to "completed"
 *
 * OFFER
 * when parking notify placebook when parkspace will be available again and create offer
 *
 */
public class PlacebookDriver extends Driver implements PlacebookUser {

    public PlacebookInterface placebook;

    private Thread strategyThread;

    private Car car;

    public PlacebookDriver(Car car, DriverPreference preference, SimulationManagerInterface simulationManager, ParkspaceManagerInterface parkspaceManager,
                           SimulationStatisticInterface simulationStatistic, DriverStatus status, PlacebookInterface placebook) {

        super(car, preference, simulationManager, parkspaceManager, simulationStatistic, status);

        this.car = car;
        this.placebook = placebook;
    }

    protected void start() {
        if(this.getStatus() == DriverStatus.PARKED) {
            if(this.park()) {
                this.unPark();
            }
            this.driveHome();
        }
        else {
            this.setStartSearchTime();
            placebook.search(this);

            this.startDefaultStrategy();

            PlacebookChecker pbc = new PlacebookChecker(this, this.placebook, this.getSimulationManager());
            Thread threadPlacebook = new Thread(pbc);
            threadPlacebook.start();
        }
    }

    public void startDefaultStrategy() {
        this.setDriveDefaultStrategy(true);
        MatchStrategy strategy = (MatchStrategy) this.getPreference().getStrategy();
        this.strategyThread = new Thread(strategy);
        this.strategyThread.start();
    }

    @Override
    public Node getTargetDestinationNode() {
        return this.getPreference().getTargetDestinationNode();
    }

    @Override
    public void notifyMatched() {
        super.setMatched();
    }

    @Override
    public int getMaxWalkDistance() {
        return this.getPreference().getMaxWalkDistance();
    }

    public double getMaxWillingToPayForParking() {
        return this.getPreference().getMaxWillingToPayForParking();
    }

    public Thread getStrategyThread() {
        return strategyThread;
    }

    public void setStrategyThread(Thread strategyThread) {
        this.strategyThread = strategyThread;
    }

    public boolean isLookingForParking() {
        return this.getStatus() == DriverStatus.SEARCH || this.getStatus() == DriverStatus.MATCHED;
    }

    public boolean park() {
        Node currentNode = this.getCurrentNode();
        if(!currentNode.isParkspace()) {
            return false;
        }
        Parkspace parkspace = (Parkspace) currentNode;

        if(!this.car.claimParkspace(parkspace)) {
            return false;
        }
        this.setParked();
        this.setParkspace(parkspace);

        Match match = placebook.getMatchForSearcher(this);
        if(match != null) {
            Logger.logPlacebookUser(this, "Match #"+match.getId()+",searcher #"+this.getId()+" offerer "+match.getOffer().getPlacebookUser().getId()+", offer status: " + match.getOffererStatus() + " search status: " + match.getSearcherStatus());
            if(match.getParkspace().equals(parkspace)) {
                placebook.setCompleted(match);
                Logger.logPlacebookUser(this, "parked on MATCHED parkspace: complete");
            }
            else {
                placebook.cancelMatch(match, this);
                Logger.logPlacebookUser(this, "parked on another parkspace: cancel match with offerer " + match.getOffer().getPlacebookUser().getId());
            }
        }

        int parkTime = this.getParkTime();
        placebook.cancelSearch(this);
        placebook.offer(this, parkspace, parkTime * 60);

        this.setFoundParking();
        this.car.park(parkTime);
        return true;
    }

    public void unPark() {
        Match match = placebook.getMatchForOfferer(this);

        //no match just leave parkspace
        if(match == null) {
            placebook.cancelOffer(this);
            super.unPark();
            return;
        }

        int maxWaitTime = 120;
        int waitTime = 0;
        int waitStep = 1;
        long sleepMillis = new Double(Simulation.convertToSimulationTime(waitStep * 1000, this.getSimulationManager().getSimulationSpeed())).longValue();
        while(!this.hasFinished()) {
            if(match.getSearcherStatus() == Match.STATUS_CANCEL) {
                Logger.logDriver(this, "will leave parkspace "+this.getCurrentNode().getId()+", match has been cancelled by searcher "+match.getSearch().getPlacebookUser().getId());
                super.unPark();
                break;
            } else if(match.getSearcherStatus() == Match.STATUS_SEARCHER_READY) {
                super.unPark();
                placebook.setParkspaceReady(match);
                break;
            }
            else if(match.getSearcherStatus() == Match.STATUS_MATCH) {
                try {
                    Thread.sleep(sleepMillis);
                } catch (InterruptedException e) {
                    return;
                }
                waitTime += waitStep;
                if(waitTime % 10 == 0) {
                    Logger.logDriver(this, "is waiting " + waitTime + " (maxWaitTime: " + maxWaitTime + ")for matched searcher #" +
                            match.getSearch().getPlacebookUser().getId() + " to arrive, current searcher status: " + match.getSearcherStatus());
                }
                if (waitTime > maxWaitTime) {
                    Logger.logDriver(this, "will no longer wait for matched car arrive to parkspace");
                    placebook.cancelMatch(match, this);
                    super.unPark();
                    break;
                }
            }
        }

    }



}
