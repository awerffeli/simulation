package entities.strategies.placebook;

import models.Parkspace;

/**
 * Created by werf on 14.11.14.
 */
public class Match {

    private String searcherStatus;
    private String offererStatus;


    private Search search;
    private Offer offer;

    public final static String STATUS_MATCH = "match";
    public final static String STATUS_SEARCHER_READY = "searcher_ready";
    public final static String STATUS_PARKSPACE_READY = "parkspace_ready";
    public final static String STATUS_CANCEL = "cancelled";
    public final static String STATUS_COMPLETE = "completed";

    private final int id;

    private static int currentId = 1;

    public Match(String searcherStatus, String offererStatus,Search search, Offer offer) {
        this.id = currentId++;
        this.searcherStatus = searcherStatus;
        this.offererStatus = offererStatus;
        this.offer = offer;
        this.search = search;
    }


    public Offer getOffer() {
        return offer;
    }

    public Search getSearch() {
        return search;
    }

    public Parkspace getParkspace() {
        return offer.getParkspace();
    }

    public String getSearcherStatus() {
        return searcherStatus;
    }

    public String getOffererStatus() {
        return offererStatus;
    }

    protected void setSearcherStatus(String status) {
        this.searcherStatus = status;
    }

    protected void setOffererStatus(String status) {
        this.offererStatus = status;
    }

    public int getId() {
        return id;
    }

    public String toString() {

        StringBuilder value = new StringBuilder();

        value.append("Match ");
        value.append("id " + id);
        value.append(" searcher " + this.getSearch().getPlacebookUser().getId());
        value.append(" offerer " + this.getOffer().getPlacebookUser().getId());

        return value.toString();
    }

    public boolean isOpen() {
        if((this.searcherStatus.equals(Match.STATUS_MATCH) || this.searcherStatus.equals(Match.STATUS_PARKSPACE_READY) || this.searcherStatus.equals(Match.STATUS_SEARCHER_READY)) &&
                (this.offererStatus.equals(Match.STATUS_MATCH) || this.offererStatus.equals(Match.STATUS_PARKSPACE_READY) || this.offererStatus.equals(Match.STATUS_SEARCHER_READY))) {
            return true;
        }
        return false;
    }
}