package entities.strategies.placebook;

import models.Node;

/**
 * Created by werf on 17.11.14.
 */
public class Search {
    private PlacebookUser placebookUser;
    private Node destination;
    private int maxWalkingDistance;
    private int parkTime;
    private String status = Placebook.STATUS_RUNNING;

    Search(PlacebookUser placebookUser) {
        this.placebookUser = placebookUser;
        this.destination = placebookUser.getTargetDestinationNode();
        this.maxWalkingDistance = placebookUser.getMaxWalkDistance();
        this.parkTime = placebookUser.getParkTime();
    }

    public int getMaxWalkingDistance() {
        return maxWalkingDistance;
    }

    public int getParkTime() {
        return parkTime;
    }

    public PlacebookUser getPlacebookUser() {
        return placebookUser;
    }

    public String getStatus() {
        return status;
    }

    public void cancel() {
        this.status = Placebook.STATUS_CANCEL;
    }
}
