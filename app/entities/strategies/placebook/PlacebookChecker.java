package entities.strategies.placebook;

import entities.PlacebookDriver;
import entities.SimulationManagerInterface;
import models.Node;
import models.Simulation;
import util.Logger;

/**
 * Created by werf on 14.11.14.
 * PlacebookChecker represents the device with the placebook app installed
 */
public class PlacebookChecker implements Runnable{
    private final PlacebookDriver driver;
    private final PlacebookInterface placebook;
    private final SimulationManagerInterface simulationManager;

    public PlacebookChecker(PlacebookDriver driver, PlacebookInterface placebook, SimulationManagerInterface simulationManager) {
        this.driver = driver;
        this.placebook = placebook;
        this.simulationManager = simulationManager;
    }

    public void run() {
        while(!driver.hasFinished() && !driver.hasFoundParking()) {
            Match match = placebook.getMatchForSearcher(driver);

            if(match != null) {
                //stop normal strategy
                driver.setDriveDefaultStrategy(false);
                try {
                    driver.getStrategyThread().join();
                } catch (InterruptedException e) {}

                Logger.logPlacebookUser(driver, "will now drive to parkspace #" + match.getOffer().getParkspace().getId());

                Node targetNode = match.getParkspace();

                boolean foundAnotherParkingOnTheWay = driver.driveFastestRouteToTarget(targetNode);

                if(foundAnotherParkingOnTheWay && driver.park()) {
                    driver.unPark();
                    driver.driveHome();
                    return;
                }

                placebook.setSearcherReady(match);

                int maxWaitTime = 120;
                int waitTime = 0;
                int waitStep = 1;

                long sleepMillis = new Double(Simulation.convertToSimulationTime(waitStep * 1000, this.simulationManager.getSimulationSpeed())).longValue();
                while(!driver.hasFinished() && !driver.hasFoundParking()) {

                    if(match.getOffererStatus() == Match.STATUS_CANCEL) {
                        Logger.logPlacebookUser(driver, "offer has been canceled by other driver, starting new placebook search");
                        //start new search
                        placebook.search(driver);
                        restartDefaultStrategy();
                        break;
                    }
                    else if(match.getOffererStatus() == Match.STATUS_MATCH) {
                        waitTime += waitStep;
                        try {
                            if(waitTime % 10 == 0) {
                                Logger.logPlacebookUser(driver, "is waiting " + waitTime + " (maxWaitTime: " + maxWaitTime + ")for matched offerer #" +
                                        match.getOffer().getPlacebookUser().getId() + " to free parkspace #" + match.getParkspace().getId()+", current offer status: " + match.getOffererStatus());
                            }
                            Thread.sleep(sleepMillis);
                        } catch (InterruptedException e) {
                            return;
                        }

                        if(waitTime > maxWaitTime) {
                            Logger.logPlacebookUser(driver, "will no longer wait for matched car to free parking");
                            placebook.cancelMatch(match, driver);

                            restartDefaultStrategy();
                            break;
                        }
                    }
                    else if(match.getOffererStatus() == Match.STATUS_PARKSPACE_READY) {
                        boolean foundParking = driver.park();
                        if(foundParking) {
                            driver.unPark();
                            driver.driveHome();
                            return;          //thread ends
                        } else {
                            Logger.logPlacebookUser(driver, "wanted to park on offered parkspace #"+match.getParkspace().getId()+" but it was already taken");
                            placebook.cancelMatch(match, driver);

                            //start new search
                            placebook.search(driver);
                            restartDefaultStrategy();
                            break;
                        }
                    }
                    else {
                        System.err.println("driver "+driver.getId()+" Should not happen match offer status: "+match.getOffererStatus());
                    }
                }
            }
        }
    }

    private void restartDefaultStrategy() {
        Logger.logPlacebookUser(driver, "restarting default strategy");
        driver.startDefaultStrategy();
    }
}
