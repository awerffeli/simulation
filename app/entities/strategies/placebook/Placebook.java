package entities.strategies.placebook;

import entities.MapInterface;
import entities.SimulationManagerInterface;
import models.DefaultMap;
import models.Node;
import models.Parkspace;
import models.Simulation;
import util.Logger;

import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by werf on 12.11.14.
 */

public class Placebook implements PlacebookInterface, Runnable{


    public final static String STATUS_RUNNING = "running";
    public final static String STATUS_CANCEL = "cancelled";
    private final SimulationManagerInterface simulationManager;

    private MapInterface map;
    private Queue<Offer> offers = new ConcurrentLinkedQueue<>();
    private Queue<Search> searches = new ConcurrentLinkedQueue<>();
    private Map<Integer, Match> matches = new ConcurrentHashMap<>();

    private final int ARRIVAL_THRESHOLD_SEC = 120;

    private int searchCount = 0;
    private int offerCount = 0;
    private int matchCount = 0;
    private int cancelMatchCount = 0;
    private int successfulMatchCount = 0;
    private int cancelSearchCount = 0;
    private int cancelOfferCount = 0;
    private int noMatchThreshold;
    private int noMatchWalkdistance;
    private int noMatchPrice;

    public Placebook(MapInterface map, SimulationManagerInterface simulationManager) {
        this.simulationManager = simulationManager;
        this.map = map;
    }

    @Override
    public void run() {
        this.startMatching();
    }

    private void startMatching() {

        double arrivalThresholdConverted = Simulation.convertToSimulationTime(this.ARRIVAL_THRESHOLD_SEC, this.simulationManager.getSimulationSpeed());

        while(!this.simulationManager.hasFinished()) {
            Iterator<Search> searchIterator = this.searches.iterator();

            while(searchIterator.hasNext()) {
                Search search = searchIterator.next();
                Match match;
                PotencialMatch potencialMatch = null;
                Iterator<Offer> offerIterator = this.offers.iterator();

                while(offerIterator.hasNext()) {

                    if(this.simulationManager.hasFinished()) {
                        this.printResults();
                        return;
                    }

                    Offer offer = offerIterator.next();
                    Parkspace parkspace = offer.getParkspace();
                    double parkspacePrice = parkspace.getPricePerHour();
                    double maxWillingToPay = search.getPlacebookUser().getMaxWillingToPayForParking();

                    if(parkspacePrice <= maxWillingToPay) {

                        int maxAllowedParkTime = parkspace.getMaxAllowedParkTime();
                        int parkTime = search.getParkTime();

                        if(parkTime <= maxAllowedParkTime) {
                            int walkDistance = this.getWalkDistance(search, offer);
                            if (walkDistance < search.getMaxWalkingDistance()) {


                                int driveDuration = this.getDriveDuration(search, offer);
                                double driveDurationConverted = Simulation.convertToSimulationTime(driveDuration, this.simulationManager.getSimulationSpeed());

                                int offerSecondsRemaining = offer.getSecondsRemaining();

                                double offerSecondsRemainingConverted = Simulation.convertToSimulationTime(offerSecondsRemaining, this.simulationManager.getSimulationSpeed());

                                double threshold = offerSecondsRemainingConverted - driveDurationConverted;

                                if (threshold > 0 && threshold < arrivalThresholdConverted) {

                                    int walkDuration = this.getWalkDuration(walkDistance);
                                    double walkDurationConverted = Simulation.convertToSimulationTime(walkDuration, this.simulationManager.getSimulationSpeed());
                                    double totalDuration = driveDurationConverted + walkDurationConverted;

                                    if (potencialMatch == null || totalDuration < potencialMatch.getTotalDuration()) {
                                        if (search.getStatus().equals(Placebook.STATUS_RUNNING) && offer.getStatus().equals(Placebook.STATUS_RUNNING)) {
                                            potencialMatch = new PotencialMatch(search, offer, totalDuration);
                                        }
                                    }
                                } else {
                                    /*
                                    System.out.println("driveDuration: "+driveDuration);
                                    System.out.println("offerSecondsRemaining: "+offerSecondsRemaining);
                                    System.out.println("threshold: "+threshold);
                                    */
                                    this.noMatchThreshold++;
                                }
                            } else {
                                this.noMatchWalkdistance++;
                            }
                        }
                    } else {
                        this.noMatchPrice++;
                    }
                }

                //here check if potencialMatch and create match (match best offer for searcher)
                //when match created go on with next search and find best offer for this one
                if(potencialMatch != null) {
                    match = new Match(Match.STATUS_MATCH, Match.STATUS_MATCH, potencialMatch.getSearch(), potencialMatch.getOffer());
                    match.getOffer().getPlacebookUser().notifyMatched();
                    match.getSearch().getPlacebookUser().notifyMatched();

                    Logger.logPlacebook("NEW Match: searcher #" + match.getSearch().getPlacebookUser().getId() + " offerer #" + match.getOffer().getPlacebookUser().getId());
                    this.matchCount++;
                    this.matches.put(match.getId(), match);
                    searchIterator.remove();

                    //cannot be removed by iterator as we are outside offers loop
                    this.offers.remove(match.getOffer());
                }
            }

            int sleepSeconds = 5;
            long sleepMillis = new Double(Simulation.convertToSimulationTime(sleepSeconds * 1000, this.simulationManager.getSimulationSpeed())).longValue();
            try {
                Thread.sleep(sleepMillis);
            } catch (InterruptedException e) {}
        }
        this.printResults();

    }

    private void printResults() {
        System.out.println("noMatchThreshold "+noMatchThreshold);
        System.out.println("noMatchWalkdistance "+noMatchWalkdistance);
        System.out.println("noMatchPrice "+noMatchPrice);
    }

    private int getWalkDistance(Search search, Offer offer) {
        Node target = search.getPlacebookUser().getTargetDestinationNode();
        Node parkspace = offer.getPlacebookUser().getCurrentNode();
        return new Double(DefaultMap.getDistance(parkspace, target)).intValue();
    }

    private int getWalkDuration(double distance) {
        //walkspeed: 5km/h = 1.4m/s
        double walkDuration = distance / 1.4;
        return new Double(walkDuration).intValue();
    }

    private int getDriveDuration(Search search, Offer offer) {
        Node currentNode = search.getPlacebookUser().getCurrentNode();
        Node parkspace = offer.getPlacebookUser().getCurrentNode();
        return map.getFastestRoute(currentNode, parkspace).getDuration();
    }

    @Override
    public void search(PlacebookUser placebookUser) {
        Logger.logPlacebookUser(placebookUser, "new search, target " + placebookUser.getTargetDestinationNode().getId());
        this.searches.add(new Search(placebookUser));
        this.searchCount++;
    }

    @Override
    public void offer(PlacebookUser placebookUser, Parkspace parkspace, int expiration) {
        Logger.logPlacebookUser(placebookUser, "new offer on #" + parkspace.getId() + ", expiration " + expiration);

        this.offers.add(new Offer(placebookUser, parkspace, expiration));
        this.offerCount++;
    }

    @Override
    public Match getMatchForSearcher(PlacebookUser placebookUser) {
        for(Match match : this.matches.values()) {
            if(match.getSearch().getPlacebookUser().equals(placebookUser) && match.isOpen()) {
                return match;
            }
        }
        return null;
    }

    @Override
    public Match getMatchForOfferer(PlacebookUser placebookUser) {
        for(Match match : this.matches.values()) {
            if(match.getOffer().getPlacebookUser().equals(placebookUser) && match.isOpen()) {
                return match;
            }
        }
        return null;
    }

    public void cancelSearch(PlacebookUser placebookUser) {
        for(Search search : this.searches) {
            if(search.getPlacebookUser().equals(placebookUser)) {
                search.cancel();
                this.searches.remove(search);
            }
        }
    }

    public void cancelOffer(PlacebookUser placebookUser) {
        for(Offer offer : this.offers) {
            if(offer.getPlacebookUser().equals(placebookUser)) {
                offer.cancel();
                this.offers.remove(offer);
            }
        }
    }

    public void cancelMatch(Match match, PlacebookUser placebookUser){
        this.cancelMatchCount++;
        if(match.getSearch().getPlacebookUser().equals(placebookUser)) {
            this.cancelMatchBySearcher(match);
        }
        else if(match.getOffer().getPlacebookUser().equals(placebookUser)){
            this.cancelMatchByOfferer(match);
        }
    }


    private void cancelMatchBySearcher(Match match) {
        match.setSearcherStatus(Match.STATUS_CANCEL);
        this.matches.remove(match.getId());
        this.cancelSearchCount++;
    }

    private void cancelMatchByOfferer(Match match) {
        match.setOffererStatus(Match.STATUS_CANCEL);
        this.matches.remove(match.getId());
        this.cancelOfferCount++;
    }

    @Override
    public void setSearcherReady(Match match) {
        match.setSearcherStatus(Match.STATUS_SEARCHER_READY);
    }

    public void setParkspaceReady(Match match) {
        match.setOffererStatus(Match.STATUS_PARKSPACE_READY);
    }

    @Override
    public void setCompleted(Match match) {
        Search search = match.getSearch();
        Offer offer = match.getOffer();
        match.setSearcherStatus(Match.STATUS_COMPLETE);
        match.setOffererStatus(Match.STATUS_COMPLETE);
        Logger.logPlacebook("removed completed match searcher #" + search.getPlacebookUser().getId() + " offerer #" + offer.getPlacebookUser().getId());
        this.matches.remove(match);
        this.successfulMatchCount++;
    }

    public int getCancelSearchCount() {
        return cancelSearchCount;
    }

    public int getCancelOfferCount() {
        return cancelOfferCount;
    }

    public int getSuccessfulMatchCount() {
        return successfulMatchCount;
    }

    public int getMatchCount() {
        return matchCount;
    }

    public int getOfferCount() {
        return offerCount;
    }

    public int getSearchCount() {
        return searchCount;
    }

    public int getCancelMatchCount() {
        return cancelMatchCount;
    }


}

class PotencialMatch {
    private Search search;
    private Offer offer;
    private double totalDuration;

    PotencialMatch(Search search, Offer offer, double totalDuration) {
        this.search = search;
        this.offer = offer;
        this.totalDuration = totalDuration;
    }

    public Search getSearch() {
        return search;
    }

    public Offer getOffer() {
        return offer;
    }

    public double getTotalDuration() {
        return totalDuration;
    }


}