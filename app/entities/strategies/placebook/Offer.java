package entities.strategies.placebook;

import models.Parkspace;

import java.util.Date;

/**
 * Created by werf on 17.11.14.
 */
public class Offer {
    private PlacebookUser placebookUser;
    private Parkspace parkspace;
    private Date expiration;     //seconds
    private String status;

    Offer(PlacebookUser placebookUser, Parkspace parkspace, int expiration) {
        this.placebookUser = placebookUser;
        this.parkspace = parkspace;
        this.status = Placebook.STATUS_RUNNING;

        int expiration_millis = expiration*1000;
        this.expiration = new Date(new Date().getTime()+expiration_millis);
    }

    public Parkspace getParkspace() {
        return parkspace;
    }

    public int getSecondsRemaining() {
        Date now = new Date();
        long diff =  expiration.getTime() - now.getTime();
        long diffSeconds = diff / 1000;

        return new Long(diffSeconds).intValue();
    }

    public PlacebookUser getPlacebookUser() {
        return placebookUser;
    }

    public String getStatus() {
        return status;
    }

    public void cancel() {
        this.status = Placebook.STATUS_CANCEL;
    }
}