package entities.strategies.placebook;

import models.Parkspace;

/**
 * Created by werf on 17.11.14.
 */
public interface PlacebookInterface {

    public void search(PlacebookUser user);

    public void offer(PlacebookUser user, Parkspace parkspace,int expiration);

    public Match getMatchForSearcher(PlacebookUser user);

    public Match getMatchForOfferer(PlacebookUser user);


    public void setSearcherReady(Match match);

    public void setCompleted(Match match);

    public void setParkspaceReady(Match match);


    void cancelSearch(PlacebookUser user);

    void cancelOffer(PlacebookUser user);

    public void cancelMatch(Match match, PlacebookUser placebookUser);

}
