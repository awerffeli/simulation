package entities.strategies.placebook;

import models.Node;

/**
 * Created by werf on 08.12.14.
 */
public interface PlacebookUser {

    public int getId();

    public Node getCurrentNode();

    public Node getTargetDestinationNode();

    public void notifyMatched();

    public int getMaxWalkDistance();

    public double getMaxWillingToPayForParking();

    public int getParkTime();
}
