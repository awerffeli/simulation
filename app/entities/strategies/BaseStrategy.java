package entities.strategies;

import entities.Driver;
import entities.DriverPreference;
import entities.Route;
import models.DefaultMap;
import models.Link;
import models.Node;
import util.Logger;
import util.Utility;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by werf on 05.11.14.
 */
public abstract class BaseStrategy implements StrategyInterface{

    protected DefaultMap map;
    protected final int increaseThreshold = 120;                //seconds
    private final int turnAroundThreshold = 50;

    //@todo: in whole method catch ArrayIndexOutOfBoundException, print and print Stacktrace
    protected Link getNextDrivingLink(Driver driver, int searchRadiusMin, int searchRadiusMax) {
        List<Link> links = this.map.getNeighbourLinks(driver.getCurrentNode());
        Node lastDrivenNode = driver.getPreviousVisitedNode();

        DriverPreference preference = driver.getPreference();
        double currentDistanceToDestination = DefaultMap.getDistance(driver.getCurrentNode(), preference.getTargetDestinationNode());

        //should not happen
        if (links == null || links.size() == 0) {
   //         Logger.logDriver(driver, "ERROR: is on NO-WAY street current node: " + driver.getCurrentNode().getId());
            return null;
        }

        //one way street, drive back
        if (links.size() == 1) {
    //        Logger.logDriver(driver, "is on ONE-WAY street current node: " + driver.getCurrentNode().getId());
            return links.get(0);
        }

        //normal street
        if (links.size() == 2) {
            //if driving on a normal street and driver is passing searchRadiusMax plus a threshold turn around and drive back
            if (currentDistanceToDestination > searchRadiusMax + this.turnAroundThreshold) {
         //       Logger.logDriver(driver, "is TURNing AROUND, reason: too far for searchRadiusMax of " + searchRadiusMax + " current distance " + currentDistanceToDestination);
                return this.map.getClosestLink(preference.getTargetDestinationNode(), links);
            }

            if (currentDistanceToDestination < searchRadiusMin - this.turnAroundThreshold) {
          //      Logger.logDriver(driver, "is TURNing AROUND, reason: too close for searchRadiusMin of " + searchRadiusMin + " current distance " + currentDistanceToDestination);
                return this.map.getFarthestLink(preference.getTargetDestinationNode(), links);
            }

            //drive street random (rare case where lastDrivenNode would not be set) for example if directly placed at target
            if (lastDrivenNode == null) {
                //Logger.logDriver(driver, "is on NORMAL STREET current node: "+driver.getCurrentNode().getId()+" lastDrivenNode == null");
                return this.getMatchingLink(driver, links);
            }
            for (Link link : links) {
                //Logger.logDriver(driver, "is on NORMAL STREET current node: "+driver.getCurrentNode().getId()+" lastDrivenNode == "+lastDrivenNode.getId());
                if (!link.getDestinationNode().equals(lastDrivenNode)) {
                    return link;
                }
            }
        }

        //if we get here we have serveral options
        if (currentDistanceToDestination > searchRadiusMax) {
        //    Logger.logDriver(driver, "will drive CLOSER to target, reason is too far for searchRadiusMax " + searchRadiusMax + " current distance " + currentDistanceToDestination);
            return this.getClosestLink(driver, links);
        } else if (currentDistanceToDestination < searchRadiusMin) {
         //   Logger.logDriver(driver, "will drive FARTHER away from target, reason is too close for searchRadiusMin " + searchRadiusMin + " current distance " + currentDistanceToDestination);
            return this.getFarthestLink(driver, links);
        } else {
            //Logger.logDriver(driver, "will drive RANDOM (not recetly driven) street, reason: is ok in search searchRadiusMin "+searchRadiusMin +" searchRadiusMax "+searchRadiusMax+" current distance "+currentDistanceToDestination);
            //driver is in searching-area pick random link that was not driven recently
            return this.getMatchingLink(driver, links);
        }
    }

    /**
     * prevents driving same link back
     * @param links
     * @return
     */
    private Link getClosestLink(Driver driver, List<Link> links) {
        Node previousVisitedNode = driver.getPreviousVisitedNode();

        if (previousVisitedNode != null) {
            Iterator<Link> iterator = links.iterator();
            while (iterator.hasNext()) {
                Link link = iterator.next();
                if (link.getDestinationNode().equals(previousVisitedNode)) {
                    iterator.remove();
                }
            }
        }
        return this.map.getClosestLink(driver.getPreference().getTargetDestinationNode(), links);
    }

    /**
     * prevents driving same link back
     * @param links
     * @return
     */
    private Link getFarthestLink(Driver driver, List<Link> links) {
        Node previousVisitedNode = driver.getPreviousVisitedNode();

        if (previousVisitedNode != null) {
            Iterator<Link> iterator = links.iterator();
            while (iterator.hasNext()) {
                Link link = iterator.next();
                if (link.getDestinationNode().equals(previousVisitedNode)) {
                    iterator.remove();
                }
            }
        }
        return this.map.getFarthestLink(driver.getPreference().getTargetDestinationNode(), links);
    }

    /**
     * return a random link that has not been driven by the driver recently
     * if all have been driven recently return a random link (but do not drive back)
     * @param links
     * @return
     */
    private Link getMatchingLink(Driver driver, List<Link> links) {

        List<Node> recentlyDrivenNodes = driver.getRecentlyDrivenNodes();
        List<Link> notRecentlyDrivenLinks = new ArrayList<>();

        for(Link link : links) {
            if(!recentlyDrivenNodes.contains(link.getDestinationNode())) {
                notRecentlyDrivenLinks.add(link);
            }
        }

        //get random link that has not been driven recently
        if(notRecentlyDrivenLinks.size() > 0) {
            //Logger.logDriver(driver, "there are "+notRecentlyDrivenLinks.size()+" links that havent been driven before. driving random");
            return notRecentlyDrivenLinks.get(Utility.random(0, notRecentlyDrivenLinks.size() - 1));
        }

        //prevent driving back (remove link where we came from)
        Node previousVisitedNode = driver.getPreviousVisitedNode();
        if(previousVisitedNode != null) {
            Iterator<Link> iterator = links.iterator();
            while (iterator.hasNext()) {
                Link link = iterator.next();
                if (link.getDestinationNode().equals(previousVisitedNode)) {
                    iterator.remove();
                }
            }
        }
        //if all links have recently been driven recently just drive random link
        return links.get(Utility.random(0, notRecentlyDrivenLinks.size() - 1));
    }

    /**
     * @param driver
     * @param searchRadiusMin
     * @param searchRadiusMax
     */
    protected void findParkspaceInArea(Driver driver, int searchRadiusMin, int searchRadiusMax, int increaseRadiusMin, int increaseRadiusMax) {
        DriverPreference preference = driver.getPreference();

        double parkspaceSearchTime;
        double maxSearchTime = preference.getMaxSearchTime() * 60;     //convert to seconds

        double increateOnTreshold = driver.getParkspaceSearchTime() + this.increaseThreshold;

        //Logger.logDriver(driver, "is now looking for a parkspace in the radius of min " + searchRadiusMin + " m, and max "+searchRadiusMax);
        do {
            parkspaceSearchTime = driver.getParkspaceSearchTime();
            if(maxSearchTime < driver.getParkspaceSearchTime()) {
             //   Logger.logDriver(driver, "has driven for " + parkspaceSearchTime + " seconds searching for parkspace, will stop looking and go home");
                driver.driveHome();
                return;
            }
            if(driver.hasFinished()) {
                return;
            }

            Link link = this.getNextDrivingLink(driver, searchRadiusMin, searchRadiusMax);
            if(driver.hasFinished()) {
                return;
            }

            boolean foundParking = driver.driveLink(link);

            if(foundParking && driver.park()) {
                driver.unPark();
                driver.driveHome();
                return;
            }

            if(!driver.hasFinished() && ((parkspaceSearchTime > increateOnTreshold) && searchRadiusMax < preference.getMaxWalkDistance())) {
                increateOnTreshold = driver.getParkspaceSearchTime() + this.increaseThreshold;
                searchRadiusMin = searchRadiusMin + increaseRadiusMin;
                searchRadiusMax = searchRadiusMax + increaseRadiusMax;
                Logger.logDriver(driver, "drove "+parkspaceSearchTime+" seconds without finding parkspace will increase radius to "+searchRadiusMax+" m");
            }

        } while(!driver.hasFinished() && !driver.hasFoundParking() && driver.isDriveDefaultStrategy());

        if(!driver.isDriveDefaultStrategy()) {
            Logger.logDriver(driver, " stopped default strategy");
            return;
        }
    }

    protected void driveTowardsTarget(Driver driver, int stopWhenDistance) {
        DriverPreference preference = driver.getPreference();

        Node targetDestinationNode = preference.getTargetDestinationNode();
        Route route = map.getFastestRoute(driver.getCurrentNode(), targetDestinationNode);
        for(Link link : route.getLinks()) {
            driver.driveLink(link);
            if(!driver.isDriveDefaultStrategy() || driver.hasFinished()) {
                return;
            }

            //already before arriving on destination look for parking
            if(DefaultMap.getDistance(link.getDestinationNode(), targetDestinationNode) <= stopWhenDistance) {
                break;
            }
        }
    }
    
}
