package entities.strategies;

import entities.Driver;
import models.DefaultMap;
import util.Logger;


/**
 * Created by werf on 08.10.14.
 */

public class MatchStrategy extends OneTwoBlockStrategy implements StrategyInterface, Runnable {
    private Driver driver;

    public MatchStrategy(DefaultMap map) {
        super(map);
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    @Override
    public void drive(Driver driver) {
        double maxSearchTime = driver.getPreference().getMaxSearchTime() * 60;     //convert to seconds
        if(driver.getParkspaceSearchTime() >  maxSearchTime) {  //seconds
            Logger.logDriver(driver, "has driven " + driver.getParkspaceSearchTime() + " minutes searching for parkspace, will stop looking and go home");
            driver.driveHome();
            return;
        }
        super.drive(driver);
    }

    @Override
    public String getColor() {
        return "blue";
    }

    @Override
    public String getName() {
        return "Match";
    }


    @Override
    public void run() {
        this.drive(this.driver);
    }
}