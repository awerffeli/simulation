package entities.strategies;

import entities.Driver;

/**
 * Created by werf on 08.10.14.
 */
public interface StrategyInterface {

    public String getName();

    public void drive(Driver driver);

    public String getColor();
}
