package entities.strategies;

import entities.Driver;
import entities.Route;
import models.DefaultMap;
import models.Node;
import util.Logger;

/**
 * Created by werf on 08.10.14.
 */
public class ShortestPathStrategy implements StrategyInterface {

    private DefaultMap map;

    public ShortestPathStrategy(DefaultMap map) {
        this.map = map;
    }

    @Override
    public String getName() {
        return "Shortest Path";
    }

    /**
     * 1. set random target
     * 2. get route by strategy (e.g. fastest route to destination)
     * 3. drive route and check on every node whether its a parkspace
     * 4. if its a parkspace check price and distance to final destination of driver
     * (4.1 if its a match park for some time)
     * 5.1 theres a small change that driver will drive to edge and will be removed
     * 5.2 in most of the cases driver will drive to a new random destination and start over from 1.
     */
    @Override
    public void drive(Driver driver) {
        Node destinationNode = driver.getPreference().getTargetDestinationNode();
        Route route;
        boolean foundParking;

        do {
            route = this.map.getFastestRoute(driver.getCurrentNode(), destinationNode);
            Logger.logDriver(driver, "has new destination Node: " + destinationNode.getId());
            foundParking = driver.driveRoute(route);

            destinationNode = this.map.getRandomDestination();

        } while(driver.isLookingForParking() && !foundParking);

    }

    @Override
    public String getColor() {
        return "red";
    }


}