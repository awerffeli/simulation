package entities.strategies;

import entities.Driver;
import models.DefaultMap;

/**
 * Created by werf on 08.10.14.
 */
//used by: 37%
public class LuckStrategy extends BaseStrategy {

    public LuckStrategy(DefaultMap map) {
        this.map = map;
    }

    @Override
    public void drive(Driver driver) {
        int searchRadiusMin = 0;                   //meter
        int searchRadiusMax = 120;                  //meter

        driver.setDriving();
        this.driveTowardsTarget(driver, searchRadiusMin);
        if(!driver.isDriveDefaultStrategy()) {
            return;
        }
        driver.setSearching();                      //start looking for parking only when arrived at destination

        this.findParkspaceInArea(driver, searchRadiusMin, searchRadiusMax, 0, 100);
    }

    @Override
    public String getColor() {
        return "orange";
    }

    @Override
    public String getName() {
        return "Luck";
    }

}