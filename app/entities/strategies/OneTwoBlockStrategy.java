package entities.strategies;

import entities.Driver;
import models.DefaultMap;

/**
 * Created by werf on 08.10.14.
 */

public class OneTwoBlockStrategy extends BaseStrategy {

    public OneTwoBlockStrategy(DefaultMap map) {
        this.map = map;
    }

    @Override
    public void drive(Driver driver) {
        int searchRadiusMin = 100;                  //meter
        int searchRadiusMax = 220;                  //meter
        int stopWhenDistance = (searchRadiusMax + searchRadiusMin) / 2;

        driver.setDriving();
        this.driveTowardsTarget(driver, stopWhenDistance);
        if(!driver.isDriveDefaultStrategy()) {
            return;
        }
        driver.setSearching();

        this.findParkspaceInArea(driver, searchRadiusMin, searchRadiusMax, 0, 100);
    }

    @Override
    public String getColor() {
        return "green";
    }

    @Override
    public String getName() {
        return "1-2-Block";
    }



}