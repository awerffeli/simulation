package entities.strategies;

import entities.Driver;
import models.DefaultMap;

/**
 * Created by werf on 08.10.14.
 *
 */
public class InsideOutsideStrategy extends BaseStrategy {

    public InsideOutsideStrategy(DefaultMap map) {
        this.map = map;
    }

    @Override
    public void drive(Driver driver) {
        int searchRadiusMin = 0;                  //meter
        int searchRadiusMax = 120;                  //meter

        driver.setDriving();
        this.driveTowardsTarget(driver, searchRadiusMin);
        if(!driver.isDriveDefaultStrategy()) {
            return;
        }
        driver.setSearching();                      //start looking for parking only when arrived at destination

        this.findParkspaceInArea(driver, searchRadiusMin, searchRadiusMax, 100, 100);
    }

    @Override
    public String getName() {
        return "Inside-Out";
    }

    @Override
    public String getColor() {
        return "red";
    }

}