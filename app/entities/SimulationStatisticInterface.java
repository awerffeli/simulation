package entities;

import models.Link;

/**
 * Created by werf on 28.10.14.
 */
public interface SimulationStatisticInterface {

    public void addDriver(Driver driver);

    public void save();

    public void saveSimulationStep(Driver driver, Link Link);
}
