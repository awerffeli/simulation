package entities;

import models.Destination;
import models.Node;
import models.Parkspace;

/**
 * Created by werf on 01.10.14.
 */
public interface MapInterface {

    public int getId();

    public String getName();

    public Node getRandomNode();

    public Parkspace getRandomParkspace();

    public Destination getRandomDestination();

    public Node getRandomEdgeNode();

    public double getLatitudeNormalized(Position position);

    public double getLongitudeNormalized(Position position);

    public Route getFastestRoute(Node start, Node target);

}
