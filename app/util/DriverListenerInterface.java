package util;

import java.util.EventListener;

/**
 * Created by werf on 15.10.14.
 */
public interface DriverListenerInterface extends EventListener {

    public void stateChanged(RemoveDriverEvent event);

    public void stateChanged(UpdateDriverDestinationEvent event);

}
