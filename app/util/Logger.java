package util;

import entities.Car;
import entities.Driver;
import entities.strategies.placebook.PlacebookUser;

/**
 * Created by werf on 17.10.14.
 */
public class Logger {

    public static boolean pbEnabled = false;
    public static boolean enabled = false;

    public static void log(String msg) {
        if (enabled) {
            System.out.println(msg);
        }
    }

    public static void logCar(Car car, String msg) {
        if (enabled) {
            System.out.println(String.format("Car %s %s", car.getId(), msg));
        }
    }

    public static void logDriver(Driver driver, String msg) {
        if (enabled) {
        //String caller = new Exception().getStackTrace()[1].getClassName();
        //if(caller.equals("Placebook") || caller.equals("PlacebookDriver") || caller.equals("PlacebookChecker"))
            System.out.println(String.format("Driver %s %s", driver.getId(), msg));
        }
    }

    public static void logPlacebookUser(PlacebookUser user, String msg) {
        if (pbEnabled) {
            System.out.println(String.format("PB User %s %s", user.getId(), msg));
        }
    }

    public static void logPlacebook(String msg) {
        if (pbEnabled) {
            System.out.println(msg);
        }
    }

}