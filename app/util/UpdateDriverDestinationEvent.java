package util;

import java.util.EventObject;

/**
 * Created by werf on 15.10.14.
 */
public class UpdateDriverDestinationEvent extends EventObject {

    private final SimulationEventDispatcher dispatcher;

    public UpdateDriverDestinationEvent(SimulationEventDispatcher dispatcher) {
        super(dispatcher);
        this.dispatcher = dispatcher;
    }

    // type safe way to get source (as opposed to getSource of EventObject
    public SimulationEventDispatcher getDispatcher() {
        return dispatcher;
    }
}
