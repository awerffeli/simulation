package util;

/**
 * Created by werf on 15.10.14.
 */
public class SpeedColor {
    private int Speed;
    private String color;

    public SpeedColor(int speed, String color) {
        Speed = speed;
        this.color = color;
    }

    public int getSpeed() {
        return Speed;
    }

    public String getColor() {
        return color;
    }
}