package util;

import entities.Car;

/**
 * Created by werf on 15.10.14.
 */
public interface SimulationEventDispatcher {

    public void addCarListener(DriverListenerInterface listener);

    public void removeCar(Car car);

}
