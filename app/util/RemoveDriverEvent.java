package util;

import java.util.EventObject;

/**
 * Created by werf on 15.10.14.
 */
public class RemoveDriverEvent extends EventObject {

    private final SimulationEventDispatcher dispatcher;

    public RemoveDriverEvent(SimulationEventDispatcher dispatcher) {
        super(dispatcher);
        this.dispatcher = dispatcher;
    }

    // type safe way to get source (as opposed to getSource of EventObject
    public SimulationEventDispatcher getDispatcher() {
        return dispatcher;
    }
}
