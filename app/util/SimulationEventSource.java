package util;

import entities.Car;
import entities.Driver;
import entities.DriverPreference;
import entities.MapInterface;
import play.libs.EventSource;
import play.mvc.Results;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by werf on 03.10.14.
 */
public class SimulationEventSource extends EventSource {

    private List<EventSource.Event> events = new ArrayList<EventSource.Event>();

    private boolean connected;
    private MapInterface map;
    private boolean enabled = true;
    private static int currentId = 1;

    private volatile boolean hasEnded = false;


    public SimulationEventSource(MapInterface map) {
        super();
        this.map = map;
    }

    public SimulationEventSource(MapInterface map, boolean enabled) {
        super();
        this.map = map;
        this.enabled = enabled;
    }

    @Override
    public void onConnected() {
        for (EventSource.Event e : events) {
            this.send(e);
        }
        this.connected = true;
    }

    public void onReady(Results.Chunks.Out<String> out) {
        super.onReady(out);
    }

    private void sendEvent(EventSource.Event event) {
        //System.out.println("here 1");
        //System.out.println("this.hasEnded" + this.hasEnded);
        if(this.hasEnded) {
            return;
        }
        if (this.connected) {
            this.send(event);
        } else {
            events.add(event);
        }
    }

    private void sendCarEvent(EventSource.Event event) {
        if (this.enabled) {
            this.sendEvent(event);
        }
    }

    /**
     * send event to create cars in frontend
     */
    public void sendNewDriverEvent(Driver driver) {
        DriverPreference preference = driver.getPreference();
        String msg = String.format("{\"id\":\"%s\", \"latitude\":\"%s\", \"longitude\":\"%s\", \"status\":\"%s\", \"color\":\"%s\", " +
                        "\"strategy\":\"%s\", \"maxPP\":\"%s\", \"maxWalk\":\"%s\", \"destinationNodeId\":\"%s\"}",
                driver.getId(), map.getLatitudeNormalized(driver.getPosition()), map.getLongitudeNormalized(driver.getPosition()),
                driver.getStatus().asString(), preference.getStrategy().getColor(), preference.getStrategy().getName(),
                preference.getMaxWillingToPayForParking(), preference.getMaxWalkDistance(), preference.getTargetDestinationNode().getId());

        EventSource.Event event = new EventSource.Event(msg, ""+SimulationEventSource.currentId++, "addDriver");
        this.sendCarEvent(event);
    }

    public void sendUpdateCarEvent(Car car) {
        String msg = String.format("{\"id\":\"%s\", \"latitude\":\"%s\", \"longitude\":\"%s\", \"distance\":\"%s\"}",
                car.getId(), map.getLatitudeNormalized(car.getPosition()), map.getLongitudeNormalized(car.getPosition()), car.getDrivenDistanceRounded());

        EventSource.Event event = new EventSource.Event(msg, ""+SimulationEventSource.currentId++, "updateCar");
        this.sendCarEvent(event);
    }


    public void sendUpdateDriverEvent(Driver driver) {
        String msg = String.format("{\"id\":\"%s\", \"status\":\"%s\"}",
                driver.getId(), driver.getStatus().asString());

        EventSource.Event event = new EventSource.Event(msg, ""+SimulationEventSource.currentId++, "updateDriver");
        this.sendCarEvent(event);
    }

    /**
     * send event to removes cars in frontend
     */
    public void sendRemoveDriverEvent(Driver driver) {

        String msg = String.format("{\"id\":\"%s\"}",
                driver.getId(), map.getLatitudeNormalized(driver.getPosition()), map.getLongitudeNormalized(driver.getPosition()));
        EventSource.Event event = new EventSource.Event(msg, ""+SimulationEventSource.currentId++, "removeDriver");
        this.sendCarEvent(event);
    }

    public void sendSimulationEndEvent() {
        EventSource.Event event = new EventSource.Event("", ""+SimulationEventSource.currentId++, "endSimulation");
        this.sendEvent(event);
    }

    public void sendResultReadyEvent() {
        EventSource.Event event = new EventSource.Event("", ""+SimulationEventSource.currentId++, "resultReady");
        this.send(event);       //needs to be send with send function else it will not be send because hasEnded is true
    }

    public void end() {
        this.sendSimulationEndEvent();
        this.hasEnded = true;
    }

    public void resultReady() {
        this.sendResultReadyEvent();
    }
}
