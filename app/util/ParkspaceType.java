package util;

/**
 * Created by werf on 30.09.14.
 */
public enum ParkspaceType {
   PARKING("Parking Spot") , GARAGE("Garage");

    private String type;

    ParkspaceType(String type) {
        this.type = type;
    }

    public String asString() {
        return this.type;
    }
}