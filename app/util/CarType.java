package util;

/**
 * Created by werf on 30.09.14.
 */
public enum CarType {
    AUTOMATIC, MANUAL;
}