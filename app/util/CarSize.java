package util;

/**
 * Created by werf on 30.09.14.
 */
public enum CarSize {
    COMPACT("compact", "green"),
    NORMAL("normal", "orange"),
    BIG("big", "red");

    private String type;
    private String color;


    CarSize(String type, String color) {
        this.type = type;
        this.color = color;
    }

    public String getColor() {
        return this.color;
    }

    public String getType() {
        return this.type;
    }
}