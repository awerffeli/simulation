package util;

import java.util.ArrayList;

public class SizedArrayList<T> extends ArrayList<T> {
    private int maxSize;

    public SizedArrayList(int size) {
        super();
        this.maxSize = size;
    }

    @Override
    public boolean add(Object object) {
        //If the stack is too big, remove elements until it's the right size.
        while (this.size() >= maxSize) {
            this.remove(0);
        }
        return super.add((T) object);
    }
}