package util;

import entities.Car;
import entities.Driver;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Created by werf on 03.10.14.
 */
public class SimulationChangeListener implements PropertyChangeListener, DriverListenerInterface {

    SimulationEventSource eventSource;

    public SimulationChangeListener(SimulationEventSource eventSource) {
        this.eventSource = eventSource;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object src = evt.getSource();
        if(src instanceof Car) {
            Car car = (Car) evt.getSource();
            this.eventSource.sendUpdateCarEvent(car);
        } else if(src instanceof Driver) {
            Driver driver = (Driver) evt.getSource();
            this.eventSource.sendUpdateDriverEvent(driver);
        }
    }

    @Override
    public void stateChanged(RemoveDriverEvent event) {
        Driver driver = (Driver) event.getSource();
        this.eventSource.sendRemoveDriverEvent(driver);
    }

    @Override
    public void stateChanged(UpdateDriverDestinationEvent event) {
        Driver driver = (Driver) event.getSource();
        this.eventSource.sendUpdateDriverEvent(driver);
    }
}
