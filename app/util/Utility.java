package util;

import java.lang.reflect.Field;

/**
 * Created by werf on 27.10.14.
 */
public class Utility {
    public static int random(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }


    public static String formatSecondsToHour(int totalSeconds) {

        int hours = totalSeconds / 3600;
        int minutes = (totalSeconds % 3600) / 60;
        int seconds = totalSeconds % 60;


        return String.format("%02dh %02dm %02ds", hours, minutes, seconds);
    }

    public static String formatSecondsToMin(int totalSeconds) {
        int minutes = (totalSeconds % 3600) / 60;
        int seconds = totalSeconds % 60;

        return String.format("%02dm %02ds", minutes, seconds);
    }

    public static Object clone(Object original, Object clone) {

        for(Field field : original.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if(field.getName().equals("id") || field.getName().equals("version")|| field.getName().equals("_EBEAN_MARKER")) {
                continue;
            }
            try {
                Object value = field.get(original);
                field.set(clone, value);

            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return clone;
    }

}
