package util;

/**
 * Created by werf on 30.09.14.
 */
public enum DriverStatus {
    DRIVING("Driving"),
    PARKED("Parked"),
    MATCHED("Matched"),
    NEW("New"),
    HOME("Home"),
    SEARCH("Search");                 //just created not driving nor parked

    private String status;

    DriverStatus(String status) {
        this.status = status;
    }

    public String asString() {
        return this.status;
    }
}