(function(){
    "use strict";
    /** app level module which depends on services and controllers **/
    angular.module('simulation', ['ngResource', 'simulation.services', 'simulation.controllers', 'timer', 'ui.slider']);
})();
