
/** Controllers */
angular.module('simulation.controllers', ['simulation.services']).
    controller('SimulationCtrl', function ($scope, $http, $location, $interval, SimulationService, $timeout) {
        'use strict';

        var url = $location.$$absUrl;

         $scope.cars = [];

        $scope.msgs = [];

         $scope.cars.followCar = function(car, event) {
            $scope.followingCar = car;
            $scope.blinkCar(car.id, 3, 300);
        };

        $scope.blinkCar = function (carId, times, speed) {
            var elem = $("#car-"+carId);
            $scope.blink(elem, times,speed);
        };

        /**
       * Purpose: blink a page element
       * Preconditions: the element you want to apply the blink to, the number of times to blink the element (or -1 for infinite times), the speed of the blink
       **/
        $scope.blink = function (elem, times, speed) {
            if (times > 0 || times < 0) {

                  if ($(elem).hasClass("blink")) {
                        $(elem).removeClass("blink");
                  }
                  else {
                        $(elem).addClass("blink");
                  }
              }

              clearTimeout(function () {
                    $scope.blink(elem, times, speed);
              });

              if (times > 0 || times < 0) {
                  setTimeout(function () {
                        $scope.blink(elem, times, speed);
                  }, speed);
                  times -= 0.5;
              }
        };

        $scope.submitCar = function () {
            $http.post("/car", { text: $scope.inputText, user: $scope.user,
            time: (new Date()).toUTCString(), room: $scope.currentCar.value });
            $scope.inputText = "";
        };

        $scope.addDriver = function (car) {
            $scope.$apply(function () {
                var newCar = JSON.parse(car.data);

                var index = $scope.cars.containsCar(newCar);
                if(index === false) {

                    newCar.latitude= newCar.latitude-11;
                    newCar.longitude= newCar.longitude-8;

                    console.log("new car: "+newCar.id+" on position lat "+newCar.latitude+" long "+newCar.longitude);
                    $scope.cars.push(newCar);
                }
            });
        };

        $scope.updateDriver = function (car) {
            $scope.$apply(function () {
                var updateCar = JSON.parse(car.data);
                var index = $scope.cars.containsCar(updateCar);
                if(index === false) {
                    $scope.cars.push(updateCar);
                }
                $scope.cars[index].status = updateCar.status;
                //$scope.cars[index].destinationNodeId = updateCar.destinationNodeId;
            });
        };

        $scope.updateCar = function (car) {
            $scope.$apply(function () {
                var updateCar = JSON.parse(car.data);

                updateCar.latitude= updateCar.latitude-11;
                updateCar.longitude= updateCar.longitude-8;

                var index = $scope.cars.containsCar(updateCar);
                if(index === false) {
                    console.log("new car: "+updateCar.id+" on position lat "+updateCar.latitude+" long "+updateCar.longitude);
                    $scope.cars.push(updateCar);
                }
                $scope.cars[index].latitude = updateCar.latitude;
                $scope.cars[index].longitude = updateCar.longitude;
                $scope.cars[index].distance = updateCar.distance;
            });
        };

        $scope.removeDriver = function (car) {
            $scope.$apply(function () {
                var removeCar = JSON.parse(car.data);

                console.log("removed Car: "+removeCar.id);
                var index = $scope.cars.containsCar(removeCar);
                if(index !== false) {
                    if($scope.cars[index] !== undefined){
                        $scope.cars[index].latitude = -10000;
                        $scope.cars[index].longitude = -10000;
                        $scope.cars.splice(index, 1);
                    }
                }
            });
        };

        $scope.forceSimulationEnd = function () {
            $('.force-simulation-end').addClass("disabled");
            $scope.endSimulation();
            $http.get("/simulation/"+$scope.currentSimulationId+"/end/");
        };

        $scope.endSimulation = function () {
            console.log("sim ended");
            $('.hint-prepare-result').removeClass("hide");
        };

        $scope.redirectResult = function () {
                console.log("redirecting to result");
                window.location = "/simulation/resultAction/"+$scope.currentSimulationId;
        };

        $scope.listen = function () {
            var sourceUrl = "/simulation/carFeed/" + $scope.currentSimulationId;
            $scope.carFeed = new EventSource(sourceUrl);
            console.log("trying to listen to: "+sourceUrl);

            $scope.carFeed.addEventListener("addDriver", $scope.addDriver, false);
            $scope.carFeed.addEventListener("updateDriver", $scope.updateDriver, false);
            $scope.carFeed.addEventListener("updateCar", $scope.updateCar, false);
            $scope.carFeed.addEventListener("removeDriver", $scope.removeDriver, false);
            $scope.carFeed.addEventListener("endSimulation", $scope.endSimulation, false);
            $scope.carFeed.addEventListener("resultReady", $scope.redirectResult, false);

            $interval(function() {
                SimulationService.getSimulation($scope.currentSimulationId).get(function(simulation){
                    $scope.simulatedTime= simulation.runSimulationTime;
                    $scope.simulatedTimeFormatted = simulation.runSimulationTimeFormatted;
                });
            }, 1000);

        };

        Array.prototype.containsCar = function(obj) {
            var i = this.length;
            while (i--) {
                if (this[i].id == obj.id) {
                    return i;
                }
            }
            return false;
        };

        $scope.listen();

}).controller('SimulationCreateCtrl', function ($scope) {
     $scope.convertSpeed = function (speed) {
            var simulationSpeed = speed;
            var simulationFactor = simulationSpeed / 50;
            return (Math.round(simulationFactor * 4) / 4).toFixed(2);
     };

     $scope.initSpeed = function () {
            if($scope.simulationSpeed > 500) {
                $scope.simulationSpeed = 500;
            }

            $scope.speed = $scope.simulationSpeed;
            $scope.simulationFactor = $scope.convertSpeed($scope.simulationSpeed);
    };

      $scope.visualizationChanged = function(showVisualization) {
        if(showVisualization) {
           // $('.slider').show();
            $scope.slider = {
                 'options': {
                    disabled : false,
                    slide: function (event, ui) {
                        var speed = ui.value;
                        $scope.$apply(function () {
                            $scope.simulationFactor = $scope.convertSpeed(speed);
                            $scope.simulationSpeed = Math.round($scope.simulationFactor * 50);
                        });
                      }
                 }
             };
             $('.hint-max-speed').addClass('hide');
             $('.hint-max-visualize-speed').removeClass('hide');

            $scope.initSpeed();      //set to default speed
        }
        else {
            $scope.slider = {
                 'options': {
                    disabled : true
                 }
             };
             $('.hint-max-speed').removeClass('hide');
             $('.hint-max-visualize-speed').addClass('hide');

            $scope.simulationSpeed = 1300;      //set to max speed
            $scope.speed = $scope.simulationSpeed;
            $scope.simulationFactor = 26;

        }
      };
    $scope.visualizationChanged($scope.showVisualization);

}).controller('TrafficCtrl', function ($scope) {

});