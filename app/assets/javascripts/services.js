
/** chatModel service, provides chat cars (could as well be loaded from server) */
/*
angular.module('simulation.services', []).service('carModel', function () {
    'use strict';
    var getCars = function () {

        return [];

    };
    return { getCars: getCars };
});
*/
angular.module('simulation.services', []).factory("SimulationService", function($resource){
 'use strict';
    return {
        getSimulation: function(simId){
            var url = '/simulation/retrieveSimulation/:simId/';
            return $resource(url, {simId:simId});

        }
    };
});