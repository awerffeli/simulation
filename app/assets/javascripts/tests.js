/*
describe("Unit testing jquery directive", function() {
    var $compile, $scope, element;
    $scope = element = $compile = void 0;

    // Load the module, which contains the directive
    beforeEach(module("jqueryDirectives"));

    // Store references to $rootScope and $compile so they are available to all tests in this describe block
    beforeEach(inject(function(_$compile_, _$rootScope_) {

        // The injector unwraps the underscores (_) from around the parameter names when matching
        $scope = _$rootScope_;
        return $compile == _$compile_;

    }));

    it("should slide Down a block", function() {

        // Create html fragment
        element = angular.element('<div class="form" data-my-slide="showForm">Text</div>');

        // Set variable
        $scope.showForm = true;

        // Compile a piece of HTML containing the directive
        $compile(element)($scope);
        $scope.$digest();

        // Set expectation
        return expect(element.css('height')).toBe('1px');
    });

    it("should slide Up a block", function() {

        // Create html fragment
        element = angular.element('<div class="form" data-my-slide="showForm">Text</div>');

        // Set variable
        $scope.showForm = false;

        // Compile a piece of HTML containing the directive
        $compile(element)($scope);
        $scope.$digest();

        // Set expectation
        return expect(element.css('height')).toBe('0px');
    });

});
    */