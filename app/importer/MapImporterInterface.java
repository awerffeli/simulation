package importer;

import entities.MapInterface;

/**
 * Created by werf on 01.10.14.
 */
public interface MapImporterInterface {

    public MapInterface importMap(String name);

}
