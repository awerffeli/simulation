package importer;

import com.avaje.ebean.Ebean;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.MultiLineString;
import entities.MapInterface;
import entities.Position;
import models.*;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.opengis.feature.simple.SimpleFeature;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by werf on 30.09.14.
 *
 * How to import map:
 * Goto: http://www.openstreetmap.org/export
 * Download desired map area
 * Open downloaded map in GIS-Tool (e.g. OpenJUMP)
 * In OpenJUMP in the main-menu goto "Bearbeiten" -> "Extrahieren" -> "Extrahieren von Ebenen nach Geometrie-Typ"
 * the .osm-file will be split in multiple files from which we will right click on example_Linie and click "Datensatz speichern als"
 * In the Dialog save the file as *.shp-file
 * Then import the file into the simulation
 */
public class DefaultMapImporter implements MapImporterInterface {

    private DefaultMap map;
    private File linksFile;
    private Map config;
    private int parkspace_quantity;

    public DefaultMapImporter(File linksFile, Map config) {
        this.linksFile = linksFile;
        this.config = config;
    }

    private Link getLink(Node startNode, Node destinationNode) {
        Link link = null;
        List<Link> links = Ebean.find(Link.class).where().eq("start_node_id", startNode.getId()).eq("destination_node_id", destinationNode.getId()).findList();
        if (!links.isEmpty()) {
            link = links.get(0);
        }
        return link;
    }

    private Node getNode(double longitude, double latitude) {
        Node node = null;
        List<Node> nodes = Ebean.find(Node.class).where().eq("map_id", this.map.getId()).eq("latitude", latitude).eq("longitude", longitude).findList();
        if (!nodes.isEmpty()) {
            node = nodes.get(0);
            return node;
        }
        List<Destination> destinations = Ebean.find(Destination.class).where().eq("map_id", this.map.getId()).eq("latitude", latitude).eq("longitude", longitude).findList();
        if (!destinations.isEmpty()) {
            node = destinations.get(0);
        }
        List<Parkspace> parkspaces = Ebean.find(Parkspace.class).where().eq("map_id", this.map.getId()).eq("latitude", latitude).eq("longitude", longitude).findList();
        if (!parkspaces.isEmpty()) {
            node = parkspaces.get(0);
        }
        List<Edge> edges = Ebean.find(Edge.class).where().eq("map_id", this.map.getId()).eq("latitude", latitude).eq("longitude", longitude).findList();
        if (!edges.isEmpty()) {
            node = edges.get(0);
        }
        return node;
    }

    private void load() throws IOException {
        Map url_map = new HashMap();
        url_map.put("url", linksFile.toURL());
        DataStore dataStore = DataStoreFinder.getDataStore(url_map);
        SimpleFeatureSource featureSource = dataStore.getFeatureSource(dataStore.getTypeNames()[0]);
        SimpleFeatureCollection collection = featureSource.getFeatures();
        SimpleFeatureIterator iterator = collection.features();
        SimpleFeatureIterator iterator2 = collection.features();


        double minLng = featureSource.getBounds().getLowerCorner().getCoordinate()[0];
        double minLat = featureSource.getBounds().getLowerCorner().getCoordinate()[1];


        double maxLng = featureSource.getBounds().getUpperCorner().getCoordinate()[0];
        double maxLat = featureSource.getBounds().getUpperCorner().getCoordinate()[1];

        map.minLatitude = (int)(minLat * 100000);
        map.maxLatitude = (int)(maxLat * 100000);

        map.minLongitude = (int)(minLng * 100000);
        map.maxLongitude = (int)(maxLng * 100000);

        map.save();

/*
        int overallNodesCount = 0;

        while (iterator2.hasNext()) {
            SimpleFeature feature = iterator2.next();
            MultiLineString geometry = (MultiLineString) feature.getDefaultGeometry();
            Coordinate[] coordinates = geometry.getCoordinates();
            overallNodesCount+= coordinates.length;
        }

        System.out.println("overallNodesCount"+overallNodesCount);

        int arraySize = overallNodesCount-1;

        Boolean[] isParkspaceArray = new Boolean[overallNodesCount];

        for ( int i = 0; i <= arraySize; i++ ) {
            isParkspaceArray[i] = false;
        }

        int parkspacesCreated = 0;

        while(parkspacesCreated < parkspace_quantity) {
            int index = Utility.random(0, arraySize);
            if(isParkspaceArray[index] == false) {
                isParkspaceArray[index] = true;
                parkspacesCreated++;
            }
        }
        System.out.println("parkspacesCreated "+parkspacesCreated);
        */

        int totalNodesCount = 0;

        while (iterator.hasNext()) {

            SimpleFeature feature = iterator.next();

            MultiLineString geometry = (MultiLineString) feature.getDefaultGeometry();

            Coordinate[] coordinates = geometry.getCoordinates();

            if (coordinates.length > 0) {
                Node node;
                Position position;
                Node lastNode = null;
                Link link;
                Link linkOpposite;
                Parkspace parkspace;
                Destination destination;
                Edge edge;


                //check if existing Node
                /*
                lastNode = this.getNode(coordinates[0].x, coordinates[0].y);
                if (lastNode == null) {
                    position = new Position(coordinates[0].x, coordinates[0].y);

                    if(isParkspaceArray[totalNodesCount] == true) {
                        lastNode = new Parkspace(position, this.map, 60, 2);
                        lastNode.save();
                    }else {
                        lastNode = new Node(position, this.map);
                        lastNode.save();
                    }
                }
                */

                int speedMin = 30, speedMax = 80;

                int ppDividor;
                //added ppDividor, so we can test map with different parkspace quantity

                /*
                if(parkspace_quantity >=80) {
                    ppDividor = 4;
                }
                else if(parkspace_quantity >=60) {
                    ppDividor = 5;
                }
                else if(parkspace_quantity >=40) {
                    ppDividor = 7;
                } else if(parkspace_quantity >=20) {
                    ppDividor = 17;
                } else {
                    ppDividor = 31;
                }
                */

                for (Coordinate cord : coordinates) {

                    //first node has already been saved before loop
                    //if (coordinateCount != 0) {
                        //x = latitude
                        //y = longitude
                        position = new Position(cord.x, cord.y);

                        //check if existing Node
                        node = this.getNode(cord.x, cord.y);
                        /*
                        if(node != null) {

                            if(isParkspaceArray[totalNodesCount] == true) {

                                if(node.getClass().equals("Parkspace")) {
                                    System.out.println(node.getClass());
                                }
                                String s = "UPDATE node set dtype = :dtype, max_allowed_park_time = :max_allowed_park_time, price = :price, total_parkspaces = :total_parkspaces where id = :id";
                                SqlUpdate update = Ebean.createSqlUpdate(s);
                                update.setParameter("id", node.getId());
                                update.setParameter("dtype", 4);
                                update.setParameter("max_allowed_park_time", 60);
                                update.setParameter("price", 200);
                                update.setParameter("total_parkspaces", 1);

                                int modifiedCount = Ebean.execute(update);
                                System.out.println("created pp 1");
                                //String msg = "There where " + modifiedCount + "rows updated";
                                //System.out.println(msg);

                            }
                        }
                        */
                        //else create new node
                        if(node == null) {
                            //parkspaces
                            /*
                            if (isParkspaceArray[totalNodesCount] == true) {
                                System.out.println("created pp 2");
                                parkspace = new Parkspace(position, this.map, 60, 2);
                                parkspace.save();
                                node = parkspace;
                            }
                            */
                            /*
                            if (totalNodesCount % ppDividor == 0) {
                                parkspace = new Parkspace(position, this.map, 60, 2);
                                parkspace.save();
                                node = parkspace;
                            }
                            */
                            //every 13th is Destination
                            if (totalNodesCount % 13 == 0) {
                                destination = new Destination(position, this.map);
                                destination.save();
                                node = destination;
                            }

                            //every 29th is edge
                            else if (totalNodesCount % 29 == 0) {
                                edge = new Edge(position, this.map);
                                edge.save();
                                node = edge;

                            } //every 71th is park-garage
                            /*
                            else if (totalNodesCount % 71 == 0) {
                                parkspace = new Parkspace(position, this.map, 3);
                                parkspace.save();
                                node = parkspace;
                            }*/
                            else {
                                node = new Node(position, this.map);
                                node.save();
                            }
                        }
                        if(lastNode != null) {

                            int randomSpeed = speedMin + (int) (Math.random() * ((speedMax - speedMin) + 1));
                            int direction = 0;

                            double length = DefaultMap.getDistance(lastNode, node);

                            //only create link if not save already exists
                            Link existingLink = this.getLink(lastNode, node);
                            if (existingLink == null) {
                                link = new Link(randomSpeed, length, lastNode, node, direction);
                                link.setMap(this.map);
                                link.save();
                            }

                            existingLink = this.getLink(node, lastNode);
                            if (existingLink == null) {
                                linkOpposite = new Link(randomSpeed, length, node, lastNode, direction);
                                linkOpposite.setMap(this.map);
                                linkOpposite.save();
                            }
                        }
                        lastNode = node;
                    //}
                    totalNodesCount++;
                }
            }
        }
        iterator.close();
    }

    /*
    private void createParkspaces2() {
        int i = 1;

        while (i <= this.parkspace_quantity) {
            Node node = Ebean.find(Node.class)
                    .where()
                    .eq("map_id", this.map.getId())
                    .eq("dtype", 1)
                    .orderBy("RAND()")
                    .setMaxRows(1)
                    .findUnique();

            System.out.println("updating node: "+node.getId()+" to parkspace");
            String s = "UPDATE node set dtype = :dtype, max_allowed_park_time = :max_allowed_park_time, price = :price, total_parkspaces = :total_parkspaces where id = :id";
            SqlUpdate update = Ebean.createSqlUpdate(s);
            update.setParameter("id", node.getId());
            update.setParameter("dtype", 4);
            update.setParameter("max_allowed_park_time", 60);
            update.setParameter("price", 200);
            update.setParameter("total_parkspaces", 1);
            int modifiedCount = Ebean.execute(update);
            i++;
        }
    }
    */

    private void createParkspaces() {
        int i = 1;

        while (i <= this.parkspace_quantity) {
            Node node = Ebean.find(Node.class)
                    .where()
                    .eq("map_id", this.map.getId())
                    .eq("dtype", 1)
                    .orderBy("RAND()")
                    .setMaxRows(1)
                    .findUnique();


            Parkspace parkspace = new Parkspace(node.getPosition(), this.map);
            parkspace.save();

            List<Link> links1 = Ebean.find(Link.class)
                    .where()
                    .eq("start_node_id", node.getId())
                    .findList();

            for (Link link : links1) {
                link.setStartNode(parkspace);
                link.save();
            }

            List<Link> links2 = Ebean.find(Link.class)
                    .where()
                    .eq("destination_node_id", node.getId())
                    .findList();

            for (Link link : links2) {
                link.setDestinationNode(parkspace);
                link.save();
            }
            node.delete();
            //map.addParkspace(parkspace);
            i++;
        }
    }

    @Override
    public MapInterface importMap(String name) {

        map = new DefaultMap(name);
        map.save();
        try {
            this.load();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return map;
    }

    public MapInterface importMap(String name, int parkspace_quantity) {

        this.parkspace_quantity = parkspace_quantity;
        map = new DefaultMap(name);
        map.save();
        try {
            this.load();

        } catch (IOException e) {
            e.printStackTrace();
        }
        //this.createParkspaces2();
        this.createParkspaces();
        map.save();
        return map;
    }

}
