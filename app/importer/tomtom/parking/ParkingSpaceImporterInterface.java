package importer.tomtom.parking;

import java.io.IOException;

/**
 * Created by thoenrom on 24.11.2014.
 */
public interface ParkingSpaceImporterInterface {

    public int getParkingSpacesAtCoord(double longitude, double latitude) throws IOException;

}
