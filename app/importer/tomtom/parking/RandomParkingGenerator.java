package importer.tomtom.parking;

import importer.tomtom.parking.ParkingSpaceImporterInterface;

/**
 * Created by simbuman on 25.11.14.
 */
public class RandomParkingGenerator implements ParkingSpaceImporterInterface {
    private final double PARKING_PERCENTAGE = 0.2;
    private final int MIN_PARKING_SPACES = 1;
    private final int MAX_PARKING_SPACES = 5;

    @Override
    public int getParkingSpacesAtCoord(double longitude, double latitude) {
        int returnSpaces = 0;
        if(Math.random() < PARKING_PERCENTAGE) {
            returnSpaces = (int)(Math.random() * (MAX_PARKING_SPACES - MIN_PARKING_SPACES)) + MIN_PARKING_SPACES;
        }
        return returnSpaces;
    }
}
