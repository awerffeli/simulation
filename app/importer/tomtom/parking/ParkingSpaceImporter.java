package importer.tomtom.parking;


import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

import com.vividsolutions.jts.geom.Coordinate;
import importer.tomtom.util.LatLngHelper;
import org.json.JSONArray;
import org.json.JSONObject;
/**
 * Created by simbuman & thoenrom on 24.11.2014.

 * This importer is written to import parking spaces from JSON files.
 * Files needed: *.json the parking spaces in a given area.
 */

public class ParkingSpaceImporter implements ParkingSpaceImporterInterface{

    private File parkingSpaceJson;
    private ArrayList<ParkingSpace> parkingData;
    private HashMap<Integer, Coordinate> nodeMap;

    private int numNodes = 0;
    private int numWays = 0;
    private int numSpots = 0;

    public ParkingSpaceImporter(File jsonFile) {
        parkingSpaceJson = jsonFile;
        nodeMap = new HashMap<>();
    }

    @Override
    public int getParkingSpacesAtCoord(double latitude, double longitude) throws IOException {
        int parkingspaces = 0;

        if(parkingData == null) {
            loadParkingData();
        }

        LatLngHelper llh = LatLngHelper.getInstance();

        double minLat = llh.addMetersToLatLng(latitude, longitude, -30)[0];
        double maxLat = llh.addMetersToLatLng(latitude, longitude, 30)[0];
        double minLng = llh.addMetersToLatLng(latitude, longitude, -30)[1];
        double maxLng = llh.addMetersToLatLng(latitude, longitude, 30)[1];

        for(ParkingSpace parking : parkingData) {
            for(Coordinate coord : parking.coords) {

                if(minLat <= coord.y && coord.y <= maxLat && minLng <= coord.x && coord.x <= maxLng) {
                    parkingspaces += parking.spaces;
                    parking.setSpaces(0);
                }
            }
        }

        return parkingspaces;
    }

    private void loadParkingData() throws IOException {
        parkingData = new ArrayList<>();

        log("Loading parking data from file: " + parkingSpaceJson.getName());

        String jsonString = readJson();

        log("Processing data");

        JSONObject obj = new JSONObject(jsonString);
        JSONArray elements = obj.getJSONArray("elements");

        for(int i = 0; i < elements.length(); i++) {
            JSONObject elem = elements.getJSONObject(i);
            if(elem.has("type") && elem.getString("type").equals("node")) {
                processNode(elem);
            } else if(elem.has("type") && elem.getString("type").equals("way")) {
                processWay(elem);
            }
        }

        log("Updating references");
        for(ParkingSpace space : parkingData) {
            space.updateNodes();
        }

        log("Loading finished. " + numNodes + " nodes and " + numWays + " ways with a total capacity of " + numSpots + " were added");
    }

    private String readJson() throws IOException {
        String returnString = "";
        BufferedReader in = new BufferedReader(new FileReader(parkingSpaceJson));

        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            //log(inputLine); //uncomment for debugging
            returnString += inputLine;
        }

        return returnString;
    }

    private void processNode(JSONObject node) {
        int spaces = 1;

        int nodeID = 0;

        if(node.has("id")) {
            nodeID = node.getInt("id");
        }

        double lat = node.getDouble("lat");
        double lng = node.getDouble("lon");

        if(node.has("tags")) {
            JSONObject tags = node.getJSONObject("tags");
            if(tags.has("capacity")) {
                spaces = tags.getInt("capacity");
            }

            parkingData.add(new ParkingSpace(lat, lng, spaces));

            numNodes++;
            numSpots += spaces;
        } else {
            if(nodeID != 0) {
                addNodeToMap(nodeID, lat, lng);
            }
        }
    }

    private void processWay(JSONObject way) {
        JSONArray wayNodes = way.getJSONArray("nodes");

        ParkingSpace parkingSpace = new ParkingSpace();

        for(int i = 0; i < wayNodes.length(); i++) {
            int nodeID = wayNodes.getInt(i);
            parkingSpace.addNodeID(nodeID);
        }

        JSONObject tags = way.getJSONObject("tags");
        int spaces = wayNodes.length();
        if(tags.has("capacity")) {
            spaces = tags.getInt("capacity");
        }

        parkingSpace.setSpaces(spaces);

        numWays++;
        numSpots += spaces;

        parkingData.add(parkingSpace);
    }

    private void addNodeToMap(int id, double lat, double lng) {
        Coordinate coord = new Coordinate(lng, lat);

        if(!nodeMap.containsKey(id)) {
            nodeMap.put(id, coord);
        }
    }

    private void log(String message) {
        System.out.println(this.getClass().getName() + " - " + message);
    }

    private class ParkingSpace {
        protected ArrayList<Coordinate> coords = new ArrayList<>();
        protected ArrayList<Integer> nodeIDs = new ArrayList<>();
        protected int spaces;

        public ParkingSpace() {}
        public ParkingSpace(double lat, double lng, int spaces) {
            coords.add(new Coordinate(lng, lat));
            this.spaces = spaces;
        }

        public void addNodeID(int id) {
            nodeIDs.add(id);
        }

        public void updateNodes() {
            for(int nodeID : nodeIDs) {
                if(nodeMap.containsKey(nodeID)) {
                    coords.add(nodeMap.get(nodeID));
                } else {
                    //Should never happen, except the data is somehow corrupt
                    log("No Coordinates found for NodeID: " + nodeID);
                }
            }

            nodeIDs.clear();
        }

        public void setSpaces(int spaces) {
            if(spaces > 0) {
                this.spaces = spaces;
            } else {
                this.spaces = 0;
            }
        }
    }
}
