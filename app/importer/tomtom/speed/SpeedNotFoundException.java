package importer.tomtom.speed;

/**
 * Created by simbuman & thoenrom on 27.11.2014.
 *
 * Exception if no real speed found in data.
 */
public class SpeedNotFoundException extends Throwable {
    public SpeedNotFoundException() {
        super();
    }
    public SpeedNotFoundException(String message) {
        super(message);
    }
}
