package importer.tomtom.speed;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by simbuman & thoenrom on 23.11.14.
 * 
 * This importer is written to give a possibility to pull Speed Data out of a .dbf Database using SQL
 * Files needed: *.dbf, the speed profiles per network link *.dbf and the speed profile data *.dbf
 */
public class TomTomSpeedImporter implements SpeedImporterInterface {

    private File speedProfiles;
    private File speedProfileData;

    private String speedFilePath;

    private static int numCalls;
    private int limit;
    private static int offset;
    private ArrayList<HashMap> speedProfilesList;
    private ArrayList<HashMap> speedProfilesDataList;

    public TomTomSpeedImporter(File speedFile, File profileFile) {

        this.speedProfiles = profileFile;
        this.speedProfileData = speedFile;

        try {
            speedFilePath = speedProfiles.getCanonicalPath().substring(0,speedProfiles.getCanonicalPath().lastIndexOf(File.separator));
        } catch (IOException e) {
            e.printStackTrace();
        }

        numCalls = 0;
        limit = 1000;
        offset = 0;

        speedProfilesDataList = new ArrayList<>();
        speedProfilesList = new ArrayList<>();

            initialize();

    }

    public int getSpeed(long network_ID) throws SpeedNotFoundException {

        int speed = 0;
        String profileName = "PROFILE_1";
        String speedProfile = "SPWEEK";

        for(int i=0; i<speedProfilesDataList.size(); i++){

            String list_network_ID = "" + speedProfilesDataList.get(i).get("NETWORK_ID");
            String dbf_network_ID = "" + network_ID;

            if(list_network_ID.equals(dbf_network_ID)){

                String profileNumber = "" + speedProfilesDataList.get(i).get(profileName);

                speed =  (int) ((getRelativeSpeed(profileNumber) * (int) speedProfilesDataList.get(i).get(speedProfile)) / 100);

            }

        }

        if(speed == 0){
            throw new SpeedNotFoundException();
        }

        return speed;

    }

    private void initialize() {

        try {

            getSpeedProfiles();

            offset = 1;

            for(int i =0; i<=50; i++){
                getSpeedData();
            }

        } catch (ClassNotFoundException e) {

            System.out.println("Failed to load data from .dbf file, check driver availability.");
            e.printStackTrace();

        }

    }
    private float getRelativeSpeed(String profile){

        float relativeSpeed = 100;

        for(int i = 0; i<speedProfilesList.size(); i++){

            if(speedProfilesList.get(i).get("PROFILE_ID") == profile){

                relativeSpeed =  Float.parseFloat(speedProfilesList.get(i).get("REL_SP").toString().replace(",", "."));

            }

        }

        return relativeSpeed;
    }
    private void resultSetToArrayList(ResultSet rs) throws SQLException {

        numCalls++;

        while (rs.next()) {

            HashMap row = new HashMap(rs.getMetaData().getColumnCount());
            for (int j = 1; j <= rs.getMetaData().getColumnCount(); j++) {

                row.put(rs.getMetaData().getColumnName(j), rs.getObject(j));

            }

            if(numCalls>1){
                speedProfilesDataList.add(row);
            }
            else{
                speedProfilesList.add(row);
            }
        }
    }

    private void getSpeedProfiles() throws ClassNotFoundException {

        ResultSet rs;

        try {

            // load the driver into memory
            Class.forName("jstels.jdbc.dbf.DBFDriver2");

            // create a connection. The first command line parameter is assumed to
            // be the directory in which the .dbf files are held
            Connection conn = DriverManager.getConnection("jdbc:jstels:dbf:" + speedFilePath);

            PreparedStatement stmt;
            stmt = conn.prepareStatement("SELECT * FROM \"" + speedProfiles.getName() + "\" LIMIT ? OFFSET ?");

            stmt.setInt(1, limit);
            stmt.setInt(2, offset);

            // execute a query
            rs = stmt.executeQuery();

            if(rs != null){

                resultSetToArrayList(rs);

            }

            // close the objects
            rs.close();
            stmt.close();
            conn.close();

        } catch (SQLException e) {
            //TODO replace with logger
            e.printStackTrace();
        } catch (ClassNotFoundException e) {

             throw new ClassNotFoundException();

        }

        offset = offset + 1000;

    }

    private void getSpeedData() throws ClassNotFoundException {

        ResultSet rs;

        try {


            // load the driver into memory
            Class.forName("jstels.jdbc.dbf.DBFDriver2");

            // create a connection. The first command line parameter is assumed to
            // be the directory in which the .dbf files are held
            Connection conn = DriverManager.getConnection("jdbc:jstels:dbf:" + speedFilePath);

            PreparedStatement stmt;

            //TODO: change minimum network_id for other scenario or if full version of driver is aquired.

            stmt = conn.prepareStatement("SELECT * FROM \"" + speedProfileData.getName() + "\" WHERE network_id > 17560000106151 LIMIT ? OFFSET ?");

            stmt.setInt(1, limit);
            stmt.setInt(2, offset);

            // execute a query
            rs = stmt.executeQuery();

            if(rs != null){

                resultSetToArrayList(rs);

            }

            // close the objects
            rs.close();
            stmt.close();
            conn.close();

        } catch (SQLException e) {
            //TODO replace with logger
            e.printStackTrace();

        } catch (ClassNotFoundException e) {
            throw new ClassNotFoundException();
        }

        offset = offset + 1000;

    }
}

