package importer.tomtom.speed;

/**
 * Created by simbuman on 25.11.14.
 */
public class RandomSpeedGenerator implements SpeedImporterInterface {

    @Override
    public int getSpeed(long network_ID) throws SpeedNotFoundException {
        int minSpeed = 30;
        int maxSpeed = 50;

        int deltaSpeed = maxSpeed - minSpeed;

        int returnSpeed = (int)(Math.random() * deltaSpeed) + minSpeed;

        return returnSpeed;
    }
}
