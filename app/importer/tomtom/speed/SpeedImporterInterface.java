package importer.tomtom.speed;

/**
 * Created by simbuman & thoenrom on 23.11.14.
 * 
 * Interface for importing Speeds
 */
public interface SpeedImporterInterface {

    public int getSpeed(long network_ID) throws SpeedNotFoundException;
}
