package importer.tomtom.speed;

import org.geotools.data.shapefile.dbf.DbaseFileReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;

/**
 * Created by simbuman on 02.12.14.
 *
 * This importer is written to give a possibility to pull Speed Data out of a .dbf Database using SQL
 * Files needed: *.dbf, the speed profiles per network link *.dbf and the speed profile data *.dbf
 */
public class TomTomSpeedImporter2 implements SpeedImporterInterface {
    private File speedProfiles;
    private File speedProfileData;

    private HashMap<Integer, Double> relSpeeds = new HashMap<>();
    private HashMap<Long, Integer> speeds = new HashMap<>();

    private int hourOfDay;

    public TomTomSpeedImporter2(File speedFile, File profileFile) {
        this.speedProfiles = profileFile;
        this.speedProfileData = speedFile;

        hourOfDay = 7;
    }
    public TomTomSpeedImporter2(File speedFile, File profileFile, int hourOfDay) {
        this.speedProfiles = profileFile;
        this.speedProfileData = speedFile;

        this.hourOfDay = hourOfDay;
    }

    @Override
    public int getSpeed(long network_ID) throws SpeedNotFoundException {
        if(speeds.isEmpty()) {
            load();
        }

        if(speeds.containsKey(network_ID)) {
            return speeds.get(network_ID);
        } else {
            throw new SpeedNotFoundException("No speed found for network ID " + network_ID);
        }
    }

    private void load() throws SpeedNotFoundException {
        try {
            loadProfiles();
            loadSpeeds();
        } catch (IOException e) {
            throw new SpeedNotFoundException("Problem during loading speed profiles");
        }
    }

    private void loadProfiles() throws IOException {
        //profile_id, time_slot, rel_sp
        log("Loading Profiles");

        FileInputStream fis = new FileInputStream(speedProfiles);
        DbaseFileReader dbf = new DbaseFileReader(fis.getChannel(), false, Charset.forName("ISO-8859-1"));

        Object[] fields = new Object[dbf.getHeader().getNumFields()];
        while(dbf.hasNext()) {
            dbf.readEntry(fields);

            int profile = (int)fields[0];
            int timeSlot = (int)fields[1];
            double relSpeed = (double)fields[2];

            //TODO implement average speed percentage

            if(!relSpeeds.containsKey(profile)) {
                if((int)(timeSlot / 3600.0) == hourOfDay) {
                    relSpeeds.put(profile, relSpeed);
                }
            }
        }
    }

    private void loadSpeeds() throws IOException {
        //oid_, network_id, val_dir, spfreeflow, spweekday, spweekend, spweek, profile_1, ..., profile_7
        //int, long, int, int, int, int, ..., int

        log("Loading Speeds");

        FileInputStream fis = new FileInputStream(speedProfileData);
        DbaseFileReader dbf = new DbaseFileReader(fis.getChannel(), false, Charset.forName("ISO-8859-1"));

        int readRowsCount = 0;
        Object[] fields = new Object[dbf.getHeader().getNumFields()];
        while(dbf.hasNext()) {
            dbf.readEntry(fields);

            long networkID = (long)fields[1];
            int speed = (int)fields[4]; //3 = free flow, 4 = weekday, 5 = weekend, 6 = average week
            int profile = (int)fields[7]; //7 = Monday, 8 = Tuesday, ... 13 = Sunday

            //TODO implement day selection

            if(!speeds.containsKey(networkID)) {
                if(relSpeeds.containsKey(profile)) {
                    speed = (int)(speed * (relSpeeds.get(profile) / 100));
                }
                speeds.put(networkID, speed);
            }

            readRowsCount++;
        }

        log(speeds.size() + " speeds loaded");
    }

    private void log(String message) {
        System.out.println(this.getClass().getName() + " - " + message);
    }
}
