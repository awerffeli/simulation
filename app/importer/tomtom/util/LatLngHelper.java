package importer.tomtom.util;

/**
 * Created by simbuman on 09.12.2014.

 * This importer is written to import parking spaces from JSON files.
 * Files needed: *.json the parking spaces in a given area.
 */
public class LatLngHelper {
    public static final double EARTH_CIRCUMFERENCE = 40075.017; //in km

    private static LatLngHelper instance = null;

    private LatLngHelper() {}

    public static LatLngHelper getInstance() {
        if(instance == null) {
            instance = new LatLngHelper();
        }

        return instance;
    }

    /**
     * Returns an array with modified latitude and longitude
     *
     * @param   lat     the latitude
     * @param   lng     the longitude
     * @param   meters  the offset in meters
     * @return          double array with modified latitude, longitude. index 0 = latitude, index 1 = longitude
     */
    public double[] addMetersToLatLng(double lat, double lng, int meters) {
        double[] returnLatLng = new double[2];

        returnLatLng[0] = lat + metersToLat(meters);
        returnLatLng[1] = lng - metersToLng(meters, lat);

        return returnLatLng;
    }

    private double metersToLat(int meters) {
        return (360 * meters / 1000) / EARTH_CIRCUMFERENCE;
    }

    private double metersToLng(int meters, double latitude) {
        return (360 * meters / 1000) / (EARTH_CIRCUMFERENCE * Math.cos(latitude));
    }
}
