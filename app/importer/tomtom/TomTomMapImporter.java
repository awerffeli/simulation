package importer.tomtom;

import com.avaje.ebean.Ebean;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.MultiLineString;
import entities.MapInterface;
import entities.Position;
import importer.MapImporterInterface;
import importer.tomtom.parking.ParkingSpaceImporter;
import importer.tomtom.parking.ParkingSpaceImporterInterface;
import importer.tomtom.speed.RandomSpeedGenerator;
import importer.tomtom.speed.SpeedImporterInterface;
import importer.tomtom.speed.SpeedNotFoundException;
import importer.tomtom.speed.TomTomSpeedImporter2;
import models.*;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.opengis.feature.simple.SimpleFeature;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by simbuman & thoenrom on 18.11.14.
 *
 * This importer is written to import maps and speed profiles from TomTom.
 * Files needed: *.shp, *.shx, *.prj, *.dbf, the speed profiles per network link *.dbf and the speed profile data *.dbf
 */
public class TomTomMapImporter implements MapImporterInterface {
    private static final String SHP_STREET_GEOMETRY = "the_geom";
    private static final String SHP_NETWORK_ID = "ID";
    private static final String SHP_STREET_NAME = "FULLNAME";

    private DefaultMap map;
    private File roadShp;
    private File speedProfiles;
    private File speedProfileData;
    private File parkingSpaceJson;
    private ArrayList<StreetData> streetData = new ArrayList<>();

    private int numNodes = 0;
    private int numEdges = 0;
    private int numParkings = 0;
    private int numParkingSpots = 0;
    private int numDestinations = 0;

    //Bounding Box
    private boolean checkBoundingBox;
    private double minLat;
    private double minLng;
    private double maxLat;
    private double maxLng;

    public TomTomMapImporter(File roadShp, File speedProfiles, File speedProfileData, File parkingSpaceJson) {

        this.roadShp = roadShp;
        this.speedProfiles = speedProfiles;
        this.speedProfileData = speedProfileData;
        this.parkingSpaceJson = parkingSpaceJson;

        checkBoundingBox = false;
    }
    public TomTomMapImporter(File roadShp, File speedProfiles, File speedProfileData, File parkingSpaceJson, double minLat, double minLng, double maxLat, double maxLng) {

        this.roadShp = roadShp;
        this.speedProfiles = speedProfiles;
        this.speedProfileData = speedProfileData;
        this.parkingSpaceJson = parkingSpaceJson;

        checkBoundingBox = true;
        this.minLat = minLat;
        this.minLng = minLng;
        this.maxLat = maxLat;
        this.maxLng = maxLng;
    }

    @Override
    public MapInterface importMap(String name) {
        map = new DefaultMap(name);
        map.save();

        try {
            this.load();
        } catch (IOException ex) {
            //TODO replace with logger
            ex.printStackTrace();
        }

        log("Map loading finished");

        return map;
    }

    private void load() throws IOException {
        checkFileAvailability();
        loadRoadData();
        loadSpeedData();
        loadParkingData();
        updateMapBoundingBox();
        createSimulationObjects();
    }

    private void checkFileAvailability() throws IOException {
        if(!(roadShp.exists() && roadShp.isFile())) {
            throw new IOException("Shape file containing the roads was not found! Please check the path given.");
        }

        if(!(speedProfiles.exists() && speedProfiles.isFile())) {
            throw new IOException("Database file containing the speed profiles per link was not found! Please check the path given.");
        }

        if(!(speedProfileData.exists() && speedProfileData.isFile())) {
            throw new IOException("Database file containing the speed profile data was not found! Please check the path given.");
        }

        if(!(parkingSpaceJson.exists() && parkingSpaceJson.isFile())) {
            throw new IOException("JSON file containing the parking space data was not found! Please check the path given.");
        }

        String path = roadShp.getCanonicalPath().substring(0, roadShp.getCanonicalPath().length() - 3);
        String[] endings = {"shx", "prj", "dbf"};

        for (String ending : endings) {
            File f = new File(path + ending);

            if(!(f.exists() && f.isFile())) {
                throw new IOException(ending + " file was not found! Please check the path given.");
            }
        }
    }

    private void loadRoadData() throws IOException {
        log("loading road data");

        FileDataStore store = FileDataStoreFinder.getDataStore(roadShp);
        SimpleFeatureSource featureSource = store.getFeatureSource();

        SimpleFeatureIterator it = featureSource.getFeatures().features();

        while(it.hasNext()) {
            SimpleFeature feature = it.next();

            MultiLineString streetGeometry = (MultiLineString)feature.getAttribute(SHP_STREET_GEOMETRY); //street geometry
            Long networkID = (Long)feature.getAttribute(SHP_NETWORK_ID); //network ID (used to get speed profiles)
            //String streetName = (String)feature.getAttribute(SHP_STREET_NAME); //road name (yet unused but might be useful in the future)

            if(checkIncludeStreet(streetGeometry)) {
                streetData.add(new StreetData(streetGeometry, networkID));
            }
        }
    }

    private boolean checkIncludeStreet(MultiLineString street) {
        boolean isInBox;

        if(!checkBoundingBox) {
            isInBox = true;
        } else {
            isInBox = false;
            Coordinate[] nodes = street.getCoordinates();

            for(Coordinate node : nodes) {
                if(minLat < node.y && node.y < maxLat && minLng < node.x && node.x < maxLng) {
                   isInBox = true;
                }
            }
        }

        return isInBox;
    }

    private void loadSpeedData() {
        log("loading speed data");
        SpeedImporterInterface speedImporter = new TomTomSpeedImporter2(speedProfiles, speedProfileData);
        SpeedImporterInterface randomSpeedImporter = new RandomSpeedGenerator();

        int speed;
        int realSpeeds = 0;
        int randomSpeeds = 0;

        for(StreetData data : streetData) {
            try {
                speed = speedImporter.getSpeed(data.networkID);
                realSpeeds++;
            } catch (SpeedNotFoundException e) {
                //log("No speed found for networkID " + data.networkID + ". Falling back to random speed");
                try {
                    speed = randomSpeedImporter.getSpeed(data.networkID);
                    randomSpeeds++;
                } catch (SpeedNotFoundException e1) {
                    log("No random speed!!! Well... just take 30");
                    speed = 30;
                }
            }

            data.setSpeed(speed);
        }

        log("Speed loading finished. " + realSpeeds + " real speeds and " + randomSpeeds + " random speeds imported");
    }

    private void loadParkingData() throws IOException {
        log("loading parking data");

        //ParkingSpaceImporterInterface parkingSpaces = new RandomParkingGenerator();
        ParkingSpaceImporterInterface parkingSpaces = new ParkingSpaceImporter(parkingSpaceJson);

        for(StreetData data : streetData) {
            Coordinate[] coords = data.streetGeometry.getCoordinates();

            int spaces = 0;
            for(Coordinate coord : coords) {
                spaces += parkingSpaces.getParkingSpacesAtCoord(coord.y, coord.x);
            }

            data.setParkingSpaces(spaces);
        }
    }

    private void updateMapBoundingBox() {
        log("updating bounding box");

        double minLat = streetData.get(0).streetGeometry.getCoordinates()[0].y;
        double minLng = streetData.get(0).streetGeometry.getCoordinates()[0].x;
        double maxLat = streetData.get(0).streetGeometry.getCoordinates()[0].y;
        double maxLng = streetData.get(0).streetGeometry.getCoordinates()[0].x;

        for(StreetData street : streetData) {
            Coordinate[] coords = street.streetGeometry.getCoordinates();
            for(Coordinate coord : coords) {
                if(minLat > coord.y) {
                    minLat = coord.y;
                } else if(maxLat < coord.y) {
                    maxLat = coord.y;
                }

                if(minLng > coord.x) {
                    minLng = coord.x;
                } else if(maxLng < coord.x) {
                    maxLng = coord.x;
                }
            }
        }

        map.minLatitude = (int)(minLat * 100000);
        map.maxLatitude = (int)(maxLat * 100000);

        map.minLongitude = (int)(minLng * 100000);
        map.maxLongitude = (int)(maxLng * 100000);

        map.save();
    }

    private void createSimulationObjects() {
        log("Creating simulation objects");
        Node lastNode;
        for(StreetData data : streetData) {
            Coordinate[] coords = data.streetGeometry.getCoordinates();
            lastNode = null;

            for(Coordinate coord : coords) {
                Node node = createNode(coord.y, coord.x, data);

                if(lastNode != null && node != lastNode) {
                    createLinks(node, lastNode, data.speed);
                }
                lastNode = node;

                if((numNodes + numParkings + numEdges + numDestinations) % 1000 == 0) {
                    log("Nodes: " + numNodes + ", Parking spots: " + numParkings + "(Capacity: " + numParkingSpots + "), Destinations: " + numDestinations + ", Edges: " + numEdges);
                }
            }
        }
    }

    private Node createNode(double lat, double lng, StreetData data) {
        Node node = getNode(lat, lng);

        if(node == null) {
            node = getEdgeNode(lat, lng, data);

            if(node == null) {
                if(data.parkingSpaces > 0) {
                    node = new Parkspace(new Position(lng, lat), map, data.parkingSpaces);
                    numParkings++;
                    numParkingSpots += data.parkingSpaces;
                    data.setParkingSpaces(0);
                } else {
                    if(data.streetUsed) {
                        node = new Node(new Position(lng, lat), map);
                        numNodes++;
                    } else {
                        node = getDestinationNode(lat, lng, data);
                        if(node == null) {
                            node = new Node(new Position(lng, lat), map);
                            numNodes++;
                        }
                    }
                }
            }

            node.save();
        }

        return node;
    }

    private Node getEdgeNode(double lat, double lng, StreetData data) {
        Node node = null;

        if(lat > maxLat || lng > maxLng || lat < minLat || lng < minLng) {
            Coordinate[] coords = data.streetGeometry.getCoordinates();

            if ((coords[0].y == lat && coords[0].x == lng) || (coords[coords.length - 1].y == lat && coords[coords.length - 1].x == lng)) {
                node = new Edge(new Position(lng, lat), map);
                numEdges++;
            }
        }

        return node;
    }

    private Node getDestinationNode(double lat, double lng, StreetData data) {
        Node node = null;

        double innerMinLat = minLat + 0.3 * (maxLat - minLat);
        double innerMinLng = minLng + 0.3 * (maxLng - minLng);
        double innerMaxLat = minLat + 0.7 * (maxLat - minLat);
        double innerMaxLng = minLng + 0.7 * (maxLng - minLng);
        double innerDestinationPercentage = 0.02;
        double outerDestinationPercentage = 0.01;

        if(innerMinLat < lat && lat < innerMaxLat && innerMinLng < lng && lng < innerMaxLng) {
            if(Math.random() <= innerDestinationPercentage) {
                node = new Destination(new Position(lng, lat), map);
                data.setStreetUsed();
                numDestinations++;
            }
        } else if(Math.random() <= outerDestinationPercentage) {
            node = new Destination(new Position(lng, lat), map);
            data.setStreetUsed();
            numDestinations++;
        }

        return node;
    }

    private void createLinks(Node node1, Node node2, int speed) {
        Link link;
        double length = DefaultMap.getDistance(node1, node2);

        if(getLink(node1, node2) == null) {
            link = new Link(speed, length, node1, node2, 0);
            link.setMap(map);
            link.save();
        }

        if(getLink(node2, node1) == null) {
            link = new Link(speed, length, node2, node1, 0);
            link.setMap(map);
            link.save();
        }
    }

    private Node getNode(double latitude, double longitude) {
        Node node = null;

        List<Node> nodes = Ebean.find(Node.class).where().eq("map_id", this.map.getId()).eq("latitude", latitude).eq("longitude", longitude).findList();
        if (!nodes.isEmpty()) {
            node = nodes.get(0);
            return node;
        }

        List<Destination> destinations = Ebean.find(Destination.class).where().eq("map_id", this.map.getId()).eq("latitude", latitude).eq("longitude", longitude).findList();
        if (!destinations.isEmpty()) {
            node = destinations.get(0);
        }

        List<Parkspace> parkspaces = Ebean.find(Parkspace.class).where().eq("map_id", this.map.getId()).eq("latitude", latitude).eq("longitude", longitude).findList();
        if (!parkspaces.isEmpty()) {
            node = parkspaces.get(0);
        }

        List<Edge> edges = Ebean.find(Edge.class).where().eq("map_id", this.map.getId()).eq("latitude", latitude).eq("longitude", longitude).findList();
        if (!edges.isEmpty()) {
            node = edges.get(0);
        }
        return node;
    }

    private Link getLink(Node startNode, Node destinationNode) {
        Link link = null;
        List<Link> links = Ebean.find(Link.class).where().eq("start_node_id", startNode.getId()).eq("destination_node_id", destinationNode.getId()).findList();
        if (!links.isEmpty()) {
            link = links.get(0);
        }
        return link;
    }

    private void log(String message) {
        System.out.println(this.getClass().getName() + " - " + message);
    }

    private class StreetData {
        protected MultiLineString streetGeometry;
        protected Long networkID;
        protected int speed;
        protected int parkingSpaces;
        protected boolean streetUsed = false;

        public StreetData(MultiLineString geometry, Long id) {
            streetGeometry = geometry;
            networkID = id;
        }

        public void setSpeed(int velocity) {
            speed = velocity;
        }

        public void setParkingSpaces(int amount) {
            parkingSpaces = amount;
        }

        public void setStreetUsed() {
            streetUsed = true;
        }
    }
}