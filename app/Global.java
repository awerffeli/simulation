/**
 * Created by werf on 24.10.14.
 */

import controllers.MapController;
import models.DefaultMap;
import models.Traffic;
import play.Application;
import play.GlobalSettings;

public class Global extends GlobalSettings {

    /**
     * Bootstrap application
     * @param app
     */
    @Override
    public void onStart(Application app) {

        if(DefaultMap.findAll().size() == 0) {
            MapController.importOSMZuerichMap("OSM ZH Szenario 40 Parkspaces", 40);
            //MapController.importOSMZuerichMap("OSM ZH Szenario 60 Parkspaces", 60);
            //MapController.importOSMZuerichMap("OSM ZH Szenario 80 Parkspaces", 80);
            MapController.importOSMWinterthurMap("OSM Winterthur Rechtwinklig 40 Parkspaces", 40);


            //MapController.importTomtomZuerichMap("Tomtom ZH Szenario 5 Parkspaces");
           // MapController.importTomtomWinterthurMap("Tomtom Winterthur 9 Parkspaces");

        }

        if(Traffic.findAll().size() == 0) {
           //new Traffic("Minor Traffic", 2, 2, 0, 50,30,20,34,33, 33, true).save();
           //new Traffic("Average Traffic", 5, 5, 0, 50,30,20,34,33, 33, true).save();
           //new Traffic("Heavy Traffic", 15, 15, 0, 50,30,20,34,33, 33, true).save();
           //new Traffic("Rush hour Traffic", 50, 50, 0, 50,30,20,50,50, 0, true).save();
           new Traffic("Luck Traffic", 10, 10, 0, 50,30,20,100,0, 0, 0, true).save();
           new Traffic("One Two Block Traffic", 10, 10, 0, 50,30,20,0, 0,100, 0, true).save();
            new Traffic("Inside Outside Traffic", 10, 10, 0, 50,30,20,0,0, 100, 0, true).save();
           new Traffic("Match Traffic", 10, 10, 0, 50,30,20,0,0, 0, 100, true).save();

          //  new Traffic("Baumann-Szenario 1,2,3", 40, 0, 0, 33, 33, 34, 50, 50, 0, true).save();
           // new Traffic("Baumann-Szenario 4,5,6", 40, 40, 0, 33, 33, 34, 25, 37, 37, true).save();
          //  new Traffic("Mix One Two Block / Match", 40, 20, 0, 33, 33, 34, 0, 50, 50, true).save();
        }
    }

    @Override
    public void onStop(Application app) {

    }

}