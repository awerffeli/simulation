package models;

import com.avaje.ebean.Ebean;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
public class Traffic extends Model{


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Constraints.Required
    private String name;
    private String comment;


    private boolean constantTraffic;
    private int newVehiclesRate;
    private int drivingVehiclesCount;
    private int parkedVehiclesCount;

    private float vehicleCompactPercent;
    private float vehicleNormalPercent;
    private float vehicleBigPercent;

    private float strategyLuckPercentage;                 //ÖAMTC Survey: 37%
    private float strategyOneTwoBlockPercentage;         //ÖAMTC Survey: 20%
    private float strategyInsideOutsidePercentage;
    private float strategyMatchPercentage;


    //driver maximal willing to pay for parking (in rappen)
    @Constraints.Required
    private int rangePayParkingMin;

    @Constraints.Required
    private int rangePayParkingMax;

    //driver maximal willing to walk for parking (in meter)
    @Constraints.Required
    private int rangeWalkMin;
    @Constraints.Required
    private int rangeWalkMax;

    //at simulation start driving driver parktime (in minutes)
    @Constraints.Required
    private int rangeTimeParkingMin;
    @Constraints.Required
    private int rangeTimeParkingMax;

    //at simulation start parked driver parktime (in minutes)
    @Constraints.Required
    private int rangeTimeParkedParkingMin;
    @Constraints.Required
    private int rangeTimeParkedParkingMax;

    //driver maximal parkspace search time
    @Constraints.Required
    private int maxParkspaceSearchTime;           //minutes


    private boolean isTemplate = true;

    @ManyToOne
    private Traffic originalTraffic;

    @Version
    private Integer version;        //is needed else we get a OptimisticLockException
    private boolean isRemoved = false;

    public Traffic() {

        //driver maximal willing to pay for parking (in rappen)
        this.rangePayParkingMin = 700;
        this.rangePayParkingMax = 700;

        //driver maximal willing to walk for parking (in meter)
        this.rangeWalkMin = 500;
        this.rangeWalkMax = 500;

        //at simulation start driving driver parktime (in minutes)
        this.rangeTimeParkingMin = 2;
        this.rangeTimeParkingMax = 5;

        //at simulation start parked driver parktime (in minutes)
        this.rangeTimeParkedParkingMin = 1;
        this.rangeTimeParkedParkingMax = 3;

        //driver maximal parkspace search time
        this.maxParkspaceSearchTime = 10;           //minutes


    }

    public Traffic(String name, int drivingVehiclesCount, int parkedVehiclesCount, int newVehiclesRate, float vehicleCompactPercent,
                   float vehicleNormalPercent, float vehicleBigPercent, float strategyLuckPercentage, float strategyOneTwoBlockPercentage,
                   float strategyInsideOutsidePercentage, float strategyMatchPercentage, boolean constantTraffic) {
        this.name = name;
        this.drivingVehiclesCount = drivingVehiclesCount;
        this.parkedVehiclesCount = parkedVehiclesCount;
        this.newVehiclesRate = newVehiclesRate;
        this.constantTraffic = constantTraffic;

        this.vehicleCompactPercent = vehicleCompactPercent;
        this.vehicleNormalPercent = vehicleNormalPercent;
        this.vehicleBigPercent = vehicleBigPercent;

        this.strategyLuckPercentage = strategyLuckPercentage;
        this.strategyOneTwoBlockPercentage = strategyOneTwoBlockPercentage;
        this.strategyInsideOutsidePercentage = strategyInsideOutsidePercentage;
        this.strategyMatchPercentage = strategyMatchPercentage;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNewVehiclesRate() {
        return this.newVehiclesRate;
    }

    public void setNewVehiclesRate(int newVehiclesRate) {
        this.newVehiclesRate = newVehiclesRate;
    }

    public int getParkedVehiclesCount() {
        return this.parkedVehiclesCount;
    }

    public void setParkedVehiclesCount(int parkedVehiclesCount) {
        this.parkedVehiclesCount = parkedVehiclesCount;
    }

    public int getDrivingVehiclesCount() {
        return this.drivingVehiclesCount;
    }

    public void setDrivingVehiclesCount(int drivingVehiclesCount) {
        this.drivingVehiclesCount = drivingVehiclesCount;
    }

    public boolean isHeavyTraffic() {
        int totalCount = this.getDrivingVehiclesCount() + this.getParkedVehiclesCount();
        if (totalCount > 50 || this.getNewVehiclesRate() > 20) {
            return true;
        }
        return false;
    }

    public float getVehicleCompactPercent() {
        return vehicleCompactPercent;
    }

    public void setVehicleCompactPercent(float vehicleCompactPercent) {
        this.vehicleCompactPercent = vehicleCompactPercent;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public float getVehicleNormalPercent() {
        return vehicleNormalPercent;
    }

    public void setVehicleNormalPercent(float vehicleNormalPercent) {
        this.vehicleNormalPercent = vehicleNormalPercent;
    }

    public String getVehicleBigPercentFormatted() {
        return String.format("%.1f %%", this.getVehicleBigPercent());
    }

    public String getVehicleNormalPercentFormatted() {
        return String.format("%.1f %%", this.getVehicleNormalPercent());
    }

    public String getVehicleCompactPercentFormatted() {
        return String.format("%.1f %%", this.getVehicleCompactPercent());
    }

    public float getVehicleBigPercent() {
        return vehicleBigPercent;
    }

    public void setVehicleBigPercent(float vehicleBigPercent) {
        this.vehicleBigPercent = vehicleBigPercent;
    }

    public float getStrategyLuckPercentage() {
        return strategyLuckPercentage;
    }

    public float getStrategyOneTwoBlockPercentage() {
        return strategyOneTwoBlockPercentage;
    }

    public String getStrategyMatchPercentageFormatted() {
        return String.format("%.1f %%", this.getStrategyMatchPercentage());
    }

    public String getStrategyInsideOutsidePercentageFormatted() {
        return String.format("%.1f %%", this.getStrategyInsideOutsidePercentage());
    }

    public String getStrategyOneTwoBlockPercentageFormatted() {
        return String.format("%.1f %%", this.getStrategyOneTwoBlockPercentage());
    }

    public String getStrategyLuckPercentageFormatted() {
        return String.format("%.1f %%", this.getStrategyLuckPercentage());
    }
    
    public float getStrategyMatchPercentage() {
        return strategyMatchPercentage;
    }

    public float getStrategyInsideOutsidePercentage() {
        return strategyInsideOutsidePercentage;
    }

    public void setStrategyMatchPercentage(float strategyMatchPercentage) {
        this.strategyMatchPercentage = strategyMatchPercentage;
    }

    public void setStrategyLuckPercentage(float strategyLuckPercentage) {
        this.strategyLuckPercentage = strategyLuckPercentage;
    }

    public void setStrategyOneTwoBlockPercentage(float strategyOneTwoBlockPercentage) {
        this.strategyOneTwoBlockPercentage = strategyOneTwoBlockPercentage;
    }

    public void setStrategyInsideOutsidePercentage(float strategyInsideOutsidePercentage) {
        this.strategyInsideOutsidePercentage = strategyInsideOutsidePercentage;
    }

    public Traffic getOriginalTraffic() {
        return originalTraffic;
    }

    public void setStrategies(int strategyBlock, int strategyLuck, int strategyInsideOutside, int strategyMatch) {
        int totalStrategy = strategyBlock + strategyLuck + strategyInsideOutside + strategyMatch;

        this.setStrategyLuckPercentage(100f * strategyLuck / totalStrategy);
        this.setStrategyOneTwoBlockPercentage(100f * strategyBlock / totalStrategy);
        this.setStrategyInsideOutsidePercentage(100f * strategyInsideOutside / totalStrategy);
        this.setStrategyMatchPercentage(100f * strategyMatch / totalStrategy);
    }

    public void setVehicles(int vehicleCompact, int vehicleNormal, int vehicleBig) {
        int totalVehicle = vehicleCompact + vehicleNormal + vehicleBig;

        this.setVehicleCompactPercent(100f * vehicleCompact / totalVehicle);
        this.setVehicleNormalPercent(100f * vehicleNormal / totalVehicle);
        this.setVehicleBigPercent(100f * vehicleBig / totalVehicle);
    }

    public static Map<String, String> findAll() {

        List<Traffic> traffics = new Model.Finder(String.class, Traffic.class).all();

        Map<String, String> retVal = new HashMap<>();
        for (Traffic traffic : traffics) {
            retVal.put("" + traffic.getId(), traffic.getName());
        }
        return retVal;
    }


    public static Map<String, String> findAllTemplates() {

        List<Traffic> traffics = new Model.Finder(String.class, Traffic.class).where().eq("isTemplate", true).eq("isRemoved", false).findList();

        Map<String, String> retVal = new HashMap<>();
        for (Traffic traffic : traffics) {
            retVal.put("" + traffic.getId(), traffic.getName());
        }
        return retVal;
    }


    /**
     * when we run simulation traffic is cloned, so if traffic is edited it will not affect running or finished simulations
     * @param original
     * @return
     */
    public static Traffic clone(Traffic original) {

        Traffic trafficClone = new Traffic();
        original.refresh();
        for (Field field : Traffic.class.getDeclaredFields()) {
            field.setAccessible(true);
            if (field.getName().equals("id") || field.getName().equals("version") || field.getName().equals("_EBEAN_MARKER")) {
                continue;
            }

            try {
                if (field.getName().equals("isTemplate")) {
                    field.set(trafficClone, new Boolean(false));
                }  else if (field.getName().equals("originalTraffic")) {
                    field.set(trafficClone, original);
                }
                else {
                    Object value = field.get(original);
                    field.set(trafficClone, value);
                }
            } catch (IllegalAccessException e) {}
        }
        return trafficClone;
    }

    public boolean isConstantTraffic() {
        return constantTraffic;
    }

    public void setConstantTraffic(boolean constantTraffic) {
        this.constantTraffic = constantTraffic;
    }


    public int getRangePayParkingMin() {
        return rangePayParkingMin;
    }

    public void setRangePayParkingMin(int rangePayParkingMin) {
        this.rangePayParkingMin = rangePayParkingMin;
    }

    public int getRangePayParkingMax() {
        return rangePayParkingMax;
    }

    public void setRangePayParkingMax(int rangePayParkingMax) {
        this.rangePayParkingMax = rangePayParkingMax;
    }

    public int getRangeWalkMin() {
        return rangeWalkMin;
    }

    public void setRangeWalkMin(int rangeWalkMin) {
        this.rangeWalkMin = rangeWalkMin;
    }

    public int getRangeWalkMax() {
        return rangeWalkMax;
    }

    public void setRangeWalkMax(int rangeWalkMax) {
        this.rangeWalkMax = rangeWalkMax;
    }

    public int getRangeTimeParkingMax() {
        return rangeTimeParkingMax;
    }

    public void setRangeTimeParkingMax(int rangeTimeParkingMax) {
        this.rangeTimeParkingMax = rangeTimeParkingMax;
    }

    public int getRangeTimeParkingMin() {
        return rangeTimeParkingMin;
    }

    public void setRangeTimeParkingMin(int rangeTimeParkingMin) {
        this.rangeTimeParkingMin = rangeTimeParkingMin;
    }

    public int getRangeTimeParkedParkingMax() {
        return rangeTimeParkedParkingMax;
    }

    public void setRangeTimeParkedParkingMax(int rangeTimeParkedParkingMax) {
        this.rangeTimeParkedParkingMax = rangeTimeParkedParkingMax;
    }

    public int getRangeTimeParkedParkingMin() {
        return rangeTimeParkedParkingMin;
    }

    public void setRangeTimeParkedParkingMin(int rangeTimeParkedParkingMin) {
        this.rangeTimeParkedParkingMin = rangeTimeParkedParkingMin;
    }

    public int getMaxParkspaceSearchTime() {
        return maxParkspaceSearchTime;
    }

    public void setMaxParkspaceSearchTime(int maxParkspaceSearchTime) {
        this.maxParkspaceSearchTime = maxParkspaceSearchTime;
    }

    public static Traffic getByName(String name) {
        return Ebean.find(Traffic.class)
                .where()
                .eq("name", name)
                .setMaxRows(1)
                .findUnique();
    }

    public void save() {
        super.save();
        if(this.getName() == null || this.getName().isEmpty()) {
            long id = this.getId();
            this.setName("Traffic "+id);
            super.save();
        }
    }



    public void remove() {
        this.isRemoved = true;
        this.save();
    }

}