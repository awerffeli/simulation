package models;

import models.results.MatchingResult;
import models.results.SimulationResult;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import util.Utility;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
public class Simulation extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "simulation", orphanRemoval = true, cascade = CascadeType.ALL)
    private SimulationResult simulationResult;



    /*@Constraints.Required()
     * removed contraint because in else on simulation run we get an error when getting the traffic by name
     */
    @ManyToOne
    private Traffic traffic;

    @Constraints.Required()
    @ManyToOne
    private DefaultMap map;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "simulation", orphanRemoval = true, cascade = CascadeType.ALL)
    private MatchingResult matchingResult;

    private Date runDate;

    //will be generated automatically if empty
    private String name;

    private String comment;
    private int runsCount = 1;
    private boolean hasFinished = false;

    public static final int DEFAULT_SPEED = 50;
    public static final int MAX_SPEED = 1300;
    private int simulationDuration = 5;                            //in minutes
    private int simulationSpeed = DEFAULT_SPEED;                    //realtime = 50, 100 = twice as fast, 25 twice as slow

    private long startTime;
    private long endTime;
    private boolean showVisualization = true;

    private boolean isRemoved = false;

    public Simulation() {
        this.runDate = new Date();
        this.startTime = System.currentTimeMillis();
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getRunDate() {
        return runDate;
    }

    public String getRunDateFormatted() {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(runDate);
    }

    public Traffic getTraffic() {
        return traffic;
    }

    public void setTraffic(Traffic traffic) {
        this.traffic = traffic;
    }

    public void setRunDate(Date runDate) {
        this.runDate = runDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRunsCount() {
        return runsCount;
    }

    public void setRunsCount(int runsCount) {
        this.runsCount = runsCount;
    }

    /**
     * returns the simulation duration in minutes
     * @return
     */
    public int getSimulationDuration() {
        return simulationDuration;
    }

    public int getSimulationDurationSecondsEffective() {
        if(this.endTime == 0 || this.startTime == 0) {
            return 0;
        }
        long durationSeconds = new Long((this.endTime - this.startTime) / 1000).intValue();
        return new Double(Simulation.convertFromSimulationTime(durationSeconds, this.getSimulationResult().getSimulation().getSimulationSpeed())).intValue();
    }

    public String getSimulationDurationFormatted() {
        int duration = this.getSimulationDuration() * 60;       //seconds
        return Utility.formatSecondsToHour(duration);
    }

    public String getEffectiveSimulationDurationFormatted() {
        int duration = this.getSimulationDurationSecondsEffective();       //seconds
        return Utility.formatSecondsToHour(duration);
    }

    /**
     * returns the simulation duration in seconds
     * @return
     */
    public int getSimulationDurationSeconds() {
        return simulationDuration * 60;
    }

    /**
     * pass param in minutes
     * @param simulationDuration
     */
    public void setSimulationDuration(int simulationDuration) {
        this.simulationDuration = simulationDuration;
    }

    public boolean hasFinished() {
        return hasFinished;
    }

    public void setFinished() {
        this.endTime = System.currentTimeMillis();
        this.hasFinished = true;
    }

    /**
     * considers simualationSpeed
     */
    public int getEffectiveSimulationDurationSeconds() {
        return (int) Simulation.convertToSimulationTime(this.getSimulationDurationSeconds(), this.simulationSpeed);
    }

    /**
     * calculates the seconds taken into account the simulationspeed
     *
     * @param time
     * @return
     */
    public static double convertToSimulationTime(int time, int simulationSpeed) {
        return ((double)time) * ((double)DEFAULT_SPEED / simulationSpeed);
    }

    /**
     *
     * @param time
     * @return
     */
    public static double convertFromSimulationTime(double time, int simulationSpeed) {
        return ((double)time) / ((double)DEFAULT_SPEED / simulationSpeed);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSimulationSpeedFormatted() {
        if (this.simulationSpeed > 65) {
            return "fast";
        } else if (this.simulationSpeed > 35) {
            return "normal";
        }
        return "slow";
    }

    public int getSimulationSpeed() {
        return this.simulationSpeed;
    }

    public float getSimulationSpeedFactor() {
        return (float)this.simulationSpeed / Simulation.DEFAULT_SPEED;
    }

    public String getSimulationSpeedFactorFormatted() {
        return String.format("%.2fx", this.getSimulationSpeedFactor());
    }

    public void setSimulationSpeed(int simulationSpeed) {
        this.simulationSpeed = simulationSpeed;
    }

    public boolean isShowVisualization() {
        return showVisualization;
    }

    public boolean isShowVisualizationEffective() {
        if (this.showVisualization && this.isVisualizationPossible()) {
            return true;
        }
        return false;
    }

    public void setShowVisualization(boolean showVisualization) {
        this.showVisualization = showVisualization;
    }

    public DefaultMap getMap() {
        return map;
    }

    public void setMap(DefaultMap map) {
        this.map = map;
    }

    public double getRemainingSimulationTime() {
        double simulationDuration = Simulation.convertToSimulationTime(this.getSimulationDurationSeconds(), this.getSimulationSpeed());
        double alreadyRunTime = this.getRunSimulationTime();
        return (simulationDuration - alreadyRunTime);
    }

    //in seconds
    public double getRunSimulationTime() {
        long endingTime = this.endTime;
        if(endingTime == 0) {
            endingTime = System.currentTimeMillis();
        }
        long estimatedTime = endingTime - this.startTime;
        double runningTime = (estimatedTime / 1000);
        return runningTime;
    }

    public String getEffectiveRunSimulationTimeFormatted() {
        Double time = this.getRunSimulationTime();
        Double simTime = Simulation.convertFromSimulationTime(time, this.simulationSpeed);

        return Utility.formatSecondsToHour(simTime.intValue());
    }

    private boolean isVisualizationPossible() {
        if (!this.traffic.isHeavyTraffic() && !this.isTooFastToVisualize()) {
            return true;
        }
        return false;
    }

    private boolean isTooFastToVisualize() {
        if (this.simulationSpeed > 600) {
            return true;
        }
        return false;
    }

    public SimulationResult getSimulationResult() {
        return this.simulationResult;
    }

    public MatchingResult getMatchingResult() {
        return matchingResult;
    }

    public void remove() {
        this.isRemoved = true;
        this.save();
    }
}