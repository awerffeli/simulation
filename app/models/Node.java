package models;

import entities.Position;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Created by werf on 01.10.14.
 */

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("1")
public class Node extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;

    @ManyToOne
    protected DefaultMap map;

    @Transient
    protected Position position;

    protected double longitude;

    protected double latitude;



    public Node(Position position, DefaultMap map) {
        this.latitude = position.getLatitude();
        this.longitude = position.getLongitude();
        this.position = position;
        this.map = map;
    }

    public Node() {

    }

    public int getId() {
        return id;
    }

    public Position getPosition() {
        //in db we just save latitude and longitude, recreate position object
        if (this.position == null) {
            this.position = new Position(this.longitude, this.latitude);
        }
        return this.position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public DefaultMap getMap() {
        return map;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        Node other = (Node) obj;
        if (this.id != other.getId()) {
            return false;
        }

        return true;
    }

    public boolean isParkspace() {
        return this instanceof Parkspace;
    }


    public String toString() {

        StringBuilder value = new StringBuilder();

        value.append("Node ");
        value.append("id " + id);
        value.append(" position " + this.getPosition());

        return value.toString();
    }
}
