package models;

import entities.strategies.StrategyInterface;
import play.db.ebean.Model;
import util.CarSize;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 * Created by werf on 20.11.14.
 */
@Entity
public class DeterministicDriver extends Model {

    @ManyToOne
    private Traffic traffic;

    @ManyToOne
    private Node startNode;

    @Transient
    private StrategyInterface strategy;

    @ManyToOne
    private Node targetDestinationNode;

    private boolean isLeavingSimulation;

    private CarSize carSize;

    public DeterministicDriver() {

    }

    public DeterministicDriver(Traffic traffic, Node startNode, Node targetDestinationNode, StrategyInterface strategy, boolean isLeavingSimulation, CarSize carSize) {
        this.traffic = traffic;
        this.startNode = startNode;
        this.targetDestinationNode = targetDestinationNode;
        this.strategy = strategy;
        this.isLeavingSimulation = isLeavingSimulation;
        this.carSize = carSize;
    }

    public Node getStartNode() {
        return startNode;
    }

    public void setStartNode(Node startNode) {
        this.startNode = startNode;
    }

    public StrategyInterface getStrategy() {
        return strategy;
    }

    public void setStrategy(StrategyInterface strategy) {
        this.strategy = strategy;
    }

    public Node getTargetDestinationNode() {
        return targetDestinationNode;
    }

    public void setTargetDestinationNode(Node targetDestinationNode) {
        this.targetDestinationNode = targetDestinationNode;
    }



    public boolean isLeavingSimulation() {
        return isLeavingSimulation;
    }

    public void setLeavingSimulation(boolean isLeavingSimulation) {
        this.isLeavingSimulation = isLeavingSimulation;
    }

    public CarSize getCarSize() {
        return carSize;
    }

    public void setCarSize(CarSize carSize) {
        this.carSize = carSize;
    }
}
