package models;

import entities.Position;
import util.ParkspaceType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;


/**
 * Created by werf on 08.10.14.
 */

@Entity
@DiscriminatorValue("4")
public class Parkspace extends Node {

    private int maxAllowedParkTime;                      //in Minutes
    private int price;                                   //Per hour, in Rappen
    private int totalParkspaces;

    @Transient
    private int occupiedParkspaces = 0;

    private ParkspaceType type;

    public Parkspace(Position position, DefaultMap map) {
        this(position, map, 1);
    }

    public Parkspace(Position position, DefaultMap map, int totalParkspaces) {
        super(position, map);

        this.totalParkspaces = totalParkspaces;

        //set random price between 1-5 CHF

        //this.maxAllowedParkTime = Utility.random(60, 90);
        this.maxAllowedParkTime = 90;

        this.totalParkspaces = totalParkspaces;

        if(totalParkspaces > 1) {
            this.type = ParkspaceType.GARAGE;
            //this.price = Utility.random(200, 400);
            this.price = 200;
        } else {
            this.type = ParkspaceType.PARKING;
            //this.price = Utility.random(100, 200);
            this.price = 100;
        }
    }



    public boolean isGarage() {
        return type == ParkspaceType.GARAGE;
    }

    public boolean isParking() {
        return type == ParkspaceType.PARKING;
    }

    public ParkspaceType getType() {
        return type;
    }



    public boolean isAvailable() {
        if(this.occupiedParkspaces < totalParkspaces) {
            return true;
        }
        return false;
    }

    public synchronized boolean parkCar(){
        if(!this.isAvailable()) {
            return false;
        }
        this.occupiedParkspaces++;
        return true;
    }

    public synchronized void removeCar() {
        this.occupiedParkspaces--;
    }

    public double getPricePerHour() {
        return (double) price / 100;
    }

    public void setPrice(double price) {
        this.price = (int) (price * 100);
    }

    /**
     * in minutes
     * @return
     */
    public int getMaxAllowedParkTime() {
        return maxAllowedParkTime;
    }

    public int getTotalParkspaces() {
        return totalParkspaces;
    }

    public String toString() {

        StringBuilder value = new StringBuilder();

        value.append("Parkspace ");
        value.append("id " + id);
        value.append(" position " + this.getPosition());

        return value.toString();
    }

}
