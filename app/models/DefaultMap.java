package models;

import entities.MapArea;
import entities.MapInterface;
import entities.Position;
import entities.Route;
import play.db.ebean.Model;
import util.DijkstraAlgorithm;
import util.SpeedColor;
import util.Utility;

import javax.persistence.*;
import java.util.*;

/**
 * Created by werf on 01.10.14.
 */

@Entity
public class DefaultMap extends Model implements MapInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "map")
    private List<Node> nodes;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "map")
    private List<Destination> destinations;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "map")
    private List<Edge> edges;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "map")
    private List<Parkspace> parkspaces;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "map")
    private List<Link> links;

    @Transient
    private List<SpeedColor> speedColors;

    @Transient
    private List<Integer> edgeNodes = new ArrayList<Integer>();

    public int maxLatitude;
    public int minLatitude;

    public int maxLongitude;
    public int minLongitude;

    private String name;

    @Version
    private Integer version;        //is needed else we get a OptimisticLockException

    public static final int simulationMapHeight = 940;
    public static final int simulationMapWidth = 1010;
    private boolean isRemoved = false;


    public DefaultMap() {
        init();
    }

    public DefaultMap(String name) {
        this.name = name;
        init();
    }

    private void init() {
        speedColors = new ArrayList<>();

        speedColors.add(new SpeedColor(30, "#b22222"));
        speedColors.add(new SpeedColor(45, "orange"));
        speedColors.add(new SpeedColor(60, "#20b2aa"));
        speedColors.add(new SpeedColor(75, "#006400"));

        edgeNodes.add(1);
        edgeNodes.add(2);
        edgeNodes.add(3);
        edgeNodes.add(4);
        edgeNodes.add(5);
        edgeNodes.add(6);
        edgeNodes.add(7);
        edgeNodes.add(8);
        edgeNodes.add(9);
        edgeNodes.add(10);
        edgeNodes.add(11);
        edgeNodes.add(12);

        edgeNodes.add(111);
        edgeNodes.add(112);
        edgeNodes.add(113);
        edgeNodes.add(114);
        edgeNodes.add(115);
        edgeNodes.add(116);
        edgeNodes.add(117);
        edgeNodes.add(118);
        edgeNodes.add(119);
        edgeNodes.add(120);
        edgeNodes.add(121);
    }

    public List<SpeedColor> getSpeedColors() {
        return this.speedColors;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public Node getNode(int node_id) {
        return this.nodes.get(node_id - 1);
    }

    public Node getParkspace(int parkspace_id) {
        return this.getParkspaces().get(parkspace_id - 1);
    }

    public int getParkspacesCount() {
        int total = 0;
        for(Parkspace parkspace : this.parkspaces) {
            total+= parkspace.getTotalParkspaces();
        }
        return total;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void addParkspace(Parkspace parkspace) {
        this.parkspaces.add(parkspace);
    }

    public void addNode(Node node) {
        this.nodes.add(node);
    }

    public void addLink(Link link) {
        this.links.add(link);
    }

    public List<Parkspace> getParkspaces() {
        return parkspaces;
    }

    public void setParkspaces(List<Parkspace> parkspaces) {
        this.parkspaces = parkspaces;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Edge getRandomEdgeNode() {
        return this.edges.get(Utility.random(0, this.edges.size()-1));
        /*
        Edge edge = Ebean.find(Edge.class)
                .where()
                .eq("map_id", this.getId())
                        //.in("id", edgeNodes)
                .orderBy("RAND()")
                .setMaxRows(1)
                .findUnique();
        return edge;
        */
    }

    public Node getRandomNode() {
        return this.nodes.get(Utility.random(0, this.nodes.size()-1));
        /*
        Node node = Ebean.find(Node.class)
                .where()
                .eq("map_id", this.getId())
                .orderBy("RAND()")
                .setMaxRows(1)
                .findUnique();
        return node;
        */
    }

    public Destination getRandomDestination() {
        return this.destinations.get(Utility.random(0, this.destinations.size()-1));
        /*
        Destination destination = Ebean.find(Destination.class)
                .where()
                .eq("map_id", this.getId())
                .orderBy("RAND()")
                .setMaxRows(1)
                .findUnique();

        return destination;
        */
    }


    public Parkspace getRandomParkspace() {
        return this.parkspaces.get(Utility.random(0, this.parkspaces.size()-1));

        /*
        Parkspace parkspace = Ebean.find(Parkspace.class)
                .where()
                .eq("map_id", this.getId())
                .orderBy("RAND()")
                .setMaxRows(1)
                .findUnique();
        return parkspace;
        */
    }

    /**
     * normalized for Frontend
     *
     * @return double
     */

    public double getLongitudeNormalized(Position position) {
        int mapCanvasWidth = 1000;

        int minLng = this.minLongitude;
        int maxLng = this.maxLongitude;
        int deltaLong = maxLng - minLng;

        return ((position.getLongitude() * 100000 - minLng) / deltaLong) * mapCanvasWidth;
    }

    /**
     * normalized for Frontend
     *
     * @return double
     */

    public double getLatitudeNormalized(Position position) {
        int mapCanvasHeight = 600;
        int mapCanvasOffset = 50;

        int minLat = this.minLatitude;
        int maxLat =this.maxLatitude;
        int deltaLat = maxLat - minLat;

        return (mapCanvasHeight+mapCanvasOffset) - ((position.getLatitude() * 100000 - minLat) / deltaLat) * mapCanvasHeight;
    }

    /**
     * In Meter
     *
     * @param fromNode
     * @param toNode
     * @return
     */
    public static double getDistance(Node fromNode, Node toNode) {

        Position fromPosition = fromNode.getPosition();
        Position toPosition = toNode.getPosition();

        /*
        double lat1 = fromPosition.getLatitude();
        double lon1 = fromPosition.getLongitude();

        double lat2 = toPosition.getLatitude();
        double lon2 = toPosition.getLongitude();
        */

        double lat1 = Math.toRadians(fromPosition.getLatitude());
        double lon1 = Math.toRadians(fromPosition.getLongitude());

        double lat2 = Math.toRadians(toPosition.getLatitude());
        double lon2 = Math.toRadians(toPosition.getLongitude());

        int R = 6373; // km, R is the radius of the Earth

        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;
        double a = Math.pow((Math.sin(dlat / 2)), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon / 2), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c;

        double distance = (d * 1000);

        /*
        System.out.println("from node id: "+fromNode.getId()+" "+fromNode.getPosition().getLatitude()+" "+fromNode.getPosition().getLongitude());
        System.out.println("to node id: "+toNode.getId()+" "+toNode.getPosition().getLatitude()+" "+toNode.getPosition().getLongitude());
        System.out.println("distance: :"+distance);
        */

        return distance;
    }

    public List<Link> getNeighbourLinks(Node node) {

        List<Link> neightbourLinks = new LinkedList<>();
        for(Link link : this.links) {
            if(node.equals(link.getStartNode())) {
                neightbourLinks.add(link);
            }
        }
        return neightbourLinks;
    }

    public Link getClosestLink(Node targetDestinationNode, List<Link> links) {
        //todo: improvement: get link where angle between currentsmallest so we drive closer to target
        Link matchingLink = null;

        for(Link link : links) {

            if(matchingLink == null ||
                    DefaultMap.getDistance(link.getDestinationNode(), targetDestinationNode) < DefaultMap.getDistance(matchingLink.getDestinationNode(), targetDestinationNode)) {
                matchingLink = link;
            }
        }

        return matchingLink;
    }

    public Link getFarthestLink(Node targetDestinationNode, List<Link> links) {
        //todo: improvement: get link where angle smallest so we drive closer to target

        Link matchingLink = null;

        for(Link link : links) {

            if(matchingLink == null ||
                    DefaultMap.getDistance(link.getDestinationNode(), targetDestinationNode) > DefaultMap.getDistance(matchingLink.getDestinationNode(), targetDestinationNode)) {
                matchingLink = link;
            }
        }

        return matchingLink;
    }


    public String toString() {
        return "DefaultMap{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }


    public static Map<String, String> findAll() {

        List<DefaultMap> maps = new Model.Finder(String.class, DefaultMap.class).all();

        Map<String, String> retVal = new HashMap<String, String>();
        for (DefaultMap map : maps) {
            retVal.put("" + map.getId(), map.getName());
        }
        return retVal;
    }

    public static Map<String, String> findAllActive() {

        List<DefaultMap> maps = new Model.Finder(String.class, DefaultMap.class).where().eq("is_removed", false).findList();

        Map<String, String> retVal = new HashMap<String, String>();
        for (DefaultMap map : maps) {
            retVal.put("" + map.getId(), map.getName());
        }
        return retVal;
    }

    public Route getFastestRoute(Node start, Node target) {
        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(this);
        dijkstra.execute(start);
        LinkedList<Node> path = dijkstra.getPath(target);
        if (path == null) {
            return new Route(new LinkedList<Link>());
        }

        LinkedList<Link> routeLinks = new LinkedList<>();
        Node previousNode = path.pop();

        for (Node node : path) {
            //routeLinks.add(Link.getLinkByStartAndDest(previousNode, node));
            routeLinks.add(this.getLinkByStartAndDest(previousNode, node));
            previousNode = node;
        }

        return new Route(routeLinks);
    }

    private Link getLinkByStartAndDest(Node start, Node destination) {
        for(Link link : this.links) {
            if(link.getStartNode().equals(start) && link.getDestinationNode().equals(destination)) {
                return link;
            }
        }
        return null;
    }

    /**
     * radius in meter
     * @param node
     * @param radius
     * @return
     */
    public MapArea getMapArea(Node node, int radius) {
        List<Link> mapAreaLinks = new LinkedList<>();

        for(Link link : this.links) {
            Node startNode = link.getStartNode();
            Node destinationNode = link.getDestinationNode();

            if(DefaultMap.getDistance(node, startNode) <= radius && DefaultMap.getDistance(node, destinationNode) <= radius) {
                mapAreaLinks.add(link);
            }
        }
        return new MapArea(mapAreaLinks);
    }

    public void delete() {
        for(Link link : this.getLinks()) {
            link.delete();

        }

        for(Node node : this.getNodes()) {
            node.delete();
        }

        super.delete();
    }

    public void remove() {
        this.isRemoved = true;
        this.save();
    }

}
