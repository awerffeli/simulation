package models;

import entities.Position;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by werf on 08.10.14.
 */

@Entity
@DiscriminatorValue("2")
public class Destination extends Node {

    public Destination(Position position, DefaultMap map) {
        super(position, map);
    }

    public String toString() {

        StringBuilder value = new StringBuilder();

        value.append("Destination ");
        value.append("id " + id);
        value.append(" position " + this.getPosition());

        return value.toString();
    }
}
