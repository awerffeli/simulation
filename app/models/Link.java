package models;

import play.db.ebean.Model;
import util.SpeedColor;

import javax.persistence.*;
import java.util.List;

/**
 * Created by werf on 01.10.14.
 */

@Entity
public class Link extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne()
    private DefaultMap map;

    @OneToOne
    private Node startNode;

    @OneToOne
    private Node destinationNode;

    private int speed;
    private double length;
    private int direction;

    public Link() {
    }

    public Link(int speed, double length, Node startNode, Node destinationNode, int direction) {
        this.speed = speed;
        this.length = length;
        this.startNode = startNode;
        this.destinationNode = destinationNode;
        this.direction = direction;
    }

    public int getId() {
        return id;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public Node getStartNode() {
        return startNode;
    }

    public void setStartNode(Node startNode) {
        this.startNode = startNode;
    }

    public Node getDestinationNode() {
        return destinationNode;
    }

    public void setDestinationNode(Node destinationNode) {
        this.destinationNode = destinationNode;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public DefaultMap getMap() {
        return map;
    }

    public void setMap(DefaultMap map) {
        this.map = map;
    }

    public String getSpeedColor() {

        String currentColor = "";
        List<SpeedColor> speedColors = this.getMap().getSpeedColors();

        for (SpeedColor speedColor : speedColors) {

            Integer speed = speedColor.getSpeed();
            String color = speedColor.getColor();

            if (this.speed > speed) {
                currentColor = color;
            }
        }

        return currentColor;
    }

    public String toString() {

        StringBuilder value = new StringBuilder();

        value.append("ID: " + id);
        value.append(" SPEED: " + speed);
        value.append(" LENGTH: " + length);
        value.append(" DIRECTION: " + direction);
        value.append(" FROM: " + startNode);
        value.append(" TO: " + destinationNode);

        return value.toString();
    }
}
