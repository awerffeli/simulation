package models.results;

import entities.strategies.placebook.Placebook;
import models.Simulation;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Created by werf on 08.12.14.
 */

@Entity
public class MatchingResult extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne
    private Simulation simulation;

    private int searchCount;
    private int offerCount;

    private int matchCount;
    private int cancelMatchCount;
    private int successfulMatchCount;

    private int cancelSearchCount;
    private int cancelOfferCount;


    public MatchingResult(Simulation simulation, Placebook placebook) {
        this.simulation = simulation;
        this.searchCount = placebook.getSearchCount();
        this.offerCount = placebook.getOfferCount();
        this.matchCount = placebook.getMatchCount();
        this.cancelMatchCount = placebook.getCancelMatchCount();
        this.successfulMatchCount = placebook.getSuccessfulMatchCount();
        this.cancelSearchCount = placebook.getCancelSearchCount();
        this.cancelOfferCount = placebook.getCancelOfferCount();
    }

    public int getEffectiveSearchCount() {
        return searchCount - cancelSearchCount;
    }

    public int getEffectiveOfferCount() {
        return offerCount - cancelOfferCount;
    }


    public int getSearchCount() {
        return searchCount;
    }

    public int getOfferCount() {
        return offerCount;
    }

    public int getMatchCount() {
        return matchCount;
    }

    public int getCancelMatchCount() {
        return cancelMatchCount;
    }

    public int getSuccessfulMatchCount() {
        return successfulMatchCount;
    }

    public int getCancelSearchCount() {
        return cancelSearchCount;
    }

    public int getCancelOfferCount() {
        return cancelOfferCount;
    }

}
