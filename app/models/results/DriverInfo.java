package models.results;

import entities.Driver;
import entities.DriverPreference;
import models.DefaultMap;
import models.Node;
import models.Parkspace;
import models.Simulation;
import play.db.ebean.Model;
import util.Utility;

import javax.persistence.*;
import java.util.List;

/**
 * Is used for simulation result
 * Created by werf on 14.10.14.
 */

@Entity
public class DriverInfo extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int carId;

    @Transient
    private int currentStep = 1;

    @ManyToOne
    private SimulationResult simulationResult;

    private String carSize;
    private String strategy;

    private int maxWalkingDistance;                                 //in Meter
    private double maxWillingToPayForParking;                       //in CHF

    @ManyToOne
    private Node destinationNode;

    @ManyToOne
    private Parkspace parkspace;

    private long start, end;

    @ManyToOne
    private Parkspace optimalParkspace;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "driverInfo", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<SimulationStep> steps;

    private double drivenDistance = -1;
    private double walkedDistance = -1;

    //is needed else we get a OptimisticLockException
    @Version
    private Integer version;

    public DriverInfo(Driver driver, SimulationResult simulationResult) {
        this.start = driver.getStartSearchTime();
        this.end = driver.getEndSearchTime();
        this.carId = driver.getId();
        this.simulationResult = simulationResult;
        this.carSize = driver.getCarSize().getType();

        DriverPreference preference = driver.getPreference();
        this.strategy = preference.getStrategy().getName();
        this.maxWalkingDistance = preference.getMaxWalkDistance();
        this.maxWillingToPayForParking = preference.getMaxWillingToPayForParking();
        this.destinationNode = preference.getTargetDestinationNode();
        this.parkspace = driver.getParkspace();
    }

    public int getCarId() {
        return carId;
    }


    public String getCarSize() {
        return carSize;
    }

    public String getStrategy() {
        return strategy;
    }

    public int getMaxWalkingDistance() {
        return maxWalkingDistance;
    }

    public double getMaxWillingToPayForParking() {
        return maxWillingToPayForParking;
    }

    /**
     * driven distance for parkspace search
     * in km
     * @return
     */
    public double getDrivenDistance() {
        if(this.drivenDistance == -1) {
            double drivenDistance = 0;
            for (SimulationStep step : this.steps) {
                drivenDistance += step.getLink().getLength();
            }
            this.drivenDistance = drivenDistance;
            this.save();
        }
        return this.drivenDistance;
    }
    public double getDrivenDistanceKM() {
        return this.drivenDistance / 1000;
    }

    public String getDrivenDistanceKFormatted() {
        return String.format("%.2f km", this.getDrivenDistanceKM());
    }

    public boolean isFoundParkspace() {
        if(this.getParkspace() != null) {
            return true;
        }
        return false;
    }


    public String getWalkedDistanceFormatted() {
        return String.format("%.0f m", this.getWalkedDistance());
    }

    public long getParkspaceSearchTimeMillis() {
        if(this.end != 0 && this.start != 0) {
            return this.end - this.start;
        }
        return 0;
    }

    /**
     * in realtime seconds
     * @return
     */
    public double getParkspaceSearchTimeRealTimeSeconds() {
        return (Simulation.convertFromSimulationTime((int)(this.getParkspaceSearchTimeMillis()), this.getSimulationResult().getSimulation().getSimulationSpeed()) / 1000);
    }

    public String getParkspaceSearchTimeFormatted() {
        int totalSeconds = new Double(this.getParkspaceSearchTimeRealTimeSeconds()).intValue();
        return Utility.formatSecondsToMin(totalSeconds);
    }

    public Parkspace getParkspace() {
        return parkspace;
    }

    public Parkspace getOptimalParkspace() {
        return optimalParkspace;
    }


    public long getEnd() {
        return end;
    }

    public void setOptimalParkspace(Parkspace optimalParkspace) {
        this.optimalParkspace = optimalParkspace;
    }

    public Node getDestinationNode() {
        return destinationNode;
    }

    public void setDestinationNode(Node destinationNode) {
        this.destinationNode = destinationNode;
    }

    public SimulationResult getSimulationResult() {
        return simulationResult;
    }

    public double getWalkedDistance() {
        if(this.walkedDistance < 0) {
            this.walkedDistance = this.getDistancePPToDest();
            this.save();
        }
        return this.walkedDistance;
    }

    public double getDistanceOptimalParkspaceToDestination() {
        return this.getDistancePPToDest();
    }

    private double getDistancePPToDest() {
        if(this.parkspace != null && this.destinationNode != null) {
            return DefaultMap.getDistance(this.parkspace, this.destinationNode);
        }
        return 0;
    }

    public double getDestinationLatitude() {
        return this.destinationNode.getPosition().getLatitude();
    }

    public double getDestinationLongitude() {
        return this.destinationNode.getPosition().getLongitude();
    }

    public int getCurrentStep() {
        return currentStep++;
    }

    public void addSimulationStep(SimulationStep step) {
        this.steps.add(step);
    }

    public void delete() {
        super.delete();
    }

}