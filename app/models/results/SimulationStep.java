package models.results;

import models.Link;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Is used for simulation result
 * Created by werf on 14.10.14.
 */

@Entity
public class SimulationStep extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int stepNr;

    @ManyToOne
    private DriverInfo driverInfo;

    @ManyToOne
    private Link link;

    public SimulationStep(DriverInfo driverInfo, Link link) {
        this.stepNr = driverInfo.getCurrentStep();
        this.driverInfo = driverInfo;
        this.link = link;
    }

    public int getStepNr() {
        return stepNr;
    }

    public DriverInfo getDriverInfo() {
        return driverInfo;
    }

    public Link getLink() {
        return link;
    }

}
