package models.results;

import com.avaje.ebean.Ebean;
import models.Simulation;
import models.Traffic;
import play.db.ebean.Model;
import util.Utility;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by werf on 29.10.14.
 */
@Entity
public class SimulationResult extends Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne
    private Simulation simulation;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "simulationResult", cascade = CascadeType.ALL)
    private List<DriverInfo> driverInfos;

    private double totalDrivenDistance = -1;            //km
    private double totalWalkedDistance = -1;            //m
    private double maxDrivenDistance = -1;

    private String trafficName;
    private int newVehiclesRate;
    private int parkedVehiclesCount;
    private int drivingVehiclesCount;
    private boolean constantTraffic;

    @Version
    private Integer version;        //is needed else may get a OptimisticLockException

    public SimulationResult(Simulation simulation) {
        this.simulation = simulation;
        Traffic traffic = simulation.getTraffic();
        this.trafficName = traffic.getName();
        this.newVehiclesRate = traffic.getNewVehiclesRate();
        this.parkedVehiclesCount = traffic.getParkedVehiclesCount();
        this.drivingVehiclesCount = traffic.getDrivingVehiclesCount();
        this.constantTraffic = traffic.isConstantTraffic();
    }

    public Simulation getSimulation() {
        return simulation;
    }

    public List<DriverInfo> getDriverInfos() {
        return driverInfos;
    }

    public List<DriverInfo> getDriverInfosSorted() {
        return Ebean.find(DriverInfo.class).where().eq("simulation_result_id", this.id).orderBy("start").findList();
    }

    /**
     * total driven distance for parkspace search
     * @return
     */
    public double getTotalDrivenDistance() {
        if(this.totalDrivenDistance == -1) {
            this.totalDrivenDistance = this.getTotalDrivenDistance(this.getDriverInfos());
            this.save();
        }
        return this.totalDrivenDistance;
    }

    public void addDriverInfo(DriverInfo driverInfo) {
        this.driverInfos.add(driverInfo);
    }

    public double getTotalDrivenDistance(String strategy) {
        return this.getTotalDrivenDistance(this.getDriverInfoByStrategy(strategy));
    }


    private double getTotalDrivenDistance(List<DriverInfo> driverInfos) {
        double totalDrivenDistance = 0;
        for (DriverInfo driverInfo : driverInfos) {
            totalDrivenDistance += driverInfo.getDrivenDistance();
        }
        return totalDrivenDistance / 1000;
    }

    public double getTotalWalkedDistance() {
        if(this.totalWalkedDistance < 0) {
            List<DriverInfo> driverInfos = this.getDriverInfos();
            double tempTotalWalkedDistance = 0;
            for (DriverInfo driverInfo : driverInfos) {
                tempTotalWalkedDistance += driverInfo.getWalkedDistance();
            }
            this.totalWalkedDistance = tempTotalWalkedDistance;
        }
        double result = this.totalWalkedDistance;
        return result;
    }

    public double getTotalWalkedDistance(String strategy) {
        List<DriverInfo> driverInfos= this.getDriverInfoByStrategy(strategy);
        double strategyTotalWalkedDistance = 0;
        for (DriverInfo driverInfo : driverInfos) {
            strategyTotalWalkedDistance += driverInfo.getWalkedDistance();
        }
        return strategyTotalWalkedDistance;
    }

    public boolean isConstantTraffic() {
        return constantTraffic;
    }

    /**
     * average driven distance for parkspace search
     * @return
     */
    public double getAverageDrivenDistance() {
        int vehicleCount = this.getVehiclesCount();
        if(vehicleCount == 0) {
            return 0;
        }
        return this.getTotalDrivenDistance() / vehicleCount;
    }

    /**
     * average driven walked from pp to destination
     * @return
     */
    public double getAverageWalkedDistance() {
        int vehicleCount = this.getDriverInfos(true).size();
        if(vehicleCount == 0) {
            return 0;
        }
        return this.getTotalWalkedDistance() / vehicleCount;
    }

    public double getAverageDrivenDistance(String strategy) {
        List<DriverInfo> driverInfos = this.getDriverInfoByStrategy(strategy, true);
        if(driverInfos.size() == 0) {
            return 0;
        }
        return this.getTotalDrivenDistance(driverInfos) / driverInfos.size();
    }

    public double getAverageWalkedDistance(String strategy) {
        List<DriverInfo> driverInfos = this.getDriverInfoByStrategy(strategy, true);
        if(driverInfos.size() == 0) {
            return 0;
        }
        return this.getTotalWalkedDistance(strategy) / driverInfos.size();
    }


    public int getVehiclesCount() {
        return this.getDriverInfos().size();
    }

    public String getTrafficName() {
        return trafficName;
    }


    public int getNewVehiclesRate() {
        return newVehiclesRate;
    }

    public int getParkedVehiclesCount() {
        return parkedVehiclesCount;
    }

    public int getDrivingVehiclesCount() {
        return drivingVehiclesCount;
    }

    /**
     * @return
     */
    public double getAverageParkspaceSearchTime(boolean includeNotFoundParking) {
        return this.getAverageParkspaceSearchTime(this.getDriverInfos(), includeNotFoundParking);
    }

    private double getAverageParkspaceSearchTime(List<DriverInfo> driverInfos, boolean includeNotFoundParking) {
        double averageParkspaceSearchTime;
        double total = 0;
        double searchTime;
        double count = 0;
        for (DriverInfo driverInfo : driverInfos) {
            if(!includeNotFoundParking && driverInfo.getParkspace() == null) {
                continue;
            }
            searchTime = driverInfo.getParkspaceSearchTimeMillis();
            if (searchTime > 0) {
                total += searchTime;
                count ++;
            }
        }
        if(count == 0) {
            averageParkspaceSearchTime = 0;
        } else {
            averageParkspaceSearchTime = total / count;
        }
        return averageParkspaceSearchTime;
    }

    public String getAverageParkspaceSearchTimeFormatted(boolean includeNotFoundParking) {
        double averageMillis = this.getAverageParkspaceSearchTime(includeNotFoundParking);
        double averageRealTimeMillis = Simulation.convertFromSimulationTime(averageMillis, this.getSimulation().getSimulationSpeed());
        return Utility.formatSecondsToMin(new Double(averageRealTimeMillis / 1000).intValue());
    }

    public String getAverageParkspaceSearchTimeFormatted(String strategy, boolean includeNotFoundParking) {
        List<DriverInfo> driverInfos = this.getDriverInfoByStrategy(strategy);
        double averageMillis = this.getAverageParkspaceSearchTime(driverInfos, includeNotFoundParking);
        double averageRealTimeMillis = Simulation.convertFromSimulationTime(averageMillis, this.getSimulation().getSimulationSpeed());

        return Utility.formatSecondsToMin(new Double(averageRealTimeMillis / 1000).intValue());
    }

    private List<DriverInfo> getDriverInfoByStrategy(String strategy) {
        return this.getDriverInfoByStrategy(strategy, false);
    }

    private List<DriverInfo> getDriverInfos(boolean requireParkspaceFound) {
        if(!requireParkspaceFound) {
            return this.driverInfos;
        }

        List<DriverInfo> driverInfos = new LinkedList<>();
        for(DriverInfo driverInfo : this.getDriverInfos()) {
            if(driverInfo.isFoundParkspace()) {
                driverInfos.add(driverInfo);
            }
        }
        return driverInfos;
    }

    private List<DriverInfo> getDriverInfoByStrategy(String strategy, boolean requireParkspaceFound) {
        List<DriverInfo> driverInfos = new LinkedList<>();
        for(DriverInfo driverInfo : this.getDriverInfos()) {
            if(strategy.equals(driverInfo.getStrategy())) {
                if(!requireParkspaceFound || driverInfo.isFoundParkspace()) {
                    driverInfos.add(driverInfo);
                }
            }
        }
        return driverInfos;
    }

    public int getStrategyUsage(String strategy) {
        return this.getDriverInfoByStrategy(strategy).size();
    }

    public float getStrategyUsagePercent(String strategy) {
        int strategyUsage = this.getStrategyUsage(strategy);
        if(strategyUsage == 0) {
            return 0;
        }
        float total = this.getDriverInfos().size();
        float divider = total / strategyUsage;
        if(divider == 0) {
            return 0;
        }
        float usage = 100f / divider;
        return usage;
    }

    public String getStrategyUsagePercentFormatted(String strategy) {
        return String.format("%.2f %%", this.getStrategyUsagePercent(strategy));
    }
}
