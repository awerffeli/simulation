package controllers;

import com.avaje.ebean.Ebean;
import models.Traffic;
import play.data.DynamicForm;
import play.data.Form;
import play.db.ebean.Model;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.traffic.edit;

import java.util.List;

public class TrafficController extends Controller {

    public static Result index() {
        List<Traffic> traffics = new Model.Finder(String.class, Traffic.class).where().eq("isTemplate", true).eq("isRemoved", false).findList();
        return ok(views.html.traffic.index.render(traffics));
    }

    public static Result newTraffic() {
        Form<Traffic> form = new Form(Traffic.class);
        form = form.fill(new Traffic());
        return ok(views.html.traffic.create.render(form));
    }

    public static Result createAction() {
        Form<Traffic> form = new Form(Traffic.class).bindFromRequest();

        if (form.hasErrors()) {
            return badRequest(views.html.traffic.create.render(form));
        } else {
            Traffic traffic = form.get();

            DynamicForm form2 = Form.form().bindFromRequest();
            int strategyBlock = Integer.parseInt(form2.get("strategyOneTwoBlock"));
            int strategyLuck = Integer.parseInt(form2.get("strategyLuck"));
            int strategyInsideOutside = Integer.parseInt(form2.get("strategyInsideOutside"));
            int strategyMatch = Integer.parseInt(form2.get("strategyMatch"));

            int vehicleCompact = Integer.parseInt(form2.get("vehicleCompact"));
            int vehicleNormal = Integer.parseInt(form2.get("vehicleNormal"));
            int vehicleBig = Integer.parseInt(form2.get("vehicleBig"));

            traffic.setStrategies(strategyBlock, strategyLuck, strategyInsideOutside, strategyMatch);
            traffic.setVehicles(vehicleCompact, vehicleNormal, vehicleBig);

            traffic.save();
            return redirect(controllers.routes.TrafficController.index());
        }
    }

    public static Result editTraffic(Integer trafficId) {

        Traffic traffic = Ebean.find(Traffic.class).where().eq("id", trafficId).findUnique();

        Form<Traffic> form = new Form(Traffic.class);
        form = form.fill(traffic);
        return ok(views.html.traffic.edit.render(form));
    }


    public static Result editAction(Integer trafficId) {

        Traffic t1 = Ebean.find(Traffic.class).where().eq("id", trafficId).findUnique();

        Form<Traffic> form = new Form(Traffic.class).bindFromRequest();

        if (form.hasErrors()) {
            form = form.fill(t1);
            return badRequest(edit.render(form));
        } else {
            //workaround: if we use t1 = form.bindFromRequest().get() play will create a new entity

            Traffic t2 = form.bindFromRequest().get();

            t1.setDrivingVehiclesCount(t2.getDrivingVehiclesCount());
            t1.setParkedVehiclesCount(t2.getParkedVehiclesCount());
            t1.setNewVehiclesRate(t2.getNewVehiclesRate());
            t1.setConstantTraffic(t2.isConstantTraffic());
            t1.setName(t2.getName());
            t1.setComment(t2.getComment());

            t1.setRangePayParkingMin(t2.getRangePayParkingMin());
            t1.setRangePayParkingMax(t2.getRangePayParkingMax());
            t1.setRangeWalkMin(t2.getRangeWalkMin());
            t1.setRangeWalkMax(t2.getRangeWalkMax());
            t1.setRangeTimeParkingMin(t2.getRangeTimeParkingMin());
            t1.setRangeTimeParkingMax(t2.getRangeTimeParkingMax());
            t1.setRangeTimeParkedParkingMin(t2.getRangeTimeParkedParkingMin());
            t1.setRangeTimeParkedParkingMax(t2.getRangeTimeParkedParkingMax());
            t1.setMaxParkspaceSearchTime(t2.getMaxParkspaceSearchTime());

            DynamicForm form2 = Form.form().bindFromRequest();
            int strategyBlock = new Float(Float.parseFloat(form2.get("strategyOneTwoBlock"))).intValue();
            int strategyLuck = new Float(Float.parseFloat(form2.get("strategyLuck"))).intValue();
            int strategyInsideOutside = Integer.parseInt(form2.get("strategyInsideOutside"));
            int strategyMatch = new Float(Float.parseFloat(form2.get("strategyMatch"))).intValue();

            int vehicleCompact = new Float(Float.parseFloat(form2.get("vehicleCompact"))).intValue();
            int vehicleNormal = new Float(Float.parseFloat(form2.get("vehicleNormal"))).intValue();
            int vehicleBig = new Float(Float.parseFloat(form2.get("vehicleBig"))).intValue();

            t1.setStrategies(strategyBlock, strategyLuck, strategyInsideOutside, strategyMatch);
            t1.setVehicles(vehicleCompact, vehicleNormal, vehicleBig);

            t1.save();
            return redirect(controllers.routes.TrafficController.index());
        }
    }

    public static Result deleteAction(Integer trafficId) {
        Traffic traffic = Ebean.find(Traffic.class).where().eq("id", trafficId).findUnique();
        traffic.remove();
        return redirect(controllers.routes.TrafficController.index());
    }


}