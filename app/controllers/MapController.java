package controllers;

import com.avaje.ebean.Ebean;
import importer.DefaultMapImporter;
import importer.MapImporterInterface;
import importer.tomtom.TomTomMapImporter;
import models.DefaultMap;
import play.Play;
import play.data.DynamicForm;
import play.data.Form;
import play.db.ebean.Model;
import play.mvc.Controller;
import play.mvc.Result;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapController extends Controller {

    public static Result index() {
        List<DefaultMap> maps = new Model.Finder(String.class, DefaultMap.class).where().eq("isRemoved", false).findList();
        return ok(views.html.map.index.render(maps));
    }

    public static Result newMap() {
        Form<DefaultMap> form = new Form(DefaultMap.class);
        form = form.fill(new DefaultMap());
        return ok(views.html.map.create.render(40));
    }

    public static Result createAction() {

        DynamicForm form = Form.form().bindFromRequest();

        if (form.hasErrors()) {
            return redirect(routes.MapController.newMap());
        } else {
            // Traffic traffic = form.get();
            int parkspace_quantity = Integer.parseInt(form.get("parkspaces"));
            String mapType = form.get("mapType");
            String mapName = form.get("mapName");
            if(mapName.equals("")) {
                mapName = mapType;
            }

            if(mapType.equals("osm_winterthur")) {
                return MapController.importOSMWinterthurMap(mapName, parkspace_quantity);
            }
            if(mapType.equals("osm_zuerich")) {
                return MapController.importOSMZuerichMap(mapName, parkspace_quantity);
            }

            if(mapType.equals("tomtom_winterthur")) {
                return MapController.importTomtomWinterthurMap(mapName);
            }
            if(mapType.equals("tomtom_zuerich")) {
                return MapController.importTomtomZuerichMap(mapName);
            }
            return badRequest(views.html.map.create.render(40));
        }
    }

    public static Result preview(Integer mapId) {
        DefaultMap map = Ebean.find(DefaultMap.class).where().eq("id", mapId).findUnique();
        return ok(views.html.map.preview.render(map));
    }

    public static Result importOSMWinterthurMap(String name, int parkspaces) {
        File linksFile = Play.application().getFile("app/assets/maps/winterthur/roadnetwork_links.shp");

        Map config = new HashMap<>();

        //873188 = MIN(longitude)
        config.put("adjustmentZoomLongitude", 0.73F);
        config.put("adjustmentMoveLongitude", 10);
        config.put("adjustmentFactorLongitude", 873188);

        //4750009 = MIN(latitude)
        config.put("adjustmentZoomLatitude", 1F);
        config.put("adjustmentMoveLatitude", 10);
        config.put("adjustmentFactorLatitude", (4750009 - (DefaultMap.simulationMapHeight / 2)));


        DefaultMapImporter mh = new DefaultMapImporter(linksFile, config);

        mh.importMap(name, parkspaces);

        return redirect(controllers.routes.MapController.index());
    }

    public static Result importOSMZuerichMap(String name, int parkspaces) {

        File linksFile = Play.application().getFile("app/assets/maps/zuerich_hb/zh_hb_Linie_2.shp");

        Map config = new HashMap<>();

        config.put("adjustmentZoomLongitude", 1.1F);
        config.put("adjustmentMoveLongitude", 10);
        config.put("adjustmentFactorLongitude", 853451);


        config.put("adjustmentZoomLatitude", 1.1F);
        config.put("adjustmentMoveLatitude", -100);
        config.put("adjustmentFactorLatitude", 4736602);

        DefaultMapImporter mh = new DefaultMapImporter(linksFile, config);

        mh.importMap(name, parkspaces);
        return redirect(controllers.routes.MapController.index());
    }


    public static Result importTomtomWinterthurMap(String name) {
        File mapFile = Play.application().getFile("app/assets/maps/TomTom/Kanton_Zuerich.shp");
        File speedFile = Play.application().getFile("app/assets/maps/TomTom/Speeds.dbf");
        File profileFile = Play.application().getFile("app/assets/maps/TomTom/SpeedProfiles.dbf");
        File parkingSpaceJson = Play.application().getFile("app/assets/maps/TomTom/parkingspaces_ZH_Winterthur.json");

        Map config = new HashMap<String, Number>();

        //Winterthur: 47.494461, 8.690547 - 47.511740, 8.751401
        //Winterthur Altstadt: 47.497331, 8.722058 - 47.502782, 8.733902
        MapImporterInterface mh = new TomTomMapImporter(mapFile, speedFile, profileFile, parkingSpaceJson, 47.497331, 8.722058, 47.502782, 8.733902);

        DefaultMap map = (DefaultMap) mh.importMap(name);

        return redirect(controllers.routes.MapController.index());
    }

    public static Result importTomtomZuerichMap(String name) {
        File mapFile = Play.application().getFile("app/assets/maps/TomTom/Kanton_Zuerich.shp");
        File speedFile = Play.application().getFile("app/assets/maps/TomTom/Speeds.dbf");
        File profileFile = Play.application().getFile("app/assets/maps/TomTom/SpeedProfiles.dbf");
        //File parkingSpaceJson = Play.application().getFile("app/assets/maps/TomTom/parkingspaces_ZH_HB.json");
        File parkingSpaceJson = Play.application().getFile("app/assets/maps/TomTom/parkingspaces_ZH_City_whole.json");

        Map config = new HashMap<String, Number>();

        //Zürich: 47.362225, 8.501692 - 47.388381, 8.559374
        //Zürich HB: 47.374229, 8.534387 - 47.377252, 8.545008
        //MapImporterInterface mh = new TomTomMapImporter(config, mapFile, speedFile, profileFile, parkingSpaceJson, 47.374229, 8.534387, 47.377252, 8.545008);
        MapImporterInterface mh = new TomTomMapImporter(mapFile, speedFile, profileFile, parkingSpaceJson, 47.362225, 8.501692, 47.388381, 8.559374);

        DefaultMap map = (DefaultMap) mh.importMap(name);

        return redirect(controllers.routes.MapController.index());
    }

    public static Result deleteAction(Integer mapId) {
        DefaultMap map = Ebean.find(DefaultMap.class).where().eq("id", mapId).findUnique();
        map.remove();
        return redirect(controllers.routes.MapController.index());
    }

}