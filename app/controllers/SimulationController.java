package controllers;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.node.ObjectNode;
import entities.SimulationManager;
import models.DefaultMap;
import models.Simulation;
import models.Traffic;
import models.results.MatchingResult;
import models.results.SimulationResult;
import play.data.DynamicForm;
import play.data.Form;
import play.db.ebean.Model;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import play.twirl.api.Content;
import util.SimulationEventSource;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimulationController extends Controller {

    private static Map<Integer, SimulationManager> simulationManagers = new HashMap<>();

    public static Result index() {
        List<Simulation> simulations = new Model.Finder(String.class, Simulation.class).where().eq("isRemoved", false).findList();
        return ok(views.html.simulation.index.render(simulations));
    }

    public static Result newSimulation() {
        Form<Simulation> form = new Form(Simulation.class);
        form = form.fill(new Simulation());
        return ok(views.html.simulation.create.render(form));
    }


    public static Result createAction() {
        Form<Simulation> form = new Form(Simulation.class).bindFromRequest();

        if (form.hasErrors()) {
            return badRequest(views.html.simulation.create.render(form));
            //return ok(views.html.simulation.create.render(form2));
        } else {
            Simulation simulation = form.get();
            simulation.save();

            Traffic traffic = null;
            DynamicForm form2 = Form.form().bindFromRequest();
            String trafficName = form2.get("trafficName");

            if(trafficName != null) {
                trafficName.trim();
                if(!trafficName.isEmpty()) {
                    traffic = Traffic.getByName(trafficName);
                }

            }

            if(traffic == null) {
                traffic = simulation.getTraffic();
            }

            if(traffic == null) {
                return badRequest(views.html.simulation.create.render(form));
            }

            Traffic trafficClone = Traffic.clone(traffic);

            trafficClone.save();
            simulation.setTraffic(trafficClone);

            //if simulation cannot be visualized (due to heavy traffic) set to max speed
            if(simulation.isShowVisualization() && !simulation.isShowVisualizationEffective()) {
                simulation.setSimulationSpeed(Simulation.MAX_SPEED);
            }

            simulation.save();

            int id = simulation.getId();
            if(simulation.getName().isEmpty()) {
                simulation.setName("Simulation " + id);
                simulation.save();
            }

            return redirect(controllers.routes.SimulationController.runAction(simulation.getId()));
        }
    }

    public static void removeSimulationManager(SimulationManager simulationManager) {
        int simId = simulationManager.getSimulation().getId();
        SimulationController.simulationManagers.remove(simId);
    }



    public static Result cloneSimulation(Integer simId) {
        Simulation simulation = Ebean.find(Simulation.class).where().eq("id", simId).findUnique();

        //set original TemplateTraffic, clone is not available in frontend select
        Traffic traffic = simulation.getTraffic();
        Traffic originalTraffic = traffic.getOriginalTraffic();
        simulation.setTraffic(originalTraffic);
        simulation.setName("");
        Form<Simulation> form = new Form(Simulation.class);
        form = form.fill(simulation);
        return ok(views.html.simulation.create.render(form));
    }

    public static Result showResultAction(Integer simId) {
        Simulation simulation = Ebean.find(Simulation.class).where().eq("id", simId).findUnique();
        return SimulationController.showResult(simulation);
    }

    public static Result showResult(Simulation simulation) {
        SimulationResult simulationResult = simulation.getSimulationResult();
        MatchingResult matchingResult = simulation.getMatchingResult();

        return ok(views.html.simulation.result.render(simulationResult, simulation, matchingResult));
    }

    public static Result exportResultAction(Integer simId) {
        Simulation simulation = Ebean.find(Simulation.class).where().eq("id", simId).findUnique();
        SimulationResult simulationResult = simulation.getSimulationResult();
        Content content = views.html.simulation.simresults.export.render(simulationResult, simulation);

        String body = content.body();
        //byte ptext[] = body.getBytes(UTF_8);
        //String resultExport = new String(ptext, ISO_8859_1);

        try {
            // Create temp file.
            File tempFile = File.createTempFile("sim_export_"+simId+"_", ".xls");
            // Delete temp file when program exits.
            tempFile.deleteOnExit();

            // Write to temp file
            BufferedWriter out = new BufferedWriter(new FileWriter(tempFile));
            out.write(body);
            out.close();
            return Results.ok(tempFile, false);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return Results.internalServerError();
    }

    public static Result deleteAction(Integer simId) {
        Simulation simulation = Ebean.find(Simulation.class).where().eq("id", simId).findUnique();
        simulation.remove();
        return redirect(routes.SimulationController.index());
    }

    public static Result forceEndSimulation(Integer simId) {
        Simulation simulation = Ebean.find(Simulation.class).where().eq("id", simId).findUnique();

        if(SimulationController.simulationManagers.containsKey(simulation.getId())) {
            SimulationManager sm = SimulationController.simulationManagers.get(simulation.getId());
            sm.endSimulation();
        }
        return SimulationController.showResult(simulation);
    }

    public static Result runAction(int simId) {
        Simulation simulation = Ebean.find(Simulation.class).where().eq("id", simId).findUnique();
        return SimulationController.runSimulation(simulation);
    }

    public static Result runSimulation(Simulation simulation) {
        if(simulation.hasFinished()) {
            return redirect(controllers.routes.SimulationController.showResultAction(simulation.getId()));
        }

        DefaultMap map = simulation.getMap();
        return ok(views.html.simulation.running.render(simulation, map));
    }

    /**
     * Establish the SSE HTTP 1.1 connection.
     * @return Result
     */
    public static Result carFeed(Integer simId) {
        Simulation simulation = Ebean.find(Simulation.class).where().eq("id", simId).findUnique();

        SimulationManager sm;
        SimulationEventSource eventSource;

        if(SimulationController.simulationManagers.containsKey(simulation.getId())) {
            sm = SimulationController.simulationManagers.get(simulation.getId());
            return ok(sm.getEventSource());
        }
        else {
            if (simulation.isShowVisualizationEffective()) {
                eventSource = new SimulationEventSource(simulation.getMap(), true);
            } else {
                eventSource = new SimulationEventSource(simulation.getMap(), false);
            }
            sm = new SimulationManager(simulation, eventSource);
            SimulationController.simulationManagers.put(simulation.getId(), sm);

            //needs to be thread else events are not fired asynchronously
            Thread threadSimulation = new Thread(sm);
            threadSimulation.start();
        }
        return ok(eventSource);
    }

    public static Result retrieveSimulation(Integer simId) {
        Simulation simulation = Ebean.find(Simulation.class).where().eq("id", simId).findUnique();

        ObjectNode result = Json.newObject();
        result.put("runSimulationTime", simulation.getRunSimulationTime());
        result.put("runSimulationTimeFormatted", simulation.getEffectiveRunSimulationTimeFormatted());
        return ok(result);
    }
}